package hlz.cn.jhc.mytools.shortvideo;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.TouchDelegate;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;

public class ShootVideoActivity extends BaseActivity {


    @BindView(R.id.mSurfaceView)
    CameraSurfaceView mSurfaceView;
    @BindView(R.id.btn1)
    Button btn1;
    @BindView(R.id.btn2)
    Button btn2;

    int zoom = 1;
    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_shoot_video);
        ButterKnife.bind(this);


    }

    @Override
    protected void initData() {

    }


    @OnClick({R.id.btn1, R.id.btn2})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn1:
                zoom++;
                mSurfaceView.setZoom(zoom);
                break;
            case R.id.btn2:
                break;
        }
    }
}
