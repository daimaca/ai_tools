package hlz.cn.jhc.mytools.movies.web;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient.CustomViewCallback;
import com.tencent.smtt.export.external.interfaces.IX5WebSettings;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.export.external.interfaces.SslError;
import com.tencent.smtt.export.external.interfaces.SslErrorHandler;
import com.tencent.smtt.export.external.interfaces.WebResourceResponse;
import com.tencent.smtt.sdk.CookieManager;
import com.tencent.smtt.sdk.CookieSyncManager;
import com.tencent.smtt.sdk.DownloadListener;
import com.tencent.smtt.sdk.ValueCallback;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;
import com.tencent.smtt.utils.TbsLog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;
import hlz.cn.jhc.mytools.movies.CommentSelectPlatformBean;
import hlz.cn.jhc.mytools.share.ShareUtils;
import hlz.cn.jhc.mytools.utils.Tools;
import hlz.cn.jhc.mytools.zxing.TwoCodeActivity;

public class X5WebActivity extends BaseActivity {

    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.mViewParent)
    RelativeLayout mViewParent;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.close)
    ImageView close;
    @BindView(R.id.reflesh)
    ImageView reflesh;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_right)
    ImageView ivRight;
    @BindView(R.id.iv_add)
    ImageView ivAdd;
    @BindView(R.id.tv_right_content)
    TextView tvRightContent;

    /**
     * 作为一个浏览器的示例展示出来，采用android+web的模式
     */
    private X5WebView mWebView;
    private boolean isWebViewLoadingFinish = false;

    private String mHomeUrl = "https://v.splash_qq.com/";
    private static final String vip1 = "https://www.iqiyi.com/v_19rqsmg2o8.html";
    private static final String vip1_2 = "https://www.iqiyi.com/v_19rqtgolp4.html";
    private static final String vip2 = "https://v.splash_qq.com/x/cover/0gsf9fytppje54d/y002766q4ol.html";
    private static final String TAG = "SdkDemo";
    String TAG2 = "http://jx.aeidu.cn/index.php?url=https://v.splash_qq.com/x/cover/0gsf9fytppje54d/y002766q4ol.html";

    private String currentUrl = "";



    /**
     * 在线看
     * http://lfsenior.cn/?m=vod-type-id-4.html
     */

    private ValueCallback<Uri> uploadFile;
    private SelectVipUrlDialog dialog;
    private ProfessionalParsingSelectPlatformDialog mProfessionalParsingSelectdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBar();
    }

    @Override
    protected void setLayout() {

        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 11) {
                getWindow()
                        .setFlags(
                                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
            }
        } catch (Exception e) {
        }
        /*
         * getWindow().addFlags(
         * android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
         */
        setContentView(R.layout.activity_x5_web);
        ButterKnife.bind(this);
        WatchCollectionTable bean = getIntent().getParcelableExtra("bean");
        mHomeUrl = bean.getUrl();
        tvTitle.setText(bean.getTitle());
    }

    @Override
    protected void initData() {
        init();
        initDialog();
    }

    private String vipUrl = null;
    private void initDialog() {
        dialog = new SelectVipUrlDialog(this);
        dialog.setOnSelectOnclick(new SelectVipUrlDialog.SelectOnclick() {
            @Override
            public void onSure(DataVipUrl.VipUrlBean bean) {
                if (TextUtils.isEmpty(vipUrl)){
                    vipUrl = mWebView.getUrl();
                }
                if (!mWebView.getUrl().contains(vipUrl)&&!isExistVipUrl(mWebView.getUrl())){
//                    vipUrl = getInitWebViewUrl(mWebView.getUrl());
                    vipUrl = mWebView.getUrl();
                }
//                String webUrl = getInitWebViewUrl(mWebView.getUrl());
//                String url = bean.getUrl() + mWebView.getUrl();
                String url = bean.getUrl() + vipUrl;
                Log.i("url", "onSure: "+mWebView.getOriginalUrl());
                Log.i("url", "onSure: "+mWebView.getUrl());
                Log.i("url", "onSure: ---------------------"+url);
                mWebView.loadUrl(url);
                dialog.dismiss();
            }

            @Override
            public void onCancel() {
                dialog.dismiss();
            }
        });

        initDialog2();
    }

    private String getInitWebViewUrl(String url){
        for (int i = 0; i < DataVipUrl.urls.length; i++) {
            if (url.contains(DataVipUrl.urls[i])){
                url = url.replace(DataVipUrl.urls[i],"");
            }
        }
        return url;
    }

    private boolean isExistVipUrl(String url){
        for (int i = 0; i < DataVipUrl.urls.length; i++) {
            if (url.contains(DataVipUrl.urls[i])){
                return true;
            }
        }
        return false;
    }

    private void initDialog2() {

        mProfessionalParsingSelectdialog = new ProfessionalParsingSelectPlatformDialog(this);
        mProfessionalParsingSelectdialog.setOnSelectOnclick(new ProfessionalParsingSelectPlatformDialog.SelectOnclick() {
            @Override
            public void onSure(CommentSelectPlatformBean bean) {
                String url = bean.getUrl();
                mWebView.loadUrl(url);
                mProfessionalParsingSelectdialog.dismiss();
            }

            @Override
            public void onCancel() {
                mProfessionalParsingSelectdialog.dismiss();
            }
        });
    }

    private void init() {

        mWebView = new X5WebView(this, null);
        mViewParent.addView(mWebView, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));

        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView webView, String s, Bitmap bitmap) {
                super.onPageStarted(webView, s, bitmap);
                isWebViewLoadingFinish = false;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i("url", "shouldOverrideUrlLoading: "+url);
                currentUrl = url;
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                isWebViewLoadingFinish = true;
                mWebView.getSettings().setBlockNetworkImage(false);
                Log.i(TAG, "onPageFinished: ");
//                String js = ADFilterTool.getClearAdDivJs(view.getContext());
//                Log.v("adJs", js);
//                view.loadUrl(js);
                /* mWebView.showLog("test Log"); */
            }

            @Override
            public void onLoadResource(WebView webView, String s) {
                super.onLoadResource(webView, s);
//                Tools.addImage(s);
            }

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView webView, String s) {
                if (!Tools.isAd(s)){
                    Tools.addImage(s);
                }
                if (Tools.isAd(s)){
                    return new WebResourceResponse(null,null,null);
                }
                return super.shouldInterceptRequest(webView, s);
            }

            @Override
            public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
                super.onReceivedSslError(webView, sslErrorHandler, sslError);//默认的处理 把WebView变成空白
                sslErrorHandler.proceed();//接受证书
            }
        });

        mWebView.setWebChromeClient(new WebChromeClient() {

            @Override
            public boolean onJsConfirm(WebView arg0, String arg1, String arg2,
                                       JsResult arg3) {
                return super.onJsConfirm(arg0, arg1, arg2, arg3);
            }

            View myVideoView;
            View myNormalView;
            CustomViewCallback callback;

            /**
             * 全屏播放配置
             */
            @Override
            public void onShowCustomView(View view,
                                         CustomViewCallback customViewCallback) {
//                FrameLayout normalView = (FrameLayout) findViewById(R.id.web_filechooser);
//                ViewGroup viewGroup = (ViewGroup) normalView.getParent();
                mViewParent.removeView(mWebView);
                mViewParent.addView(view);
                myVideoView = view;
                myNormalView = mWebView;
                callback = customViewCallback;
            }

            @Override
            public void onHideCustomView() {
                if (callback != null) {
                    callback.onCustomViewHidden();
                    callback = null;
                }
                if (myVideoView != null) {
                    ViewGroup viewGroup = (ViewGroup) myVideoView.getParent();
                    viewGroup.removeView(myVideoView);
                    viewGroup.addView(myNormalView);
                }
            }
            @Override
            public boolean onJsAlert(WebView arg0, String arg1, String arg2,
                                     JsResult arg3) {
                /**
                 * 这里写入你自定义的window alert
                 */
//                Log.i(TAG, "onJsAlert: ---------------"+arg1);
//                Log.i(TAG, "onJsAlert: ---------------"+arg2);
//                Log.i(TAG, "onJsAlert: ---------------"+arg3.toString());
                return super.onJsAlert(null, arg1, arg2, arg3);
            }

            @Override
            public void onProgressChanged(WebView webView, int i) {
                super.onProgressChanged(webView, i);
                if (i == 0 || i >= 100) {
                    mProgressBar.setVisibility(View.GONE);
                } else {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mProgressBar.setProgress(i);
                }
            }
        });

        mWebView.setDownloadListener(new DownloadListener() {

            @Override
            public void onDownloadStart(String arg0, String arg1, String arg2,
                                        String arg3, long arg4) {
                Log.i(TAG, "onDownloadStart: "+arg0);
//                TbsLog.i(TAG, "url: " + arg0);
//                Log.i(TAG, "onDownloadStart: 1" + arg0);
//                Log.i(TAG, "onDownloadStart: 2" + arg1);
//                Log.i(TAG, "onDownloadStart: 3" + arg2);
//                Log.i(TAG, "onDownloadStart: 4" + arg3);
//                Log.i(TAG, "onDownloadStart: 5" + arg4);
            }
        });

        long time = System.currentTimeMillis();
        settingsWeb();
        mWebView.loadUrl(mHomeUrl);

        TbsLog.d("time-cost", "cost time: "
                + (System.currentTimeMillis() - time));
        CookieSyncManager.createInstance(this);
        CookieSyncManager.getInstance().sync();
        CookieManager.getInstance().setAcceptCookie(true);
        CookieManager.getInstance().flush();
    }

    private void settingsWeb() {
        WebSettings webSetting = mWebView.getSettings();
        webSetting.setBlockNetworkImage(true);// 先阻塞加载图片
        webSetting.setAppCachePath(this.getDir("appcache", 0).getPath());
        webSetting.setDatabasePath(this.getDir("databases", 0).getPath());
        webSetting.setGeolocationDatabasePath(this.getDir("geolocation", 0).getPath());
        /**
         * todo 设置是否是电脑版版加载web
         */
        boolean isComputer = Tools.isWebComputer();
        if (isComputer) {
            webSetting.setUserAgentString("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36");
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mWebView != null && mWebView.canGoBack()) {
                mWebView.goBack();
                return true;
            } else
                return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mWebView.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        TbsLog.d(TAG, "onActivityResult, requestCode:" + requestCode
                + ",resultCode:" + resultCode);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 0:
                    if (null != uploadFile) {
                        Uri result = data == null || resultCode != RESULT_OK ? null
                                : data.getData();
                        uploadFile.onReceiveValue(result);
                        uploadFile = null;
                    }
                    break;
                default:
                    break;
            }
        } else if (resultCode == RESULT_CANCELED) {
            if (null != uploadFile) {
                uploadFile.onReceiveValue(null);
                uploadFile = null;
            }

        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (intent == null || mWebView == null || intent.getData() == null)
            return;
        mWebView.loadUrl(intent.getData().toString());
    }

    @Override
    protected void onDestroy() {
        if (mViewParent != null) {
            mViewParent.removeView(mWebView);
        }
        if (mWebView != null)
            mWebView.destroy();
        super.onDestroy();
        LoadResourceAdapter.setLoadResourceList(null);//清空图片资源
    }

    @OnClick({R.id.back, R.id.close, R.id.reflesh, R.id.iv_right, R.id.tv_right_content,R.id.iv_add})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                if (mWebView == null) return;
                if (mWebView.canGoBack()) {
                    mWebView.goBack();
                } else {
                    finish();
                }
                break;
            case R.id.close:
                finish();
                break;
            case R.id.reflesh:
                if (mWebView == null) return;
                mWebView.reload();
                break;
            case R.id.iv_right:
                if (mWebView == null) return;
                showDialog();
                break;
            case R.id.tv_right_content:
                copyCurrentUrl();
                break;
                case R.id.iv_add:
                    add();
                break;
        }
    }


    private void add() {

        int w = (int) getResources().getDimension(R.dimen.dp_125);
        int h = (int) getResources().getDimension(R.dimen.dp_220);
        View view = LayoutInflater.from(this).inflate(R.layout.add_popwindow, null, false);
        final PopupWindow popWindow = new PopupWindow(view, w, h, true);
//        popWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.add_popwindow_bg));
        popWindow.setOutsideTouchable(true);
        popWindow.setTouchable(true);


        LinearLayout ll_profession_analysis = view.findViewById(R.id.ll_profession_analysis);
        LinearLayout ll_get_resource = view.findViewById(R.id.ll_get_resource);
        LinearLayout ll_two_code = view.findViewById(R.id.ll_two_code);
        LinearLayout ll_share = view.findViewById(R.id.ll_share);
        LinearLayout ll_outside_browser = view.findViewById(R.id.ll_outside_browser);
        int y = (int) getResources().getDimension(R.dimen.dp_6);
        popWindow.showAsDropDown(ivAdd, 0, y);

        ll_profession_analysis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popWindow.dismiss();
                mProfessionalParsingSelectdialog.show();
            }
        });
        ll_get_resource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popWindow.dismiss();
                startActivity(new Intent(X5WebActivity.this, LoadResourceActivity.class));
            }
        });
        ll_two_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popWindow.dismiss();
                Intent codeIntent = new Intent();
                codeIntent.setClass(X5WebActivity.this, TwoCodeActivity.class);
                codeIntent.putExtra("url",mWebView.getUrl());
                startActivity(codeIntent);
            }
        });
        ll_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popWindow.dismiss();
                ShareUtils.shareText(X5WebActivity.this, mWebView.getUrl());
            }
        });
        ll_outside_browser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popWindow.dismiss();
                Intent intent= new Intent();
                intent.setAction("android.intent.action.VIEW");
                Uri content_url = Uri.parse(mWebView.getUrl());
                intent.setData(content_url);startActivity(intent);
            }
        });

    }

    private void showDialog(){
        dialog.show();
    }

    //todo 复制地址
    private void copyCurrentUrl() {
        //获取剪贴板管理器：
        ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        // 创建普通字符型ClipData
        ClipData mClipData = ClipData.newPlainText("Label", mWebView.getUrl());
        // 将ClipData内容放到系统剪贴板里。
        cm.setPrimaryClip(mClipData);
        Tools.showShortToast("已为你复制该链接！");
    }
}
