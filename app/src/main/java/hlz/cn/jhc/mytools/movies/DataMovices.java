package hlz.cn.jhc.mytools.movies;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.application.MyApplication;
import hlz.cn.jhc.mytools.comment.LabelType;
import hlz.cn.jhc.mytools.db.SharedPreferencesConst;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;
import hlz.cn.jhc.mytools.login.LoginActivity;

/**
 * todo 视频集合
 * Created by Administrator on 2018/6/17.
 */

public class DataMovices {

    private static void initLabel(String name, WatchCollectionTable model) {
        if (TextUtils.equals(name,"爱奇艺")||TextUtils.equals(name,"腾讯视频")||TextUtils.equals(name,"优酷视频")
                ||TextUtils.equals(name,"芒果TV")||TextUtils.equals(name,"暴风影音")||TextUtils.equals(name,"看看视频")
                ||TextUtils.equals(name,"乐视")||TextUtils.equals(name,"搜孤视频")||TextUtils.equals(name,"AC—FUN")
                ||TextUtils.equals(name,"哔哩哔哩")||TextUtils.equals(name,"1905")||TextUtils.equals(name,"PP-TV")
                ||TextUtils.equals(name,"土豆视频") ||TextUtils.equals(name,"360影视")
        ){
            model.setLabel(LabelType.LABEL_OFFICIAL);
            return;
        }
        if (TextUtils.equals(name,"在线云点播")||TextUtils.equals(name,"vip影院")||TextUtils.equals(name,"疯狂影视搜索")
                ||TextUtils.equals(name,"疯狂电影迷")||TextUtils.equals(name,"Neets")||TextUtils.equals(name,"二十影院")
                ||TextUtils.equals(name,"喔喔动漫")||TextUtils.equals(name,"4399动漫")||TextUtils.equals(name,"星辰vip影院")
                ||TextUtils.equals(name,"沐享影视")||TextUtils.equals(name,"漫岛tv")||TextUtils.equals(name,"九八零影院")
                ||TextUtils.equals(name,"悠久影院")||TextUtils.equals(name,"星辰影院")||TextUtils.equals(name,"480影院")
                ||TextUtils.equals(name,"喜福影视")||TextUtils.equals(name,"谍咖网")||TextUtils.equals(name,"叮当动漫")
                ||TextUtils.equals(name,"看精品")||TextUtils.equals(name,"云影院")||TextUtils.equals(name,"部落电影")
                ||TextUtils.equals(name,"田鸡影院")||TextUtils.equals(name,"八一影院")||TextUtils.equals(name,"3397影视")
        ){
            model.setLabel(LabelType.LABEL_FLUENCY);
            return;
        }
        if (TextUtils.equals(name,"爱看番")||TextUtils.equals(name,"看看屋")||TextUtils.equals(name,"4k屋")
                ||TextUtils.equals(name,"9站")||TextUtils.equals(name,"精彩动漫")||TextUtils.equals(name,"66vip视频")
                ||TextUtils.equals(name,"叫你看")||TextUtils.equals(name,"77k影院")||TextUtils.equals(name,"chok吧电影院")
                ||TextUtils.equals(name,"野兔影院")||TextUtils.equals(name,"迅播影院")||TextUtils.equals(name,"雅阁视频")
                ||TextUtils.equals(name,"奇奇动漫")||TextUtils.equals(name,"欧呐呐")||TextUtils.equals(name,"夏日影院")
        ){
            model.setLabel(LabelType.LABEL_RECOMMEND);
            return;
        }
    }

    public static String[] names = new String[]{"专业视频解析","爱奇艺", "腾讯视频", "优酷视频", "乐视", "暴风影音", "看看视频",
            "芒果TV", "搜孤视频", "AC—FUN", "哔哩哔哩", "1905",
            "PP-TV", "音乐-tai", "在线云点播", "vip影院", "看撒动漫",
            "天天电影网", "动漫之家", "88电影网", "土豆视频", "疯狂影视搜索",
            "疯狂电影迷", "Neets", "爱看番", "看看屋", "4k屋",
            "二十影院", "9站", "爱动漫", "动漫岛", "喔喔动漫",
            "4399动漫", "精彩动漫", "66vip视频", "星辰vip影院",
            "淘米动漫" ,"叫你看", "77k影院","沐享影视","chok吧电影院","青苹果影院",
            "漫岛tv","九八零影院","悠久影院","星辰影院","480影院",
            "野兔影院","80影院","喜福影视","迅播影院","360影视",
            "谍咖网","雅阁视频","阿哥美剧","叮当动漫","奇奇动漫",
            "看精品","雅图在线","云影院","部落电影", "欧呐呐",
            "田鸡影院","夏日影院","八一影院","3397影视","乐趣影院"
    };
    public static int[] img = new int[]{R.mipmap.video_vip,
            R.mipmap.img_aiqiyi_movies, R.mipmap.img_tengxunshipin_movies, R.mipmap.img_youku_movies, R.mipmap.img_leshi_movies, R.mipmap.img_baofengyinying_movies,R.mipmap.img_tiantiankankan_mocies,
            R.mipmap.img_mangguotv_movies, R.mipmap.img_shouhushiping_movies, R.mipmap.img_acfun_movies, R.mipmap.img_bilibili_movies, R.mipmap.img_1905_movies,
            R.mipmap.img_pptv_movies, R.mipmap.img_yinyuetai_movies, R.mipmap.img_zaixiandianyunbo_movies, R.mipmap.img_vipyingyuan_movies, R.mipmap.img_kanshadongman_movies,
            R.mipmap.img_guoyudongmanzhijia_movies, R.mipmap.img_dongmanzhijia_movies, R.mipmap.img_xinshijiedongman_movies, R.mipmap.img_tudoushipin_movies, R.mipmap.img_fengkuangdianyingshoushuo_movies,
            R.mipmap.img_xianglongyingyuan_movies, R.mipmap.img_neets_movies, R.mipmap.img_aikanfan_movies, R.mipmap.img_kankanwu_movies, R.mipmap.img_4kwu_movies,
            R.mipmap.img_shieryingyuan_movies, R.mipmap.img_9zhan_movies, R.mipmap.img_aidongman_movies, R.mipmap.img_dongmandao_movies, R.mipmap.img_wowodongman_movies,
            R.mipmap.img_4399dongman_movies, R.mipmap.img_jingcaidongman_movies, R.mipmap.img_66vipshipin_movies, R.mipmap.img_xingchenvipyingyuan_movies,
            R.mipmap.img_taomidongman_movies, R.mipmap.img_jiaonikan_movies, R.mipmap.img_77kyingyuan_movies, 0,0,0,
            0,0,0,0,0,
            0,0,0,0,0,
            0,0,0,0,0,
            0,0,0,0,0,
            0,0,0,0,0
    };

    public static String[] urls = new String[]{
            "",
            "https://www.iqiyi.com/",//1 爱奇艺
            "https://v.qq.com/",//2 腾讯视频
            "https://www.youku.com/",//3 优酷视频
            "http://vip.le.com/",//4 乐视
            "http://www.baofeng.com/",//5 暴风影音
            "http://www.kankan.com/",// 看看视频

            "https://www.mgtv.com/vip/",//6 芒果TV
            "https://film.sohu.com/vip.html",//7 搜孤视频
            "http://www.acfun.cn/",//8 AC—FUN
            "https://www.bilibili.com/",//9 哔哩哔哩
            "http://vip.1905.com/",//10 1905

            "http://www.pptv.com/",//11 PP-TV
            "http://www.yinyuetai.com/",//12 音乐-tai
            "http://7.1144e.cn/",//13 在线云点播
            "http://www.sjzvip.cn/",//14 vip影院
            "http://www.kan300.com/view/top.html",//15 看撒动漫

            "http://www.27k.cc/",//16 天天电影网
            "http://donghua.dmzj.com/",//17 动漫之家
            "http://www.88ys.cc/",//18 88电影网
            "http://www.tudou.com/",//19 土豆视频
            "http://ifkdy.com/",//20 疯狂影视搜索

            "http://fkdy.me/",//21 疯狂电影迷
            "https://neets.cc/",//22 Neets
            "http://www.ikanfan.com/",//23 爱看番
            "https://www.kankanwu.com/",//24 看看屋
            "http://www.kkkkmao.com/",//25 4k喔

            "http://esyy007.com/",//26 二十影院
            "https://www.99496.com/",//27 99496-资源站
            "http://www.idm.cc/",//28 爱动漫
            "https://www.dmdao.com/",//29 动漫岛
            "http://www.wowodm.com/",//30 喔喔动漫

            "http://www.4399dmw.com/",//31 4399-动漫
            "https://www.dmnico.cn/",//32 精彩动漫
            "https://www.6666ys.com/",//33 66vip视频
            "http://www.1080vcd.com/",//34 星辰vip影院


            //附加部分
            "http://v.61.com/",// 淘米动漫
            "http://m.jiaonikan.com/",// 叫你看
            "http://www.77kyy.com/" ,// 77k影院
            "http://vip.muyl.vip/" ,// 沐享影视
            "https://www.chok8.com/" ,// chok吧电影院
            "http://www.lalalo.com/" ,// 青苹果影院

            "https://www.mandao.tv/" ,// 漫岛tv
            "http://www.tv980.com/" ,// 九八零影院
            "https://www.eqq9.com/" ,// 悠久影院
            "http://www.vodxc.com/" ,// 星辰影院
            "http://www.480hd.com/" ,// 480影院

            "https://www.ywt.cc/" ,// 野兔影院
            "http://www.80yy.cn/" ,// 80影院
            "http://www.contentchina.com/" ,// 喜福影视
            "http://www.2tu.cc/" ,// 迅播影院
            "https://www.360kan.com/" ,// 360影视

            "https://www.dieka123.com/" ,// 谍咖网
            "https://www.yageys.com/" ,// 雅阁视频
            "https://agmj.tv/" ,// 阿哥美剧
            "http://m.kt51.com/" ,// 叮当动漫
            "http://www.qiqidongman.com/" ,// 奇奇动漫

            "http://www.kanjp.com/" ,// 看精品
            "http://www.yatu.tv/" ,// 雅图在线
            "http://www.kkyyy.cc/" ,// 云影院
            "http://www.vbuluo.com/" ,// 部落电影
            "https://www.ounana.com/" ,// 欧呐呐

            "http://www.mtotoo.com/" ,// 田鸡影院
            "http://www.summerggw.com/" ,// 夏日影院
            "http://www.bayiyy.com/" ,// 八一影院
            "http://www.w3397.com/" ,// 3397
            "https://www.0lq.com/"
    };

    /**
     * todo 视频平台列表
     * @return
     */

    public static List<WatchCollectionTable> getList() {
        List<WatchCollectionTable> movicesModels = new ArrayList<>();

        movicesModels = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            WatchCollectionTable model = new WatchCollectionTable();
            model.setTitle(names[i]);
            model.setUrl(urls[i]);
            model.setImg(img[i]);
            model.setIsOwnDefined("1");
            initLabel(names[i],model);
            movicesModels.add(model);
        }
        String name = SharedPreferencesUtils.getKeyValue(MyApplication.mContext, SharedPreferencesConst.nameKey,"aixiaozhi");
        String password = SharedPreferencesUtils.getKeyValue(MyApplication.mContext, SharedPreferencesConst.passwordKey,"aixiaozhi");
        if (name.equals(LoginActivity.defaultName) && password.equals(LoginActivity.defaultPassword)) {
            movicesModels.addAll(getAdminList());
        }
        return movicesModels;
    }


    public static List<WatchCollectionTable> getAdminList() {
        List<WatchCollectionTable> movicesModels = new ArrayList<>();

        movicesModels = new ArrayList<>();
        for (int i = 0; i < adminName.length; i++) {
            WatchCollectionTable model = new WatchCollectionTable();
            model.setTitle(adminName[i]);
            model.setUrl(adminUrls[i]);
            model.setImg(0);
            model.setIsOwnDefined("1");
            movicesModels.add(model);
        }
        return movicesModels;
    }

    private static String[] adminName = new String[]{
            "7D影城","看看会员","ck电影","发烧屋影视网","牛牛影院",
            "天机影院","西瓜影院","影视精灵","天堂影院","87影院",
            "125电影网","片搜电影","新新影院","天天云影院","苏格影院",
            "卡哇猪","新剧集影院","全能影视","影视分享","有家影院",
            "恋恋影视","七七电视","俺去也","乖萌兔","午夜影院",
            "66影视","鸭子谷","被窝电影","黑鸭影院","电影济",
            "神马电影网","掌趣影院","电影天堂",
    };
    private static String[] adminUrls = new String[]{
            "http://www.1966w.com/",
            "http://vip.kankan.com/",
            "http://m.ckck.vip/",
            "http://www.hifiwu.com/",
            "https://www.48da.net/",

            "http://www.98tianji.com/",
            "https://www.xigua110.com/",
            "http://www.ysjltv.com/",
            "https://www.dytt888.com/",
            "https://www.87dyy.com/",

            "http://www.125dy.com/",
            "http://www.psdyz.com/",
            "https://www.xxyy10.com/",
            "http://www.ttyyy.vip/index.html",
            "http://www.z7yy.com/",

            "http://www.kwpig.com/",
            "http://www.xinjuji.com/",
            "http://www.qnvod.net/",
            "https://www.zxysfx.com/",
            "http://www.youjiady.com/",// 有家影院

            "http://m4.22c.im/",// 恋恋影视
            "https://www.77ds.vip/",// 恋恋影视
            "http://www.anquyetv.com/",// 俺去也
            "http://www.guaimengtu.com/",// 乖萌兔
            "http://www.qingcai123.com/",// 午夜影院

            "http://www.gzdiren.com/",// 66影视
            "http://www.soumulu.com/" ,// 鸭子谷
            "https://www.dududy.com/" ,// 被窝电影
            "https://www.hmheiya.com/" ,// 黑鸭影院
            "http://www.dianyingqi.com/" ,// 电影济

            "https://www.154dy.com/" ,// 神马电影网
            "http://www.fmcm-bj.com/" ,// 掌趣影院
            "http://www.smdytt.com/" ,// 电影天堂
    };
}
