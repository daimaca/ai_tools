package hlz.cn.jhc.mytools.home;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.movies.web.BrowserActivity;
import hlz.cn.jhc.mytools.movies.web.ProfessionalParsingX5WebActivity;
import hlz.cn.jhc.mytools.settings.SettingsActivity;
import hlz.cn.jhc.mytools.utils.Tools;

/**
 * Created by Administrator on 2018/12/23.
 */

public class LeftMenuAdapter extends BaseQuickAdapter<LeftMenuAdapter.LeftMenuBean, LeftMenuAdapter.ViewHolder> {

    public LeftMenuAdapter(Context context) {
        super(R.layout.home_left_menu_item, data(context));
    }

    @Override
    protected void convert(final ViewHolder helper, LeftMenuBean item) {

        Glide.with(mContext).load(item.getImg()).apply(Tools.getNoImageRequestOptions()).into(helper.ivImg);
        helper.tvTitle.setText(item.getName());

        helper.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (helper.getAdapterPosition()) {
                    case 0:
                        mContext.startActivity(new Intent(mContext, BrowserActivity.class));
                        break;
                    case 1:
                        mContext.startActivity(new Intent(mContext, ProfessionalParsingX5WebActivity.class));
                        break;
                    case 2:
                        mContext.startActivity(new Intent(mContext,TbsVideoActivity.class));
                        break;
                    case 3:
                        mContext.startActivity(new Intent(mContext,Game2048Activity.class));
                        break;
                    case 4:
                        mContext.startActivity(new Intent(mContext,SettingsActivity.class));
                        break;
                    default:
                        break;
                }
            }
        });

    }

    private static String[] names = new String[]{"小志浏览器", "专业视频解析", "工具集", "小游戏", "设置"};
    private static int[] imgs = new int[]{R.mipmap.browser,R.mipmap.video_vip,R.mipmap.tools,R.mipmap.game,R.mipmap.settings_img};

    private static List<LeftMenuBean> data(Context context) {
        List<LeftMenuBean> list = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            LeftMenuBean bean = new LeftMenuBean();
            bean.setName(names[i]);
            bean.setImg(imgs[i]);
            list.add(bean);
        }
        return list;
    }

    static class LeftMenuBean {
        int img;
        String name;
        String imgUrl;

        public int getImg() {
            return img;
        }

        public void setImg(int img) {
            this.img = img;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R.id.iv_img)
        CircleImageView ivImg;
        @BindView(R.id.tv_title)
        TextView tvTitle;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
