package hlz.cn.jhc.mytools.movies.agenweb;

import android.view.View;

import hlz.cn.jhc.mytools.movies.DataMovices;
import hlz.cn.jhc.mytools.movies.CommentBean;

/**
 * Created by cenxiaozhong on 2017/5/22.
 *  <p>
 *
 */

public class WebActivity extends BaseWebActivity{

    private String url = "";
    private String title = "";

    @Override
    public void initData() {
        super.initData();
        CommentBean model = getIntent().getParcelableExtra("bean");
        if (model != null) {
            url = model.getUrl();
            title = model.getTitle();
            if (url.equals(DataMovices.urls[0])){
                mTv.setVisibility(View.GONE);
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public String getTitleData() {
        return title;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //测试Cookies
        /*try {

            String targetUrl="";
            Log.i("Info","cookies:"+ AgentWebConfig.getCookiesByUrl(targetUrl="http://www.jd.com"));
            AgentWebConfig.removeAllCookies(new ValueCallback<Boolean>() {
                @Override
                public void onReceiveValue(Boolean value) {
                    Log.i("Info","onResume():"+value);
                }
            });

            String tagInfo=AgentWebConfig.getCookiesByUrl(targetUrl);
            Log.i("Info","tag:"+tagInfo);
            AgentWebConfig.syncCookie("http://www.jd.com","ID=IDHl3NVU0N3ltZm9OWHhubHVQZW1BRThLdGhLaFc5TnVtQWd1S2g1REcwNVhTS3RXQVFBQEBFDA984906B62C444931EA0");
            String tag=AgentWebConfig.getCookiesByUrl(targetUrl);
            Log.i("Info","tag:"+tag);
            AgentWebConfig.removeSessionCookies();
            Log.i("Info","removeSessionCookies:"+AgentWebConfig.getCookiesByUrl(targetUrl));
        }catch (Exception e){
            e.printStackTrace();
        }*/

    }
}
