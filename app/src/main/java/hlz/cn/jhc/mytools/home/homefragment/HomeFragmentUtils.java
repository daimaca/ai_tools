package hlz.cn.jhc.mytools.home.homefragment;

import android.content.Context;
import android.content.Intent;

import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.List;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.home.BannerActivity;
import hlz.cn.jhc.mytools.home.GlideImageLoader;
import hlz.cn.jhc.mytools.utils.Tools;

public class HomeFragmentUtils {

    public static void initBanner(Banner banner, final Context context) {
        final List<Integer> images = new ArrayList<>();
        final List<String> titles = new ArrayList<>();
        images.add(R.mipmap.banner03);
        images.add(R.mipmap.banner02);
        images.add(R.mipmap.banner01);
        String appVersion = Tools.getAppVersion();
        titles.add("已支持更新版本，可在设置“版本更新”里获取。如何知道自己是否是最新版本，查看自己的App与下载链接最高版本比较即可！");
        titles.add("欢迎使用" + appVersion + "版本！感谢对AI小志的支持，同时祝大家2019年快乐！！！\n\n爱编程 不爱Bug\n" +
                "爱加班 不爱黑眼圈\n" +
                "固执 但不偏执\n" +
                "疯狂 但不疯癫\n" +
                "生活里的菜鸟\n" +
                "工作中的大神\n" +
                "身怀宝藏，一心憧憬星辰大海\n" +
                "追求极致，目标始于高山之巅\n" +
                "一群怀揣好奇，梦想改变世界的孩子\n" +
                "一群追日逐浪，正在改变世界的极客\n" +
                "你们用最美的语言，诠释着科技的力量\n" +
                "你们用极速的创新，引领着时代的变迁");
        titles.add("咳咳咳！又老了一岁,竟然还是小编本命年，许个小愿望，今年能挣一个亿！恩，小小的愿望让我快点实现吧！");
        //设置banner样式
        banner.setBannerStyle(BannerConfig.NUM_INDICATOR_TITLE);
        //设置图片加载器
        banner.setImageLoader(new GlideImageLoader());
        //设置图片集合
        banner.setImages(images);
        //设置banner动画效果
        banner.setBannerAnimation(Transformer.DepthPage);
        //设置标题集合（当banner样式有显示title时）
        banner.setBannerTitles(titles);
        //设置自动轮播，默认为true
        banner.isAutoPlay(true);
        //设置轮播时间
        banner.setDelayTime(3000);
        //设置指示器位置（当banner模式中有指示器时）
        banner.setIndicatorGravity(BannerConfig.CENTER);
        //banner设置方法全部调用完毕时最后调用
        banner.start();

        banner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {
                BannerActivity.BannerItemData bannerItemData = new BannerActivity.BannerItemData();
                bannerItemData.setImg(images.get(position));
                bannerItemData.setTitle(titles.get(position));
                Intent intentBanner = new Intent();
                intentBanner.setClass(context, BannerActivity.class);
                intentBanner.putExtra(BannerActivity.BANNER_ITEM_DATA_KEY, bannerItemData);
                context.startActivity(intentBanner);
            }
        });
    }
}
