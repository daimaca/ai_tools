package photomovie.cn.com.game2048;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public class GameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Main2048View view = new Main2048View(this);
        setContentView(view);//加载游戏view视图

    }
}
