package hlz.cn.jhc.mytools.home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import hlz.cn.jhc.mytools.R;

public class LabelDialog extends AlertDialog {

    private Context context;
    private TextView tv_sure;
    private EditText et_label;


    public LabelDialog(Context context) {
        super(context, R.style.BottomDialogStyle);
        this.context = context;
        setBottomAnimation();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.label_dialog);
        initView();
        listener();
    }

    private void listener() {
        tv_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = et_label.getText().toString().trim();
                if (mOnSureClickListener!=null) mOnSureClickListener.sureClick(text);
            }
        });
    }

    public void setBottomAnimation(){
        // 拿到Dialog的Window, 修改Window的属性
        Window window = getWindow();
        window.getDecorView().setPadding(0, 0, 0, 0);
        // 获取Window的LayoutParams
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.width = WindowManager.LayoutParams.MATCH_PARENT;
        attributes.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        // 一定要重新设置, 才能生效
        window.setAttributes(attributes);
    }

    private void initView() {
        et_label = (EditText) findViewById(R.id.et_label);
        tv_sure = (TextView) findViewById(R.id.tv_sure);
        setCanceledOnTouchOutside(true);

        et_label.setFocusable(true);
        et_label.setFocusableInTouchMode(true);
        et_label.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        et_label.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length()==0){
                    tv_sure.setTextColor(context.getResources().getColor(R.color._999999));
                }else {
                    tv_sure.setTextColor(context.getResources().getColor(R.color.theme_06));
                }
            }
        });
    }

    @Override
    public void setOnShowListener(@Nullable OnShowListener listener) {
        super.setOnShowListener(listener);

    }

    @Override
    public void show() {
        super.show();
        et_label.setFocusable(true);
        et_label.setFocusableInTouchMode(true);
        et_label.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    @Override
    public void dismiss() {
        super.dismiss();

    }

    private OnSureClickListener mOnSureClickListener;
    public void setOnSureClickListener(OnSureClickListener mOnSureClickListener){
        this.mOnSureClickListener = mOnSureClickListener;
    }
    public interface OnSureClickListener{
        void sureClick(String text);
    }
}
