package hlz.cn.jhc.mytools.home;

import hlz.cn.jhc.mytools.base.BaseActivity;
import photomovie.cn.com.game2048.Main2048View;

public class Game2048Activity extends BaseActivity {

    @Override
    protected void setLayout() {
        Main2048View view = new Main2048View(this);
        setContentView(view);
    }

    @Override
    protected void initData() {

    }
}
