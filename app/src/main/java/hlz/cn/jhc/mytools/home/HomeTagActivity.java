package hlz.cn.jhc.mytools.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.glomadrian.grav.GravView;
import com.google.gson.Gson;
import com.meis.widget.MeiTextPathView;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.db.DBDaoUtils;
import hlz.cn.jhc.mytools.db.SharedPreferencesConst;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.greendao.TagTable;
import hlz.cn.jhc.mytools.movies.web.X5WebView;
import hlz.cn.jhc.mytools.utils.Tools;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Administrator on 2018/8/15.
 */

public class HomeTagActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks{
    private static final String TAG = "HomeActivity";
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.mTagFlowLayout)
    TagFlowLayout mTagFlowLayout;
    @BindView(R.id.floatingActionButton)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.mRelativeLayout)
    RelativeLayout mRelativeLayout;
    @BindView(R.id.mViewParent)
    RelativeLayout mViewParent;
    @BindView(R.id.mConstraintLayoutPaoPao)
    ConstraintLayout mConstraintLayoutPaoPao;

    private HomeTagAdapter mTagAdapter;

    private List<TagTable> listTag = new ArrayList<>();
    private DBDaoUtils dbDaoUtils;
    private X5WebView mWebView;


    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_home_tag);
        ButterKnife.bind(this);
        setStatusBar();
        methodRequiresTwoPermission();
    }


    @SuppressLint("RestrictedApi")
    @Override
    protected void initData() {
        back.setImageResource(R.drawable.ic_action_back);
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText("我的管理");
        dbDaoUtils = new DBDaoUtils(HomeTagActivity.this);

        initTag();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Tools.isOpenPaoPao(mConstraintLayoutPaoPao);
        addWeb();
    }

    @Override
    protected void onPause() {
        super.onPause();
        removeWeb();
        mConstraintLayoutPaoPao.removeAllViews();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeWeb();
    }

    private void initTag() {
        List<TagTable> dbTag = dbDaoUtils.getTagList();
        if (dbTag != null && dbTag.size() > 0) {
            listTag.addAll(dbTag);
        }

        //加载标签
        mTagAdapter = new HomeTagAdapter(listTag, this,dbDaoUtils);
        mTagFlowLayout.setMaxSelectCount(1);
        mTagFlowLayout.setAdapter(mTagAdapter);
    }

    @OnClick({R.id.back, R.id.floatingActionButton})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
               finish();
                break;
            case R.id.floatingActionButton:
                addLabel();
                break;
        }
    }

    private void addLabel() {
        final LabelDialog labelDialog = new LabelDialog(this);
        labelDialog.show();
        // 解决无法弹出软键盘
        labelDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        labelDialog.setOnSureClickListener(new LabelDialog.OnSureClickListener() {
            @Override
            public void sureClick(String text) {
                if (!TextUtils.isEmpty(text)){
                    if (text.length()>10){
                        Tools.showShortToast("最多10个字符！");
                        return;
                    }
                    boolean isExist = isLabelExist(text);
                    if (isExist){
                        Tools.showShortToast("标签已存在，请勿重复添加！");
                        return;
                    }
                    TagTable table = new TagTable();
                    table.setIsOwnDefined("2");
                    table.setName(text);
                    boolean s = dbDaoUtils.insert(table);
                    if (s){
                        labelDialog.dismiss();
                        Tools.showShortToast("添加成功！");
                        listTag.add(table);
                        mTagAdapter.notifyDataChanged();
                    }

                }else {
                    Tools.showShortToast("请输入标签！");
                }
            }
        });
    }

    private boolean isLabelExist(String text) {
        for (int i = 0; i < listTag.size(); i++) {
            if (text.equals(listTag.get(i).getName())){
                return true;
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    int RC_CAMERA_AND_LOCATION =1;
    @AfterPermissionGranted(1)
    private void methodRequiresTwoPermission() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.READ_PHONE_STATE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "请允许此权限，否则可能影响应用的使用",
                    RC_CAMERA_AND_LOCATION, perms);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
//        Log.i(TAG, "onPermissionsGranted: 11111111111111111111"+new Gson().toJson(perms));
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
//        Log.i(TAG, "onPermissionsDenied: 222222222222222222222222"+new Gson().toJson(perms));
    }

    private void addWeb(){
        int theme = SharedPreferencesUtils.getKeyValue(this,SharedPreferencesConst.ThemePositionKey,0);
        if (theme == 0){
//            mRelativeLayout.setBackgroundResource(R.drawable.home_bg);
            mRelativeLayout.setBackgroundResource(R.drawable.xiaozhi_bg);
            return;
        }
        mRelativeLayout.setBackgroundResource(0);
        mWebView = new X5WebView(this, null);
        mViewParent.addView(mWebView, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));

        Tools.setTheme(mWebView,theme);
    }

    private void removeWeb(){
        if (mViewParent!=null){
            mViewParent.removeView(mWebView);
        }
        if (mWebView !=null){
            mWebView.destroy();
            mWebView = null;
        }
    }
}
