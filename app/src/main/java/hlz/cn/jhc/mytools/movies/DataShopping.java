package hlz.cn.jhc.mytools.movies;

import java.util.ArrayList;
import java.util.List;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;

/**
 * todo 购物
 * Created by Administrator on 2018/6/17.
 */

public class DataShopping {
    public static String[] names = new String[]{"今日特价", "淘宝网", "天猫商城", "京东商城", "舒宁易购",
            "亚马逊", "拼多多"

    };
    public static int[] img = new int[]{
            R.mipmap.img_jinritejia, R.mipmap.img_taobao, R.mipmap.img_tianmaoshangcheng, R.mipmap.img_jingdongshangcheng, R.mipmap.img_shuningyigou,
            R.mipmap.img_yamaxun, R.mipmap.img_pingduoduo
    };

    public static String[] urls = new String[]{"http://tejia.hao123.com/",//1 今日特价
            "https://www.taobao.com/",//2 淘宝网
            "https://www.tmall.com/",//3 天猫商城
            "https://www.jd.com/?cu=true&utm_source=www.hao123.com&utm_medium=tuiguang&utm_campaign=t_1000003625_hao123mz&utm_term=a42bfa58c10245f780ee3d858a04950e",//4 京东商城
            "https://www.suning.com/?utm_source=hao123&utm_medium=mingzhan",//5 舒宁易购

            "https://www.amazon.cn/ref=z_cn",//6 亚马逊
            "https://m.pinduoduo.com/",// 拼多多
    };

    /**
     *  "http://www.1966w.com/",//20 7D影城
     */

    /**
     * todo 视频平台列表
     *
     * @return
     */

    public static List<WatchCollectionTable> getList() {
        List<WatchCollectionTable> movicesModels = new ArrayList<>();

        movicesModels = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            WatchCollectionTable model = new WatchCollectionTable();
            model.setTitle(names[i]);
            model.setUrl(urls[i]);
            model.setImg(img[i]);
            model.setIsOwnDefined("1");
            movicesModels.add(model);
        }

        return movicesModels;
    }
}
