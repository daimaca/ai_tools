package hlz.cn.jhc.mytools.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import hlz.cn.jhc.mytools.utils.Tools;

public abstract class BaseDialog extends Dialog {

    public BaseDialog(Context context) {
        super(context);
    }

    public BaseDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout();
        initView();
    }

    abstract void setLayout();
    abstract void initView();

    /**
     * 底部弹出动画
     */
    public void setBottomAnimation(){
        // 拿到Dialog的Window, 修改Window的属性
        Window window = getWindow();
        window.getDecorView().setPadding(0, 0, 0, 0);
        // 获取Window的LayoutParams
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.width = WindowManager.LayoutParams.MATCH_PARENT;
        attributes.height = WindowManager.LayoutParams.WRAP_CONTENT;
        attributes.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        // 一定要重新设置, 才能生效
        window.setAttributes(attributes);
    }

    @Override
    protected void onStart() {
        super.onStart();
//        getWindow().getAttributes().width = (int) (Tools.getWidthPixels()*0.7);
//        getWindow().getAttributes().height = (int) (Tools.getHeightPixels() * 0.5);
        getWindow().getAttributes().height = WindowManager.LayoutParams.WRAP_CONTENT;
    }
}
