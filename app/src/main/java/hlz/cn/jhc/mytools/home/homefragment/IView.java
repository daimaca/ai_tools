package hlz.cn.jhc.mytools.home.homefragment;

import android.content.Context;

import hlz.cn.jhc.mytools.rxjava.BaseView;

public interface IView extends BaseView {

    void uiUpdateApp(UpdateAppBean bean);
}
