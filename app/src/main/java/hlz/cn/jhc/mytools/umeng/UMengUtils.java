package hlz.cn.jhc.mytools.umeng;

import android.content.Context;
import android.nfc.Tag;
import android.util.Log;

import com.umeng.analytics.MobclickAgent;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.util.HashMap;
import java.util.Map;

import hlz.cn.jhc.mytools.log.LogUtils;
import hlz.cn.jhc.mytools.utils.Tools;

public class UMengUtils {

    private static final String TAG = "UMengUtils";

    /**
     * todo eventId
     */
    public static final String eventIdWatchCollectionTable = "WatchCollectionTable";//点击对应的平台
    public static final String eventIdWatchCollectionTableError = "WatchCollectionTableError";//反馈对应的出错平台
    public static final String eventIdUserFeedBack = "eventIdUserFeedBack";//用户反馈以及意见


    public static String getUMengKey(){
        return "5c2985cab465f568f30000b1";
    }

    public static String getUMengpushSecret(){
        return "aihlzmytool2018";
    }


    public static void onEvent(Context context,String s,Map<String,String> map){
        MobclickAgent.onEvent(context.getApplicationContext(),Tools.getText(s),map);
    }

    public static String getEventId(String chinese){
        String pinyinStr = "ai_";
        char[] newChar = chinese.toCharArray();
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        /*
         * 设置需要转换的拼音格式
         * 以天为例
         * HanyuPinyinToneType.WITHOUT_TONE 转换为tian
         * HanyuPinyinToneType.WITH_TONE_MARK 转换为tian1
         * HanyuPinyinVCharType.WITH_U_UNICODE 转换为tiān
         *
//         */
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (int i = 0; i < newChar.length; i++) {
            if (newChar[i] > 128){
                try {
                    pinyinStr +=  PinyinHelper.toHanyuPinyinStringArray(newChar[i], format)[0];

                } catch (BadHanyuPinyinOutputFormatCombination badHanyuPinyinOutputFormatCombination) {
                    badHanyuPinyinOutputFormatCombination.printStackTrace();
                }
            }else {
                pinyinStr += newChar[i];
            }
        }
        LogUtils.logi(TAG,"getEventId: "+pinyinStr);
        return pinyinStr;
    }
}
