package hlz.cn.jhc.mytools.zxing;

public class PickPictureBean {
    private String picPath;

    public PickPictureBean(String picPath) {
        this.picPath = picPath;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }
}
