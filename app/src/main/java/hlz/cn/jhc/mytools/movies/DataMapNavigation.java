package hlz.cn.jhc.mytools.movies;

import java.util.ArrayList;
import java.util.List;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;

/**
 * todo 地图导航
 * Created by Administrator on 2018/6/17.
 */

public class DataMapNavigation {
    public static String[] names = new String[]{"高德地图", "百度地图", "谷歌地图", "腾讯地图", "搜狗地图",
            "360地图"

    };
    public static int[] img = new int[]{
            R.mipmap.img_gaode_map, R.mipmap.img_baidu_map, R.mipmap.img_guge_map, R.mipmap.img_tengxun_map, R.mipmap.img_sougou_map,
            R.mipmap.img_360_map
    };

    public static String[] urls = new String[]{"https://www.amap.com/",//1 高德地图
            "https://map.baidu.com/",//2 百度地图
            "http://www.google.cn/maps/@35.86166,86.2657095,4z",//3 谷歌地图
            "https://map.qq.com/",//4 腾讯地图
            "http://map.sogou.com/",//5 搜狗地图
            "https://ditu.so.com/"//6 360地图
    };

    /**
     *  "http://www.1966w.com/",//20 7D影城
     */

    /**
     * todo 视频平台列表
     *
     * @return
     */

    public static List<WatchCollectionTable> getList() {
        List<WatchCollectionTable> movicesModels = new ArrayList<>();

        movicesModels = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            WatchCollectionTable model = new WatchCollectionTable();
            model.setTitle(names[i]);
            model.setUrl(urls[i]);
            model.setImg(img[i]);
            model.setIsOwnDefined("1");
            movicesModels.add(model);
        }

        return movicesModels;
    }
}
