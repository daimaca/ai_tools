package hlz.cn.jhc.mytools.comment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import hlz.cn.jhc.mytools.share.ShareUtils;

public class ShareImageThread extends Thread {

    private Context context;
    private String url;
    public ShareImageThread(Context context,String url){
        this.context = context;
        this.url = url;
    }
    @Override
    public void run() {
        Uri imageUri = ShareUtils.getImageUri(context,url);
        Log.i("shareImage", "shareImage: "+imageUri);
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        shareIntent.setType("image/*");
        context.startActivity(Intent.createChooser(shareIntent, ShareUtils.title));
    }
}
