package hlz.cn.jhc.mytools.movies.agenweb;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import hlz.cn.jhc.mytools.R;

/**
 * Created by Administrator on 2018/8/12.
 */

public class UseTypeDialog extends AlertDialog implements View.OnClickListener{
    private static String TAG = "UseTypeDialog";
    TextView tvCancel;
    TextView tvIKnow;
    private Context mContext;

    protected UseTypeDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = LayoutInflater.from(mContext).inflate(R.layout.vip_popwindows_select,null);
        setContentView(view);
        setCanceledOnTouchOutside(true);

        // 去掉四个黑角
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        // 禁止物理返回键
//        this.setOnKeyListener(new OnKeyListener() {
//            @Override
//            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
//                if (keyCode == KeyEvent.KEYCODE_BACK&&event.getRepeatCount()==0) {
//                    return true;
//                }else {
//                    return false;
//                }
//            }
//        });
        tvCancel  = (TextView) findViewById(R.id.tv_cancel);
        tvIKnow  = (TextView) findViewById(R.id.tv_i_know);
        tvCancel.setOnClickListener(this);
        tvIKnow.setOnClickListener(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;         // 屏幕宽度（像素）560*694;  0.8
        int height = dm.heightPixels;
        getWindow().getAttributes().width = (int) mContext.getResources().getDimension(R.dimen.dp_310);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_cancel:
                if (mOnUseTypeDialogListener != null) {
                    mOnUseTypeDialogListener.onComputer();
                }
                break;
            case R.id.tv_i_know:
                if (mOnUseTypeDialogListener != null) {
                    mOnUseTypeDialogListener.onPhone();
                }
                break;
        }
    }

    private OnUseTypeDialogListener mOnUseTypeDialogListener;

    public void setUseTypeDialogListener(OnUseTypeDialogListener mOnUseTypeDialogListener) {
        this.mOnUseTypeDialogListener = mOnUseTypeDialogListener;
    }

    interface OnUseTypeDialogListener {
        void onComputer();
        void onPhone();
    }
}
