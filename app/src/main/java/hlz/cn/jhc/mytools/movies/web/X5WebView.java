package hlz.cn.jhc.mytools.movies.web;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.MutableContextWrapper;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tencent.smtt.export.external.interfaces.IX5WebSettings;
import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.URLUtil;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebSettings.LayoutAlgorithm;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.ExecutionException;

import hlz.cn.jhc.mytools.dialog.ImageOperationDialog;
import hlz.cn.jhc.mytools.utils.Tools;

import static com.umeng.commonsdk.framework.UMModuleRegister.getAppContext;

public class X5WebView extends WebView implements View.OnLongClickListener{
    private static final String APP_NAME_UA = "自己定义的UA";
    TextView title;

	private WebViewClient client = new WebViewClient() {
		/**
		 * 防止加载网页时调起系统浏览器
		 */
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	};

	@SuppressLint("SetJavaScriptEnabled")
	public X5WebView(Context arg0, AttributeSet arg1) {
		super(arg0, arg1);
		this.setWebViewClient(client);
		// this.setWebChromeClient(chromeClient);
		// WebStorage webStorage = WebStorage.getInstance();


		setLayerType(View.LAYER_TYPE_NONE, null);
		setDrawingCacheEnabled(true);

		initWebViewSettings();
		this.getView().setClickable(true);
		setOnLongClickListener(this);
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void initWebViewSettings() {
        WebSettings webSetting = this.getSettings();
//        webSetting.setBlockNetworkImage(true);
        webSetting.setJavaScriptEnabled(true);
        webSetting.setJavaScriptCanOpenWindowsAutomatically(true);
        webSetting.setAllowFileAccess(true);
        webSetting.setLayoutAlgorithm(LayoutAlgorithm.NARROW_COLUMNS);
        webSetting.setSupportZoom(true);
        webSetting.setBuiltInZoomControls(true);
        webSetting.setUseWideViewPort(true);
        webSetting.setSupportMultipleWindows(true);
        // webSetting.setLoadWithOverviewMode(true);
        webSetting.setAppCacheEnabled(true);
         webSetting.setDatabaseEnabled(true);//---
        webSetting.setDomStorageEnabled(true);
        webSetting.setGeolocationEnabled(true);
        webSetting.setAppCacheMaxSize(Long.MAX_VALUE);

//         webSetting.setPageCacheCapacity(IX5WebSettings.DEFAULT_CACHE_CAPACITY);
        webSetting.setPluginState(WebSettings.PluginState.ON_DEMAND);
         webSetting.setRenderPriority(WebSettings.RenderPriority.HIGH);//--渲染优先级
        webSetting.setCacheMode(WebSettings.LOAD_DEFAULT);//--

//         this.getSettingsExtension().setPageCacheCapacity(IX5WebSettings.DEFAULT_CACHE_CAPACITY);//extension
        // settings 的设计
		webSetting.setAllowUniversalAccessFromFileURLs(true);
		webSetting.setAllowFileAccessFromFileURLs(true);
		webSetting.setLoadWithOverviewMode(true);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			webSetting.setMixedContentMode(android.webkit.WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
		}
	}

//	@Override
//	protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
//		boolean ret = super.drawChild(canvas, child, drawingTime);
//		canvas.save();
//		Paint paint = new Paint();
//		paint.setColor(0x7fff0000);
//		paint.setTextSize(24.f);
//		paint.setAntiAlias(true);
//		if (getX5WebViewExtension() != null) {
//			canvas.drawText(this.getContext().getPackageName() + "-pid:"
//					+ android.os.Process.myPid(), 10, 50, paint);
//			canvas.drawText(
//					"X5  Core:" + QbSdk.getTbsVersion(this.getContext()), 10,
//					100, paint);
//		} else {
//			canvas.drawText(this.getContext().getPackageName() + "-pid:"
//					+ android.os.Process.myPid(), 10, 50, paint);
//			canvas.drawText("Sys Core", 10, 100, paint);
//		}
//		canvas.drawText(Build.MANUFACTURER, 10, 150, paint);
//		canvas.drawText(Build.MODEL, 10, 200, paint);
//		canvas.restore();
//		return ret;
//	}

	public X5WebView(Context arg0) {
		super(arg0);
        setLayerType(View.LAYER_TYPE_NONE, null);
        setDrawingCacheEnabled(true);
		setBackgroundColor(85621);
		setOnLongClickListener(this);
	}

	@Override
	public boolean onLongClick(View view) {
		//https://www.jianshu.com/p/44f971744cb0
		Log.i("onLongClick", "onLongClick: ---x5---");
			WebView.HitTestResult result = getHitTestResult();
			if (null == result)
				return false;
			int type = result.getType();
		Log.i("onLongClick", "onLongClick: ---x5-type--"+type);
		Log.i("onLongClick", "onLongClick: ---x5--result--"+new Gson().toJson(result));
			switch (type) {
				case WebView.HitTestResult.EDIT_TEXT_TYPE: // 选中的文字类型
					break;
				case WebView.HitTestResult.PHONE_TYPE: // 处理拨号
					break;
				case WebView.HitTestResult.EMAIL_TYPE: // 处理Email
					break;
				case WebView.HitTestResult.GEO_TYPE: // 　地图类型
					break;
				case WebView.HitTestResult.SRC_ANCHOR_TYPE: // 超链接
					break;
				case WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE: // 带有链接的图片类型
				case WebView.HitTestResult.IMAGE_TYPE: // 处理长按图片的菜单项
					String url = result.getExtra();
					if (url != null && URLUtil.isValidUrl(url)) {
//						downloadImage(url);
						ImageOperationDialog dialog = new ImageOperationDialog(getContext(),url);
						dialog.show();
					}
					return true;
				case WebView.HitTestResult.UNKNOWN_TYPE: //未知
					break;
			}
			return false;
	}

	private void downloadImage(final String url) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
		builder.setTitle("是否下载到相册？")
				.setNegativeButton("确定", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Tools.showShortToast("已在后台下载中，请到相册擦看！");
						new DownLoadImageThread(getContext(),url).start();
					}
				}).setPositiveButton("取消",null).show();

	}


	public static class DownLoadImageThread extends Thread{
		private Context context;
		private String url;
		public DownLoadImageThread(Context context,String url){
			this.context = context.getApplicationContext();
			this.url = url;
		}
		@Override
		public void run() {
			File file = null;
			try {
				file = Glide.with(context).asFile().load(url).submit().get();
				displayToGallery(context,file);
			} catch (ExecutionException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	//将图片保存在相册：
	public static void displayToGallery(Context context, File photoFile) {
		if (photoFile == null || !photoFile.exists()) {
			return;
		}
		String photoPath = photoFile.getAbsolutePath();
		String photoName = photoFile.getName();
		// 把文件插入到系统图库
		try {
			ContentResolver contentResolver = context.getContentResolver();
			MediaStore.Images.Media.insertImage(contentResolver, photoPath, photoName, null);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		// 最后通知图库更新
		context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + photoPath)));
	}


//	/**
//	 * 创建WebView实例
//	 * 用了applicationContext
//	 */
//	@DebugTrace
//	public void prepareNewWebView() {
//		if (mCachedWebViewStack.size() < CACHED_WEBVIEW_MAX_NUM) {
//			mCachedWebViewStack.push(new WebView(new MutableContextWrapper(getAppContext())));
//		}
//	}
//	/**
//	 * 从缓存池中获取合适的WebView
//	 *
//	 * @param context activity context
//	 * @return WebView
//	 */
//	private WebView acquireWebViewInternal(Context context) {
//		// 为空，直接返回新实例
//		if (mCachedWebViewStack == null || mCachedWebViewStack.isEmpty()) {
//			return new WebView(context);
//		}
//		WebView webView = mCachedWebViewStack.pop();
//		// webView不为空，则开始使用预创建的WebView,并且替换Context
//		MutableContextWrapper contextWrapper = (MutableContextWrapper) webView.getContext();
//		contextWrapper.setBaseContext(context);
//		return webView;
//	}

}
