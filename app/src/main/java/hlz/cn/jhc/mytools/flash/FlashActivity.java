package hlz.cn.jhc.mytools.flash;

import android.Manifest;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.home.HomeActivity;
import hlz.cn.jhc.mytools.home.HomePageActivity;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class FlashActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks{
    @BindView(R.id.tv_time)
    TextView tv_time;
    @BindView(R.id.tv_start)
    TextView tv_start;
    @BindView(R.id.mConstraintLayout)
    ConstraintLayout mConstraintLayout;
    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.mLottieAnimationView)
    LottieAnimationView mLottieAnimationView;

    private Timer timer;
    private int TIME = 5;
    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_flash2);
        ButterKnife.bind(this);
//        setLogo();
    }

    private void setAnimation() {
        int r = (int) (Math.random() * 4);
        String json = null;
        try {
            json = getRawString(r);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mLottieAnimationView.setAnimationFromJson(json,null);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLottieAnimationView.playAnimation();
            }
        });

    }

    private String getRawString(int ramdom) throws IOException {
        String json = null;
        StringBuffer buffer = new StringBuffer();
        InputStream inputStream = null;
        switch (ramdom)
        {
            case 0:
                inputStream = getResources().openRawResource(R.raw.androidwave);
                break;
            case 1:
                inputStream = getResources().openRawResource(R.raw.lottielogo1);
                break;
            case 2:
                inputStream = getResources().openRawResource(R.raw.lottielogo2);
                break;
            case 3:
                inputStream = getResources().openRawResource(R.raw.walkthrough);
                break;
        }

        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        BufferedReader reader = new BufferedReader(inputStreamReader);
        StringBuffer sb = new StringBuffer("");
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
//                sb.append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    @Override
    protected void initData() {
//        Animation animation = AnimationUtils.loadAnimation(FlashActivity.this,R.anim.flash_animation);
//        animation.setInterpolator(new LinearInterpolator());
//        logo.startAnimation(animation);

        new Thread(new Runnable() {
            @Override
            public void run() {
                setAnimation();
            }
        }).start();

        if (timer == null){
            timer = new Timer();
        }

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tv_time.setText(TIME+"秒");
                        if (TIME == 0){
                            tv_time.setVisibility(View.GONE);
                            tv_start.setVisibility(View.VISIBLE);
                            if (timer!=null){
                                timer.cancel();
                                timer.purge();
                                timer = null;
                            }
                            methodRequiresTwoPermission();
                        }else if (TIME == 3){
                            tv_start.setVisibility(View.VISIBLE);
                        }
                        TIME--;
                    }
                });
            }
        },0,1000);

        tv_start.setVisibility(View.GONE);
        tv_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                methodRequiresTwoPermission();
            }
        });
        tv_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                methodRequiresTwoPermission();
            }
        });


    }

    int RC_CAMERA_AND_LOCATION =1;
    @AfterPermissionGranted(1)
    private void methodRequiresTwoPermission() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.READ_PHONE_STATE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
            startActivity(new Intent(FlashActivity.this, HomePageActivity.class));
            finish();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "请允许此权限，否则可能影响应用的使用，请确定！",
                    RC_CAMERA_AND_LOCATION, perms);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer!=null){
            timer.cancel();
            timer.purge();
            timer = null;
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        startActivity(new Intent(FlashActivity.this,HomeActivity.class));
        finish();
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }
}
