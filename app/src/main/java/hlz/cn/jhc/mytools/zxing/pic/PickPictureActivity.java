package hlz.cn.jhc.mytools.zxing.pic;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.eventbus.EventBusContst;
import hlz.cn.jhc.mytools.eventbus.EventBusUtils;
import hlz.cn.jhc.mytools.eventbus.EventMessage;
import hlz.cn.jhc.mytools.zxing.PickPictureBean;

/**
 * 手机图片列表中的详细
 * Created by hupei on 2016/7/7.
 */
public class PickPictureActivity extends BaseActivity {
    private GridView mGridView;
    private List<String> mList;//此相册下所有图片的路径集合
    private PickPictureAdapter mAdapter;
    private ProgressBar mProgressBar;

    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_pick_picture);
        EventBusUtils.register(this);
        mGridView = (GridView) findViewById(R.id.child_grid);
        mProgressBar = findViewById(R.id.mProgressBar);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setResult(mList.get(position));
            }
        });
        processExtraData();
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBusUtils.unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN,sticky = true)
    public void finishActivity(EventMessage message) {
        if (message!=null){
            if (message.getMessageId().equals(EventBusContst.CMD_FINISH_ACTIVITY)){
                finish();
            }
        }
        EventBusUtils.removePostSticky(message);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        processExtraData();
    }

    private void processExtraData() {
        Bundle extras = getIntent().getExtras();
        if (extras == null) return;
        mList = extras.getStringArrayList("data");
        if (mList.size() > 1) {
            SortPictureList sortList = new SortPictureList();
            Collections.sort(mList, sortList);
        }
        mAdapter = new PickPictureAdapter(this, mList);
        mGridView.setAdapter(mAdapter);
    }

    private void setResult(String picturePath) {
        mProgressBar.setVisibility(View.VISIBLE);
        EventBusUtils.postStickyObject(new PickPictureBean(picturePath));

//        Intent intent = new Intent();
//        intent.putExtra(PickPictureTotalActivity.EXTRA_PICTURE_PATH, picturePath);
//        setResult(Activity.RESULT_OK, intent);
//        finish();
    }

    public static void gotoActivity(Activity activity, ArrayList<String> childList) {
        Intent intent = new Intent(activity, PickPictureActivity.class);
        intent.putStringArrayListExtra("data", childList);
        activity.startActivityForResult(intent, PickPictureTotalActivity.REQUEST_CODE_SELECT_ALBUM);
    }
}
