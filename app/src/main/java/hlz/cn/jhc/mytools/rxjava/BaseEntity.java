package hlz.cn.jhc.mytools.rxjava;

import com.google.gson.annotations.SerializedName;

public class BaseEntity<E> {
    @SerializedName(value = "status", alternate = {"status"})
    private String status;// 1成功，0失败

    @SerializedName(value = "errCode", alternate = {"errcode","err_code", "code"})
    private String errCode;// 错误编码，200表示成功

    @SerializedName(value = "errMsg", alternate = {"errMsg","message"})
    private String errMsg;// 错误信息提示

    @SerializedName(value = "content", alternate = {"content","data"})
    private E content;// 返回数据，错误时返回错误信息

    /**
     * 标记请求是否成功
     * @return
     */
    public boolean isSuccess(){
        return errCode.equals("200");
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public E getContent() {
        return content;
    }

    public void setContent(E content) {
        this.content = content;
    }

    public BaseEntity(String status, String errCode, String errMsg, E content) {
        this.status = status;
        this.errCode = errCode;
        this.errMsg = errMsg;
        this.content = content;
    }
}
