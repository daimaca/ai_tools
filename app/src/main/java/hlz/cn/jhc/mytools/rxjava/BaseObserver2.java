package hlz.cn.jhc.mytools.rxjava;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.just.agentweb.LogUtils;

import hlz.cn.jhc.mytools.application.MyApplication;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

public abstract class BaseObserver2<T> extends DisposableObserver<T> {
    private Disposable mDisposable;
    private Context mContext;
    private static final String TAG = "BaseObserver";

    protected BaseObserver2(Context context) {
        this.mContext = context.getApplicationContext();
        MyApplication.mCompositeDisposable.add(this);
    }


    @Override
    public void onNext(T t) {
        Log.i(TAG, "onNext: "+new Gson().toJson(t));
        onHandleSuccess(t);
    }

    @Override
    public void onError(Throwable e) {
        // 属于接口通了情况下的异常

        // 属于服务器报错或404等类似的异常
        LogUtils.i(TAG, "onError: " + e.toString());
        onHandleError("数据出错！");
        MyApplication.mCompositeDisposable.remove(this);
    }

    @Override
    public void onComplete() {
        LogUtils.i(TAG, "onComplete: ====================>>>>>>>>>>>>>");
    }

    protected void cancelDisposable() {
        if (!mDisposable.isDisposed())
            mDisposable.dispose();
    }

    protected abstract void onHandleSuccess(T t);

    protected abstract void onHandleError(String msg);
}