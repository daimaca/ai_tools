package hlz.cn.jhc.mytools.movies.web;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/12/23.
 */

public class DataVipUrl {

    private static String[] names = new String[]{"无广告（推荐）","无广告（推荐2）","无广告（推荐3）","无广告（推荐4）",
            "通用稳定","超稳定","牛巴巴解析","古思解析",
            "万能接口1","万能接口2","万能接口3", "爱奇艺稳定",
            "腾讯稳定","腾讯稳定2", "乐视稳定","芒果tv稳定","芒果tv稳定2",
            "芒果tv稳3","优酷稳定","土豆稳定","搜狐视频",
            "搜狐视频2","PPTV解析", "稳定","加速",
    };
    //
    public static String[] urls = new String[]{
            "http://chujian.xiaoyule-app.cn?url=",
            "http://v.mu88f.cn/?url=",
            "http://www.wocao.xyz/index.php?url=",
            "http://jx.w9w6.cn/?url=",

            "http://jx.aeidu.cn/index.php?url=",
            "http://jx.du2.cc/jx6.php?url=",
            "http://jiexi.071811.cc/jx2.php?url=",
            "http://mv.688ing.com/player?url=",

            "https://www.jqaaa.com/jq1/?url=",
            "http://jqaaa.com/jx.php?url=",
            "http://jiexi.071811.cc/jx2.php?url=",
            "http://jx.aeidu.cn/v/4.php?url=",

            "http://jx.618ge.com/?url=",
            "https://www.jqaaa.com/jq1/?url=",
            "http://jx.598110.com/?url=",
            "http://jx.598110.com/?url=",
            "http://jx.ovov.cc/?url=",

            "http://jx.618ge.com/?url=",
            "http://jx.aeidu.cn/v/5.php?url=",
            "http://jx.aeidu.cn/v/3.php?url=",
            "http://jx.aeidu.cn/v/2.php?url=",

            "http://vip.jlsprh.com/index.php?url=",
            "http://jx.aeidu.cn/v/1.php?url=",
            "http://jx.du2.cc/jx3.php?url=",
            "http://jx.du2.cc/?url=",

            };

    public static List<VipUrlBean> getListData(int currentPosition){
        List<VipUrlBean> list = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            VipUrlBean bean = new VipUrlBean();
            bean.setName(names[i]);
            bean.setImg(0);
            bean.setUrl(urls[i]);
            if (i==currentPosition){
                bean.setIsSelect("1");
            }else {
                bean.setIsSelect("0");
            }
            list.add(bean);
        }
        return list;
    }

    public static class VipUrlBean{
        String url;
        String name;
        String isSelect;
        int img;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIsSelect() {
            return isSelect;
        }

        public void setIsSelect(String isSelect) {
            this.isSelect = isSelect;
        }

        public int getImg() {
            return img;
        }

        public void setImg(int img) {
            this.img = img;
        }
    }
}
