package hlz.cn.jhc.mytools.movies;

import java.util.ArrayList;
import java.util.List;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;

/**
 * todo 音乐
 * Created by Administrator on 2018/6/17.
 */

public class DataMusic {
    public static String[] names = new String[]{
            "网易云音乐", "虾米音乐", "QQ音乐", "5Sing", "酷狗音乐",
            "千千音乐", "九天音乐","疯狂音乐搜索","酷我音乐"
    };
    public static int[] img = new int[]{
            R.mipmap.img_wangyiyunmusic, R.mipmap.img_xiamimusic, R.mipmap.img_qqmusic, R.mipmap.img_5sing, R.mipmap.img_kugoumusic,
            R.mipmap.img_qianqianmusic, R.mipmap.img_jiutianmusic,R.mipmap.img_fengkuangmusicsearch,0
    };

    public static String[] urls = new String[]{
            "https://music.163.com/",//1
            "https://www.xiami.com/",//2
            "https://y.qq.com/",//3
            "http://5sing.kugou.com/index.html",//4
            "http://m.kugou.com/",//5

            "http://music.taihe.com/",//6
            "http://www.9sky.com/",//
            "http://music.ifkdy.com/",//
            "http://www.kuwo.cn/",//
    };

    /**
     *  "http://www.1966w.com/",//20 7D影城
     */

    /**
     * todo 视频平台列表
     *
     * @return
     */

    public static List<WatchCollectionTable> getList() {
        List<WatchCollectionTable> movicesModels = new ArrayList<>();

        movicesModels = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            WatchCollectionTable model = new WatchCollectionTable();
            model.setTitle(names[i]);
            model.setUrl(urls[i]);
            model.setImg(img[i]);
            model.setIsOwnDefined("1");
            movicesModels.add(model);
        }

        return movicesModels;
    }
}
