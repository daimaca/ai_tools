package hlz.cn.jhc.mytools.movies;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2018/6/17.
 */

public class CommentBean implements Parcelable{
    private String title;
    private String url;
    private int img;
    private String imgUrl;

    public CommentBean(){}

    protected CommentBean(Parcel in) {
        title = in.readString();
        url = in.readString();
        img = in.readInt();
        imgUrl = in.readString();
    }

    public static final Creator<CommentBean> CREATOR = new Creator<CommentBean>() {
        @Override
        public CommentBean createFromParcel(Parcel in) {
            return new CommentBean(in);
        }

        @Override
        public CommentBean[] newArray(int size) {
            return new CommentBean[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(url);
        dest.writeInt(img);
        dest.writeString(imgUrl);
    }
}
