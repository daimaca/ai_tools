package hlz.cn.jhc.mytools.home.homefragment;

import android.support.v4.os.IResultReceiver;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import hlz.cn.jhc.mytools.R;

public class SearchMoviesAdapter extends BaseQuickAdapter<SearchMoviesBean, SearchMoviesAdapter.ViewHolder> {

    private List<SearchMoviesBean> list;

    public SearchMoviesAdapter(List<SearchMoviesBean> list) {
        super(R.layout.search_movies_item, list);
        this.list = list;
    }

    @Override
    protected void convert(final SearchMoviesAdapter.ViewHolder helper, final SearchMoviesBean item) {
        if (item != null) {
            if (helper.getAdapterPosition() == list.size() - 1) {
                helper.mView.setVisibility(View.GONE);
            }
            helper.tvTitle.setText(item.getTitle());
            if (item.getIsSelect().equals("1")) {
                helper.mBottomView.setVisibility(View.VISIBLE);
                helper.mBottomView.setBackgroundColor(mContext.getResources().getColor(R.color.et_color));
                helper.tvTitle.setTextColor(mContext.getResources().getColor(R.color.et_color));
            } else {
                helper.mBottomView.setVisibility(View.INVISIBLE);
                helper.mBottomView.setBackgroundColor(mContext.getResources().getColor(R.color._666666));
                helper.tvTitle.setTextColor(mContext.getResources().getColor(R.color._666666));
            }
        }

        helper.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < list.size(); i++) {
                    if (helper.getAdapterPosition() == i) {
                        list.get(i).setIsSelect("1");
                    } else {
                        list.get(i).setIsSelect("0");
                    }
                }
                notifyDataSetChanged();
                if (mOnSearchMoviesListener != null) {
                    mOnSearchMoviesListener.onSearchMovies(item);
                }
            }
        });
    }

    class ViewHolder extends BaseViewHolder {
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.mView)
        View mView;
        @BindView(R.id.mBottomView)
        View mBottomView;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private OnSearchMoviesListener mOnSearchMoviesListener;

    public void setOnSearchMoviesListener(OnSearchMoviesListener mOnSearchMoviesListener) {
        this.mOnSearchMoviesListener = mOnSearchMoviesListener;
    }

    interface OnSearchMoviesListener {
        void onSearchMovies(SearchMoviesBean searchMoviesBean);
    }
}
