package hlz.cn.jhc.mytools.home.minefragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseFragment;
import hlz.cn.jhc.mytools.db.SharedPreferencesConst;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.settings.AboutActivity;
import hlz.cn.jhc.mytools.login.LoginActivity;
import hlz.cn.jhc.mytools.settings.FeedBackActivity;
import hlz.cn.jhc.mytools.settings.SettingsActivity;
import hlz.cn.jhc.mytools.utils.Tools;

public class MineFragment extends BaseFragment {


    private static volatile MineFragment mineFragment;

    Unbinder unbinder;
    @BindView(R.id.mLeftMenuCircleImageView)
    CircleImageView mLeftMenuCircleImageView;
    @BindView(R.id.head_layout)
    RelativeLayout headLayout;
    @BindView(R.id.ll_about)
    LinearLayout llAbout;
    @BindView(R.id.iv_img)
    CircleImageView ivImg;
    @BindView(R.id.ll_freeback)
    LinearLayout llFreeback;
    @BindView(R.id.ll_setting)
    LinearLayout llSetting;
    @BindView(R.id.mSwitchGetResource)
    Switch mSwitchGetResource;


    @SuppressLint("ValidFragment")
    public MineFragment() {
    }

    public static MineFragment netInstance() {
        Bundle bundle = new Bundle();
        MineFragment mineFragment = new MineFragment();
        mineFragment.setArguments(bundle);
        return mineFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_mine, container, false);
        unbinder = ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    protected void initData() {

        if (Math.random() > 0.5) {
            mLeftMenuCircleImageView.setImageResource(R.mipmap.head02);
        } else {
            mLeftMenuCircleImageView.setImageResource(R.mipmap.head03);
        }
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);
        LinearInterpolator lin = new LinearInterpolator();
        animation.setInterpolator(lin);
        mLeftMenuCircleImageView.setAnimation(animation);

        mSwitchGetResource.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferencesUtils.setKeyValue(SharedPreferencesConst.isGetResourceKey, b);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mSwitchGetResource.setChecked(Tools.isGetResource());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.ll_freeback, R.id.ll_about, R.id.mLeftMenuCircleImageView, R.id.ll_setting})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_freeback:
                startActivity(new Intent(getContext(),FeedBackActivity.class));
                break;
            case R.id.ll_about:
                startActivity(new Intent(getContext(),AboutActivity.class));
                break;
            case R.id.mLeftMenuCircleImageView:
                startActivity(new Intent(getContext(), LoginActivity.class));
                break;
            case R.id.ll_setting:
                startActivity(new Intent(getContext(), SettingsActivity.class));
                break;
        }
    }
}
