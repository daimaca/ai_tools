package hlz.cn.jhc.mytools.rxjava.test;

import java.util.HashMap;

public interface Presenter {

    void test(HashMap<String,String> map);
}
