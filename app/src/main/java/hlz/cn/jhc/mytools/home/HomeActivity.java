package hlz.cn.jhc.mytools.home;

import android.Manifest;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.meis.widget.MeiTextPathView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.tencent.smtt.sdk.ValueCallback;
import com.tencent.smtt.utils.TbsLog;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.db.SharedPreferencesConst;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.greendao.TagTable;
import hlz.cn.jhc.mytools.jpush.JPushUtil;
import hlz.cn.jhc.mytools.jpush.TagAliasOperatorHelper;
import hlz.cn.jhc.mytools.movies.web.X5WebView;
import hlz.cn.jhc.mytools.login.LoginActivity;
import hlz.cn.jhc.mytools.utils.Tools;
import hlz.cn.jhc.mytools.zxing.ScanActivity;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Administrator on 2018/8/15.
 */

public class HomeActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks,HomeScrollView.TranslucentListener{
    private static final String TAG = "HomeActivity";
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.rlv_list)
    RecyclerView rlvList;
    @BindView(R.id.rlv_left_menu_list)
    RecyclerView rlvLeftMenuList;
    @BindView(R.id.mLeftMenuCircleImageView)
    CircleImageView mLeftMenuCircleImageView;
    @BindView(R.id.mDrawerLayout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.mMeiTextPathView)
    MeiTextPathView mMeiTextPathView;
    @BindView(R.id.banner)
    Banner banner;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.mHomeScrollView)
    HomeScrollView mHomeScrollView;
    @BindView(R.id.rl_alpha)
    RelativeLayout rl_alpha;
    @BindView(R.id.mFloatingActionButton)
    FloatingActionButton mFloatingActionButton;
    @BindView(R.id.iv_right_img)
    ImageView ivRightImg;

    @BindView(R.id.mViewParent)
    RelativeLayout mViewParent;
    @BindView(R.id.mSmartRefreshLayout)
    SmartRefreshLayout mSmartRefreshLayout;
//    @BindView(R.id.mWebView)
//    X5WebView mWebView;

    private static int adminCount = 0;

    private ValueCallback<Uri> uploadFile;
    private HomeAdapter homeAdapter;

    private List<TagTable> homeBeans = new ArrayList<>();
    private X5WebView mWebView;


    @Override
    protected void setLayout() {
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 11) {
                getWindow()
                        .setFlags(
                                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
            }
        } catch (Exception e) {
        }

        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setStatusBar();
        methodRequiresTwoPermission();
    }


    @Override
    protected void initData() {
        rl_alpha.setAlpha(0);
//        mMeiTextPathView.setVisibility(View.VISIBLE);
        back.setImageResource(R.drawable.user);
        tvTitle.setText("AI小志");
        ivRightImg.setVisibility(View.VISIBLE);
        mHomeScrollView.setOnScrollChangedListener(this);

        setAlias(JPushUtil.getDeviceId(this));//设置别名

        initBanner();
        initHomeData();
        initLeftMenu();

//        initWeb();
//        setListener();

    }

    /**
     * todo 极光设置别名
     */
    int sequence = 1;
    private void setAlias(String aliasStr) {
        //CarstewardId 车管家id作为用户别名
        Log.i(TAG, "setAlias: "+ aliasStr);
        if (JPushUtil.isValidTagAndAlias(aliasStr)) {
            //JPush 设置Alias
            TagAliasOperatorHelper.TagAliasBean aliasBean = new TagAliasOperatorHelper.TagAliasBean();
            aliasBean.setAlias(aliasStr);
            aliasBean.setAction(TagAliasOperatorHelper.ACTION_SET);
            aliasBean.setAliasAction(true);
            sequence++;
            TagAliasOperatorHelper.getInstance().handleAction(this,sequence,aliasBean);
        }
    }

    private void setListener() {
        mSmartRefreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                mSmartRefreshLayout.finishLoadMore(2000);
                if (mWebView!=null){

                }
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mSmartRefreshLayout.finishRefresh(2000);
                if (mWebView!=null){

                }
            }
        });
    }

    private void addWeb(){
        int theme = SharedPreferencesUtils.getKeyValue(this,SharedPreferencesConst.ThemePositionKey,0);
        if (theme == 0){
            mHomeScrollView.setBackgroundResource(R.drawable.home_bg);
            return;
        }
        mHomeScrollView.setBackgroundResource(0);
        mWebView = new X5WebView(this, null);
        mViewParent.addView(mWebView, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));

        Tools.setTheme(mWebView,theme);
    }

    private void removeWeb(){
        if (mViewParent!=null){
            mViewParent.removeView(mWebView);
        }
        if (mWebView!=null){
            mWebView.destroy();
            mWebView = null;
        }
    }


    private void initBanner() {
        final List<Integer> images = new ArrayList<>();
        final List<String> titles = new ArrayList<>();
        images.add(R.mipmap.banner03);
        images.add(R.mipmap.banner02);
        images.add(R.mipmap.banner01);
        String appVersion = Tools.getAppVersion();
        titles.add("已支持更新版本，可在设置“版本更新”里获取。如何知道自己是否是最新版本，查看自己的App与下载链接最高版本比较即可！\n1.新增设置主题。\n2.新增解析平台。\n3.解析方式的优化。\n4.更新下载优化。");
        titles.add("欢迎使用" + appVersion + "版本！感谢对AI小志的支持，同时祝大家2019年快乐！！！\n\n爱编程 不爱Bug\n" +
                "爱加班 不爱黑眼圈\n" +
                "固执 但不偏执\n" +
                "疯狂 但不疯癫\n" +
                "生活里的菜鸟\n" +
                "工作中的大神\n" +
                "身怀宝藏，一心憧憬星辰大海\n" +
                "追求极致，目标始于高山之巅\n" +
                "一群怀揣好奇，梦想改变世界的孩子\n" +
                "一群追日逐浪，正在改变世界的极客\n" +
                "你们用最美的语言，诠释着科技的力量\n" +
                "你们用极速的创新，引领着时代的变迁");
        titles.add("咳咳咳！又老了一岁,竟然还是小编本命年，许个小愿望，今年能挣一个亿！恩，小小的愿望让我快点实现吧！");




        //设置banner样式
        banner.setBannerStyle(BannerConfig.NUM_INDICATOR_TITLE);
        //设置图片加载器
        banner.setImageLoader(new GlideImageLoader());
        //设置图片集合
        banner.setImages(images);
        //设置banner动画效果
        banner.setBannerAnimation(Transformer.DepthPage);
        //设置标题集合（当banner样式有显示title时）
        banner.setBannerTitles(titles);
        //设置自动轮播，默认为true
        banner.isAutoPlay(true);
        //设置轮播时间
        banner.setDelayTime(3000);
        //设置指示器位置（当banner模式中有指示器时）
        banner.setIndicatorGravity(BannerConfig.CENTER);
        //banner设置方法全部调用完毕时最后调用
        banner.start();

        banner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {
                BannerActivity.BannerItemData bannerItemData = new BannerActivity.BannerItemData();
                bannerItemData.setImg(images.get(position));
                bannerItemData.setTitle(titles.get(position));
                Intent intentBanner = new Intent();
                intentBanner.setClass(getApplicationContext(), BannerActivity.class);
                intentBanner.putExtra(BannerActivity.BANNER_ITEM_DATA_KEY, bannerItemData);
                startActivity(intentBanner);
            }
        });
    }



    private void initLeftMenu() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate);
        LinearInterpolator lin = new LinearInterpolator();
        animation.setInterpolator(lin);
        mLeftMenuCircleImageView.setAnimation(animation);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        rlvLeftMenuList.setLayoutManager(manager);
        rlvLeftMenuList.setAdapter(new LeftMenuAdapter(this));
        Tools.setListAnimation(this, rlvLeftMenuList);
    }

    private void initHomeData() {

        homeBeans.addAll(DataModel.getListHome());
        GridLayoutManager manager = new GridLayoutManager(this,4);
        homeAdapter = new HomeAdapter(homeBeans);
        rlvList.setLayoutManager(manager);
        rlvList.setAdapter(homeAdapter);
        Tools.setListAnimation(this,rlvList);

    }

    //如果你需要考虑更好的体验，可以这么操作
    @Override
    protected void onStart() {
        super.onStart();
        //开始轮播
        banner.startAutoPlay();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //结束轮播
        banner.stopAutoPlay();
    }

    @Override
    protected void onResume() {
        super.onResume();
        addWeb();
    }

    @Override
    protected void onPause() {
        super.onPause();
        adminCount = 0;
        removeWeb();
    }

    @OnClick({R.id.back,R.id.mFloatingActionButton,R.id.iv_right_img,R.id.mLeftMenuCircleImageView})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                mDrawerLayout.openDrawer(Gravity.LEFT);
                Tools.setListAnimation(this, rlvLeftMenuList);
                break;
            case R.id.mFloatingActionButton:
                finish();

//                        String url = "https://gss3.bdstatic.com/7Po3dSag_xI4khGkpoWK1HF6hhy/baike/whfpf%3D1024%2C1024%2C50/sign=229855dfab86c91708560179af0041ff/f31fbe096b63f624dc3288248b44ebf81a4ca3ae.jpg";
//                        String url = "http://www.iqiyipic.com/common/fix/h5-aura/iqiyi-logo.png";
//                        ShareUtils.shareFightImages(HomeActivity.this,url,500);

//                startActivity(new Intent(this,TestActivity.class));
                break;
            case R.id.iv_right_img:
                startActivity(new Intent(this, ScanActivity.class));
                break;
            case R.id.mLeftMenuCircleImageView:
                adminCount++;
                if (adminCount >= 15) {
                    startActivity(new Intent(this, LoginActivity.class));
                }
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mWebView != null && mWebView.canGoBack()) {
                mWebView.goBack();
                return true;
            } else
                return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (intent == null || mWebView == null || intent.getData() == null)
            return;
        mWebView.loadUrl(intent.getData().toString());
    }

    @Override
    protected void onDestroy() {
        if (mViewParent != null) {
            mViewParent.removeView(mWebView);
        }
        if (mWebView != null)
            mWebView.destroy();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        TbsLog.d(TAG, "onActivityResult, requestCode:" + requestCode
                + ",resultCode:" + resultCode);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 0:
                    if (null != uploadFile) {
                        Uri result = data == null || resultCode != RESULT_OK ? null
                                : data.getData();
                        uploadFile.onReceiveValue(result);
                        uploadFile = null;
                    }
                    break;
                default:
                    break;
            }
        } else if (resultCode == RESULT_CANCELED) {
            if (null != uploadFile) {
                uploadFile.onReceiveValue(null);
                uploadFile = null;
            }

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    int RC_CAMERA_AND_LOCATION =1;
    @AfterPermissionGranted(1)
    private void methodRequiresTwoPermission() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.READ_PHONE_STATE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "请允许此权限，否则可能影响应用的使用",
                    RC_CAMERA_AND_LOCATION, perms);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        Log.i(TAG, "onPermissionsGranted: 11111111111111111111"+new Gson().toJson(perms));
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        Log.i(TAG, "onPermissionsDenied: 222222222222222222222222"+new Gson().toJson(perms));
    }

    @Override
    public void onTranslucent(float alpha) {
        rl_alpha.setAlpha(alpha);
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setAlpha(alpha);
    }
}
