package hlz.cn.jhc.mytools.movies.web;

import java.util.ArrayList;
import java.util.List;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.movies.CommentSelectPlatformBean;

/**
 * Created by Administrator on 2018/10/13.
 */

public class DataVip {

    public static String[] names = new String[]{
            "vip视频解析(无广告)","全民解析(小广告)","全民解析2(小广告)","多平台解析（无广告）","vip视频解析2（无广告）",
            "牛巴巴vip解析(无广告)", "大云播（无广告）","千年视频解析（无广告）","无作为解析（广告有点多）","全民解析(无广告)",
            "风沙视频解析（无广告）","VIP视频解析（无广告）","易小安解析（无广告）",  "小七解析（无广告）","金桥解析（小广告）",
            "蒹葭-视频解析（无广告）","老D解析（广告有点多）","小朱解析（无广告）","思古解析（广告有点多）","917k全网解析（无广告）",
            "悟阔解析（无广告）","优优解析（无广告）","vip视频解析（广告有点多）","vip视频解析（无广告）","小雨SEO解析（无广告）",
            "七哥视频解析（无广告）","全能视频解析（无广告）", "酷博影视解析（无广告）",

    };
    public static String[] urls = new String[]{"http://czjx8.com/",//1 vip视频解析
            "http://www.qmaile.com/",//2 全民解析
            "http://www.vi0.cc/",//3 全民解析(2)
            "http://www.5ifxw.com/vip/",//4 多平台解析
            "http://www.prinevillesda.org/",//5 vip视频解析2

            "http://mv.688ing.com/",//6 牛巴巴解析
            "http://www.dayunbo.com/v/",//7 大云播
            "http://vip.1000years.net/",//8 千年VIP视频解析
            "http://vip.wuzuowei.net/",//11 无作为解析
            "http://www.9sey.cc/",//12 全民解析(3)

            "http://www.fengshayun.top/vip/",//13
            "http://www.xxall.cn/",//14 VIP视频在线解析大全
            "http://yy.jun568.com/",//15 易小安vip视频在线解析
            "http://www.peng3.com/vip/?a=http%3A%2F%2Fyun.baiyug.cn%2Fvip%2Findex.php%3Furl%3D%0D%0A&url=",//16 小七解析
            "https://www.ikukk.com/?url=https%3A%2F%2Fv.splash_qq.com%2Fx%2Fcover%2Fm441e3rjq9kwpsc.html",//18 爱酷解析 （todo 慢）

            "https://www.bavei.com/",//19 全民vip视频解析在线跳转系统
            "http://jx.jqaaa.com/#",//20 金桥解析
            "http://vip.iljw.me/?m=1",//21 蒹葭-VIP视频解析
            "http://v.laod.cn/",//22 老D解析
            "http://vipjiexi.pigguli.com/",//23 小朱解析 （todo 解析多）

            "http://dy.dakamao8.com/",//25 思古解析
            "http://917k.la/",//26 917k全网视频解析
            "http://vip.vipbuluo.com/",//30 vip视频解析 （todo 解析多）
            "http://www.yudsseo.com/shipin/",//
            "http://jx.zzit.cc/",//

            "http://jx.x-99.cn/",//
            "http://www.arsu.cn/",//
            "http://www.stoneair.online/",//




    };

    /**
     *
     * http://www.prinevillesda.org/
     *
     *
     * @param position
     * @return
     */


    public static List<CommentSelectPlatformBean> getListData(int position) {
        List<CommentSelectPlatformBean> list = new ArrayList<>();
        for (int i = 0; i < DataVip.names.length; i++) {
            CommentSelectPlatformBean bean = new CommentSelectPlatformBean();
            bean.setTitle((i+1)+". "+DataVip.names[i]);
            bean.setUrl(DataVip.urls[i]);
            bean.setImg(0);
            if (i == position) {
                bean.setIsSelect("1");
            } else {
                bean.setIsSelect("0");
            }
            list.add(bean);
        }
        return list;
    }
}
