package hlz.cn.jhc.mytools.movies;

import java.util.ArrayList;
import java.util.List;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;

/**
 * todo 小说
 * Created by Administrator on 2018/6/17.
 */

public class DataNovel {
    public static String[] names = new String[]{"顶点小说", "下书网", "起点中文网", "纵横中文网", "红袖添香",
            "潇湘书院", "17k小说", "逐浪小说", "小说阅读网", "起点女生网",
            "磨铁中文网", "久久小说", "书包网", "中国听书网站网站大全", "话匣子听书",
            "听书楼", "百听听书", "有声听书吧", "好看听书网", "520听书网",
            "听书阁", "听中国", "天方听书网", "喜马拉雅",
    };
    public static int[] img = new int[]{
            R.mipmap.img_dingdian_novel, R.mipmap.img_xiashuwang_novel, R.mipmap.img_qidianzhongwenwang_novel, R.mipmap.img_zhonghengzhongwenwang_novel, R.mipmap.img_hongxiutianxiang_novel,
            R.mipmap.img_xiaoxiangshuyuan_novel, R.mipmap.img_17k_novel, R.mipmap.img_zhulang_novel, R.mipmap.img_xiaoshuoyueduwang_novel, R.mipmap.img_qidiannvshengwang_novel,
            R.mipmap.img_motiezhongwenwang_novel, R.mipmap.img_jiujiu_novel, R.mipmap.img_shubaowang_novel, R.mipmap.img_zhongguotingshuwangzhandaquan_novel, R.mipmap.img_huaxiazitingshu_novel,
            R.mipmap.img_tingshulou_novel, R.mipmap.img_baitingtingshu_novel, R.mipmap.img_youshengtingshuba_novel, R.mipmap.img_haokantingshuba_novel, R.mipmap.img_520tingshuwang_novel,
            R.mipmap.img_tingshuge_novel, R.mipmap.img_tingzhongguo_novel, R.mipmap.img_tianfangtingshuwang_novel, R.mipmap.img_ximalaya_novel,
    };

    public static String[] urls = new String[]{
            "https://www.x23us.com/",//1
            "https://www.xiashutxt.com/",//2
            "https://www.qidian.com/",//3
            "http://www.zongheng.com/",//4
            "https://www.hongxiu.com/",//5

            "http://www.xxsy.net/",//6
            "http://www.17k.com/",//7
            "http://www.zhulang.com/",//8
            "https://www.readnovel.com/",//9
            "https://www.qdmm.com/",//10

            "http://www.motie.com/",//11
            "http://www.jjxsw.com/",//12
            "https://www.bookbao99.net/",//
            "http://www.lvse.cn/zhongguo/tingshu",//
            "http://www.huaxiazi.com/",//

            "http://www.tingshulou.com/",//
            "https://www.bookting.cn/#/home",//
            "https://www.ysts8.com/",//
            "http://www.haokan5.com/",//
            "http://www.520tingshu.com/",//

            "http://www.tingshuge.com/",//
            "http://www.tingchina.com/",//
            "http://www.tingbook.com/",//
            "https://www.ximalaya.com/",//
    };

    /**
     *  "http://www.1966w.com/",//20 7D影城
     */

    /**
     * todo 视频平台列表
     *
     * @return
     */

    public static List<WatchCollectionTable> getList() {
        List<WatchCollectionTable> movicesModels = new ArrayList<>();

        movicesModels = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            WatchCollectionTable model = new WatchCollectionTable();
            model.setTitle(names[i]);
            model.setUrl(urls[i]);
            model.setImg(img[i]);
            model.setIsOwnDefined("1");
            movicesModels.add(model);
        }

        return movicesModels;
    }
}
