package hlz.cn.jhc.mytools.movies;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;
import hlz.cn.jhc.mytools.home.LabelDialog;
import hlz.cn.jhc.mytools.utils.Tools;

public class WatchConllectionDialog extends AlertDialog implements View.OnClickListener {


    TextView tvTitle;
    EditText etTitle;
    EditText etUrl;
    TextView tvCancel;
    TextView tvSure;
    private Context context;
    WatchCollectionTable bean;

    public WatchConllectionDialog(Context context) {
        super(context);
        this.context = context.getApplicationContext();
    }
    public WatchConllectionDialog(Context context, WatchCollectionTable bean) {
        super(context);
        this.context = context.getApplicationContext();
        this.bean = bean;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_watch_collection);
        initView();
        listener();
    }

    private void listener() {

    }

    private void initView() {
        etTitle = findViewById(R.id.et_title);
        etUrl = findViewById(R.id.et_url);
        tvCancel = findViewById(R.id.tv_cancel);
        tvSure = findViewById(R.id.tv_sure);
        tvSure.setOnClickListener(this);
        tvCancel.setOnClickListener(this);

        if (bean!=null){
            etTitle.setText(Tools.getText(bean.getTitle()));
            etUrl.setText(Tools.getText(bean.getUrl()));
        }

        etUrl.setFocusable(true);
        etUrl.setFocusableInTouchMode(true);
        etUrl.requestFocus();

        etTitle.setFocusable(true);
        etTitle.setFocusableInTouchMode(true);
        etTitle.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_cancel:
                dismiss();
                break;
            case R.id.tv_sure:
                if (mOnSureClickListener!=null){
                    String title = etTitle.getText().toString().trim();
                    String url = etUrl.getText().toString().trim();
                    if (TextUtils.isEmpty(title)){
                        Tools.showShortToast("请输入标题！");
                        return;
                    }else if (TextUtils.isEmpty(url)){
                        Tools.showShortToast("请输入地址！");
                        return;
                    }
                    mOnSureClickListener.sureClick(title,url);
                }
                break;
        }
    }

    private OnSureClickListener mOnSureClickListener;
    public void setOnSureClickListener(OnSureClickListener mOnSureClickListener){
        this.mOnSureClickListener = mOnSureClickListener;
    }
    public interface OnSureClickListener{
        void sureClick(String title,String url);
    }
}
