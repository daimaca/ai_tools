package hlz.cn.jhc.mytools.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.meis.widget.MeiTextPathView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.db.DaoManager;
import hlz.cn.jhc.mytools.db.SharedPreferencesConst;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.home.ThemeWebViewActivity;
import hlz.cn.jhc.mytools.utils.Tools;

/**
 * 设置
 */
public class SettingsActivity extends BaseActivity {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.mMeiTextPathView)
    MeiTextPathView mMeiTextPathView;
    @BindView(R.id.mSwitch1)
    Switch mSwitchBall;
    @BindView(R.id.mSwitch2)
    Switch mSwitchOpenComputer;
    @BindView(R.id.mSwitchAdvertising)
    Switch mSwitchAdvertising;
    @BindView(R.id.mSwitchpaopao)
    Switch mSwitchpaopao;
    @BindView(R.id.tv_app_version)
    TextView tvAppVersion;
    @BindView(R.id.ll_version)
    LinearLayout llVersion;
    @BindView(R.id.ll_theme)
    LinearLayout llTheme;
    @BindView(R.id.ll_help)
    LinearLayout llHelp;
    @BindView(R.id.ll_about)
    LinearLayout llAbout;
    @BindView(R.id.ll_couple_back)
    LinearLayout llCoupleBack;
    @BindView(R.id.ll_play_reward)
    LinearLayout ll_play_reward;
    @BindView(R.id.ll_notification)
    LinearLayout ll_notification;
    @BindView(R.id.tv_path)
    TextView tv_path;
    @BindView(R.id.tv_app_message)
    TextView tv_app_message;
    @BindView(R.id.tv_notification_status)
    TextView tv_notification_status;

    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        setStatusBar();
    }

    @Override
    protected void initData() {
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText("设置");
        String path = DaoManager.getBasePath() + "/aixiaozhi/";
        tv_path.setText("数据路径：" + path);
        mMeiTextPathView.setVisibility(View.GONE);
        tvAppVersion.setText(Tools.getAppVersion());
        mSwitchBall.setChecked(Tools.isOpenBall());
        mSwitchOpenComputer.setChecked(Tools.isWebComputer());
        mSwitchAdvertising.setChecked(Tools.isShieldingAdvertising());
        mSwitchpaopao.setChecked(Tools.isOpenPaoPao());
        setLisneter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Tools.isNotificationEnabled(this)){
            tv_notification_status.setText("状态：已打开");
        }else {
            tv_notification_status.setText("状态：已关闭");
        }
        tv_app_message.setText(Tools.getLastVersionName());

    }

    private void setLisneter() {
        mSwitchBall.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    Tools.showShortToast("开启时可能会影响应用速度，建议低配手机关闭！");
                }else {
                    Tools.showShortToast("已为你关闭此模式！");
                }
                SharedPreferencesUtils.setKeyValue(SharedPreferencesConst.isOpenBallKey, isChecked);
            }
        });
        mSwitchOpenComputer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    Tools.showShortToast("已开启电脑版加载网页！");
                }else {
                    Tools.showShortToast("已关闭电脑版加载网页！");
                }
                SharedPreferencesUtils.setKeyValue(SharedPreferencesConst.isComputerKey, isChecked);
            }
        });
        mSwitchAdvertising.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    Tools.showShortToast("已开启智能去广告！");
                }else {
                    Tools.showShortToast("已关闭智能去广告！");
                }
                SharedPreferencesUtils.setKeyValue(SharedPreferencesConst.isShieldingAdvertisingKey, isChecked);
            }
        });
        mSwitchpaopao.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    Tools.showShortToast("已开启泡泡！");
                }else {
                    Tools.showShortToast("已关闭泡泡！");
                }
                SharedPreferencesUtils.setKeyValue(SharedPreferencesConst.isOpenpaopaoKey, isChecked);
            }
        });
    }

    @OnClick({R.id.back, R.id.ll_version, R.id.ll_help, R.id.ll_about,R.id.ll_couple_back,R.id.tv_path,R.id.ll_play_reward,R.id.ll_notification,R.id.ll_theme})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.ll_version:
//                Tools.showShortToast("暂不支持在线更新！");
                startActivity(new Intent(this,UpdateActivity.class));
                break;
            case R.id.ll_help:
//                Tools.showShortToast("小志很懒，暂未提供帮助...");
                startActivity(new Intent(this,HelpActivity.class));
                break;
            case R.id.ll_about:
//                Tools.showShortToast("小志很懒，暂时什么都没留下...");
                startActivity(new Intent(this,AboutActivity.class));
                break;
                case R.id.ll_couple_back://反馈
//                Tools.showShortToast("暂未提供反馈...");
                    startActivity(new Intent(this,FeedBackActivity.class));
                break;
                case R.id.ll_play_reward://打赏
                    startActivity(new Intent(this,PlayRewardActivity.class));
                break;
                case R.id.ll_notification://打赏
                    Tools.gotoNotificationSetting(this);
                break;
                case R.id.ll_theme://设置主题
                    startActivity(new Intent(this, ThemeWebViewActivity.class));
                break;
        }
    }
}
