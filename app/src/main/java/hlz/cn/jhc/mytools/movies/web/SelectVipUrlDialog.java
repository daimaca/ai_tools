package hlz.cn.jhc.mytools.movies.web;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.db.SharedPreferencesConst;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.utils.Tools;

/**
 * Created by Administrator on 2018/10/21.
 */

public class SelectVipUrlDialog extends AlertDialog {
    public static int mVipUrlPosition = 0;
    private Context context;
    private TextView tv_select_title;
    private TextView tv_cancel;
    private TextView tv_sure;
    private RecyclerView rlv_list;
    private int platformPosition;
    private MyAdapter mMyAdapter;

    public SelectVipUrlDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_platform_dialog);
        // 去掉四个黑角
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        initView();
        initData();
        setListener();
    }

    private void setListener() {
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectOnclick != null) {
                }
                mSelectOnclick.onCancel();
            }
        });
        tv_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectOnclick != null) {
                    mSelectOnclick.onSure(DataVipUrl.getListData(mVipUrlPosition).get(mVipUrlPosition));
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        DisplayMetrics metrics = new DisplayMetrics();
        getWindow().getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        getWindow().getAttributes().width = (int) (width * 0.8);
        getWindow().getAttributes().height = (int) (height * 0.8);
    }

    private void initData() {
        mMyAdapter = new MyAdapter(DataVipUrl.getListData(mVipUrlPosition));
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        rlv_list.setLayoutManager(layoutManager);
        rlv_list.setAdapter(mMyAdapter);
        Tools.setListAnimation(context, rlv_list);
    }


    private void initView() {
        tv_select_title = findViewById(R.id.tv_select_title);
        tv_cancel = findViewById(R.id.tv_cancel);
        tv_sure = findViewById(R.id.tv_sure);
        rlv_list = findViewById(R.id.rlv_list);
        platformPosition = SharedPreferencesUtils.getKeyValue(context, SharedPreferencesConst.SelectPlatformPositionKey, 0);
        tv_select_title.setText("当前平台：" + Tools.getText(DataVipUrl.getListData(mVipUrlPosition).get(mVipUrlPosition).getName()));
    }

    private SelectOnclick mSelectOnclick;

    public void setOnSelectOnclick(SelectOnclick mSelectOnclick) {
        this.mSelectOnclick = mSelectOnclick;
    }

    public interface SelectOnclick {
        void onSure(DataVipUrl.VipUrlBean bean);

        void onCancel();
    }

    class MyAdapter extends BaseQuickAdapter<DataVipUrl.VipUrlBean, MyAdapter.ViewHolder> {

        private List<DataVipUrl.VipUrlBean> data;

        public MyAdapter(@Nullable List<DataVipUrl.VipUrlBean> data) {
            super(R.layout.dialog_vip_url_item, data);
            this.data = data;
        }

        @Override
        protected void convert(final MyAdapter.ViewHolder helper, DataVipUrl.VipUrlBean item) {
            if (item != null) {
                helper.tvTitle.setText(item.getName());
                if (item.getImg()==0){
                    Glide.with(context).load("").apply(Tools.getNoImageRequestOptions()).into(helper.ivImg);
                }else {
                    Glide.with(context).load(item.getImg()).apply(Tools.getNoImageRequestOptions()).into(helper.ivImg);
                }

                if (item.getIsSelect().equals("1")) {
                    helper.tvTitle.setTextColor(context.getResources().getColor(R.color.green_light));
                } else {
                    helper.tvTitle.setTextColor(context.getResources().getColor(R.color._333333));
                }
            }

            helper.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mVipUrlPosition = helper.getAdapterPosition();
                    for (int i = 0; i < data.size(); i++) {
                        if (i == mVipUrlPosition) {
                            data.get(i).setIsSelect("1");
                        } else {
                            data.get(i).setIsSelect("0");
                        }
                    }
                    notifyDataSetChanged();
                    tv_select_title.setText("当前平台：" + Tools.getText(data.get(mVipUrlPosition).getName()));
                }
            });
        }

        class ViewHolder extends BaseViewHolder{
            @BindView(R.id.iv_img)
            ImageView ivImg;
            @BindView(R.id.tv_title)
            TextView tvTitle;

            ViewHolder(View view) {
                super(view);
                ButterKnife.bind(this, view);
            }
        }
    }


}
