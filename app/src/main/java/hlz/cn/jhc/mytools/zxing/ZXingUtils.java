package hlz.cn.jhc.mytools.zxing;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.google.zxing.Result;
import com.google.zxing.client.result.AddressBookParsedResult;
import com.google.zxing.client.result.ParsedResult;
import com.google.zxing.client.result.ParsedResultType;
import com.google.zxing.client.result.URIParsedResult;
import com.mylhyl.zxing.scanner.ScannerOptions;
import com.mylhyl.zxing.scanner.ScannerView;
import com.mylhyl.zxing.scanner.encode.QREncode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.application.MyApplication;
import hlz.cn.jhc.mytools.utils.Tools;


public class ZXingUtils {

    /**
     * todo 设置样式
     * @param mScannerView
     */
    public static void setScannerViewStyle(ScannerView mScannerView, Context context) {
        ScannerOptions.Builder builder = new ScannerOptions.Builder();
        int width = (int) (Tools.Px2Dp(Tools.getWidthPixels())*0.7);
        Log.i("setScannerViewStyle", "setScannerViewStyle: "+width);
        builder.setLaserStyle(ScannerOptions.LaserStyle.RES_LINE, R.mipmap.scan_line_img)
                .setFrameSize(width,width);//(int) context.getResources().getDimension(R.dimen.dp_126),(int) context.getResources().getDimension(R.dimen.dp_126)

        builder.setFrameCornerColor(context.getResources().getColor(R.color.theme_06));

        builder.setTipTextSize(12);
        builder.setTipText("将二维码/条码放入框内，即可自动扫描");
//        builder.setScanFullScreen(true);

        mScannerView.setScannerOptions(builder.build());
    }

    // todo 判断是否有闪光灯功能
    public static boolean hasFlash() {
        return MyApplication.mContext.getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }
    /**
     * todo 灯开关
     */
    public static void isLight(ScannerView scannerView, boolean isLight){
        scannerView.toggleLight(isLight);//开
    }

    /**
     * todo 创建二维码图片
     * @param context
     * @param content
     * @return
     */
    public static Bitmap createBitmap(Context context, String content){
        //文本类型
        Bitmap bitmap = new QREncode.Builder(context)
                .setColor(context.getResources().getColor(R.color.theme_06))//二维码颜色
                //.setParsedResultType(ParsedResultType.TEXT)//默认是TEXT类型
                .setContents(content)//二维码内容
                .setLogoBitmap(BitmapFactory.decodeResource(context.getResources(),R.mipmap.ic_launcher_logo))//二维码中间logo
                .build().encodeAsBitmap();

//        switch (type)
//        {
//            case ADDRESSBOOK:
//            //联系人类型
//                bitmap = new QREncode.Builder(context)
//                    .setParsedResultType(ParsedResultType.ADDRESSBOOK)
//                    .setAddressBookUri(Uri.parse(content)).build().encodeAsBitmap();
//            break;
//            case TEXT:
//                //文本类型
//                bitmap = new QREncode.Builder(context)
//                        .setColor(context.getResources().getColor(R.color.colorPrimary))//二维码颜色
//                        //.setParsedResultType(ParsedResultType.TEXT)//默认是TEXT类型
//                        .setContents(content)//二维码内容
//                        .setLogoBitmap(null)//二维码中间logo
//                        .build().encodeAsBitmap();
//                break;
//                default:break;
//        }
        return bitmap;
    }

    public static Bitmap createBitmap(Context context, String content,ParsedResultType type){
        //文本类型
        Bitmap bitmap = new QREncode.Builder(context)
                .setColor(context.getResources().getColor(R.color.theme_06))//二维码颜色
                .setParsedResultType(type)//默认是TEXT类型
                .setContents(content)//二维码内容
                .setLogoBitmap(null)//二维码中间logo
                .build().encodeAsBitmap();

        return bitmap;
    }

    /**
     * todo 根据不同类型做出相应的处理
     * @param rawResult
     * @param parsedResult
     */
    public static void setParsedResult(Result rawResult,ParsedResult parsedResult){
        ParsedResultType type = parsedResult.getType();
        switch (type) {
            case ADDRESSBOOK:
                AddressBookParsedResult addressBook = (AddressBookParsedResult) parsedResult;
//                        bundle.putSerializable(Intents.Scan.RESULT, new AddressBookResult(addressBook));
                break;
            case URI:
                URIParsedResult uriParsedResult = (URIParsedResult) parsedResult;
//                        bundle.putString(Intents.Scan.RESULT, uriParsedResult.getURI());
                break;
            case TEXT:
//                        bundle.putString(Intents.Scan.RESULT, rawResult.getText());
                break;
        }
    }

    public static class DownLoadImageThread extends Thread{
        private Context context;
        private Bitmap bitmap;
        public DownLoadImageThread(Context context,Bitmap bitmap){
            this.context = context.getApplicationContext();
            this.bitmap = bitmap;
        }
        @Override
        public void run() {

            saveImageToGallery(context,bitmap);

        }
    }

    //将图片保存在相册：
    public static void displayToGallery(Context context, File photoFile) {
        if (photoFile == null || !photoFile.exists()) {
            return;
        }
        String photoPath = photoFile.getAbsolutePath();
        String photoName = photoFile.getName();
        // 把文件插入到系统图库
        try {
            ContentResolver contentResolver = context.getContentResolver();
            MediaStore.Images.Media.insertImage(contentResolver, photoPath, photoName, null);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        // 最后通知图库更新
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + photoPath)));
    }
//    https://blog.csdn.net/wanniu/article/details/53442448
    public static void saveImageToGallery(Context context, Bitmap bmp) {
        // 首先保存图片
        File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsoluteFile();//注意小米手机必须这样获得public绝对路径
        String fileName = "AI小志二维码相册";
        File appDir = new File(file ,fileName);
        if (!appDir.exists()) {
            appDir.mkdirs();
        }


        fileName = System.currentTimeMillis() + ".jpg";
        File currentFile = new File(appDir, fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(currentFile);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // 其次把文件插入到系统图库
//        try {
//            MediaStore.Images.Media.insertImage(context.getContentResolver(),
//                    currentFile.getAbsolutePath(), fileName, null);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }

        // 最后通知图库更新
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                Uri.fromFile(new File(currentFile.getPath()))));
    }
}
