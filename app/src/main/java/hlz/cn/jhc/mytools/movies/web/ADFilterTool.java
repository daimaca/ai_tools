package hlz.cn.jhc.mytools.movies.web;

import android.content.Context;
import android.content.res.Resources;
import android.webkit.WebView;

import hlz.cn.jhc.mytools.R;

/**
 * Created by Administrator on 2018/8/12.
 */

class ADFilterTool {

    //去广告 1
    public static String getClearAdDivJs(Context context) {
        String js = "javascript:";
        Resources res = context.getResources();
        String[] adDivs = res.getStringArray(R.array.adBlockDiv);
        for (int i = 0; i < adDivs.length; i++) {

            js += "var adDiv" + i + "= document.getElementById('" + adDivs[i] + "');if(adDiv" + i + " != null)adDiv" + i + ".parentNode.removeChild(adDiv" + i + ");";
        }
        return js;
    }

    public static void hideSlector(WebView view){
        Resources res = view.getContext().getResources();
        String[] adDivs = res.getStringArray(R.array.hideSelector);
        for (int i = 0; i < adDivs.length; i++) {
            view.loadUrl("javascript:function setTop(){document.querySelector('"+adDivs[i]+"').style.display=\"none\";}setTop();");
        }
        }


    public static boolean hasAd(Context context,String url){
        Resources res= context.getResources();
        String[]adUrls =res.getStringArray(R.array.adBlockUrl);
        for(String adUrl : adUrls){
            if(url.contains(adUrl)){
                return true;
            }
        }
        return false;
    }
}
