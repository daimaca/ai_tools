package hlz.cn.jhc.mytools.comment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.util.ArrayList;

import hlz.cn.jhc.mytools.share.ShareUtils;

public class FightImageThread extends Thread{
    private Context context;
    private String url;
    private int count;

    public FightImageThread(Context context, String url, int count) {
        this.context = context;
        this.url = url;
        this.count = count;
    }

    @Override
    public void run() {
        ArrayList<Uri> uriList = new ArrayList<>();
        Uri uri = ShareUtils.getImageUri(context, url);
        for (int i = 0; i < count; i++) {
            uriList.add(uri);
        }
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uriList);
        shareIntent.setType("image/*");
        context.startActivity(Intent.createChooser(shareIntent, ShareUtils.title));
    }
}
