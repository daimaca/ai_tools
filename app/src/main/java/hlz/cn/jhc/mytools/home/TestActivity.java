package hlz.cn.jhc.mytools.home;

import android.os.Bundle;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.movies.web.X5WebView;

public class TestActivity extends BaseActivity {
    @BindView(R.id.mWebView)
    WebView mWebView;

    @Override
    protected void setLayout() {
        setContentView(R.layout.test);
        ButterKnife.bind(this);
    }

    @Override
    protected void initData() {
        mWebView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(DataModel.newsUrl);
                return super.shouldOverrideUrlLoading(view, request);
            }
        });
        mWebView.loadUrl(DataModel.newsUrl);
        mWebView.getSettings().setJavaScriptEnabled(true);
    }
}
