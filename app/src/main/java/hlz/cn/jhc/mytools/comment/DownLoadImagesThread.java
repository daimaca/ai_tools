package hlz.cn.jhc.mytools.comment;

import android.content.Context;
import android.util.Log;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * 下载多个图片
 */
public class DownLoadImagesThread extends Thread {
    private static final String TAG = "DownLoadImagesThread";
    private Context context;
    private List<String> urls;

    public DownLoadImagesThread(Context context, List<String> urls) {
        this.context = context.getApplicationContext();
        this.urls = urls;
    }

    @Override
    public void run() {
            for (int i = 0; i < urls.size(); i++) {
                DownLoadImageUtils.downloadImage(context, urls.get(i));
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.i(TAG, "run: DownLoadImagesThread  "+i);
            }
    }
}