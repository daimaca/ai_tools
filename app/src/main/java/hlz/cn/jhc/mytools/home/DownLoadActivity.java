package hlz.cn.jhc.mytools.home;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;

public class DownLoadActivity extends BaseActivity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.mTv)
    TextView mTv;
    @BindView(R.id.rlv_list)
    RecyclerView rlvList;

    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_download);
        ButterKnife.bind(this);
    }

    @Override
    protected void initData() {

    }

    @OnClick({R.id.back, R.id.mTv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.mTv:
                break;
        }
    }
}
