package hlz.cn.jhc.mytools.movies.agenweb;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.just.agentweb.AgentWeb;
import com.just.agentweb.DefaultWebClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.eventbus.EventBusUtils;
import hlz.cn.jhc.mytools.home.DataModel;
import hlz.cn.jhc.mytools.movies.DataMovices;
import hlz.cn.jhc.mytools.movies.CommentBean;
import hlz.cn.jhc.mytools.utils.Consts;
import hlz.cn.jhc.mytools.utils.Tools;

/**
 * Created by cenxiaozhong on 2017/5/26.
 * <p>
 * source code  https://github.com/Justson/AgentWeb
 */

public class BaseWebActivity extends AppCompatActivity {


    protected AgentWeb mAgentWeb;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.close)
    ImageView close;
    @BindView(R.id.reflesh)
    ImageView reflesh;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.container)
    LinearLayout mLinearLayout;
    @BindView(R.id.mTopMenuLinearLayout)
    LinearLayout mTopMenuLinearLayout;
    @BindView(R.id.mTv)
    TextView mTv;
    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;
    private AlertDialog mAlertDialog;

    private String url = "";
    private String title = "";
    private UseTypeDialog useTypeDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_web);
        ButterKnife.bind(this);
        setStatusBar();

        initData();
        initUI();
        setListener();

        long p = System.currentTimeMillis();
//        CoolIndicatorLayout mCoolIndicatorLayout = new CoolIndicatorLayout(this);
        //mAgentWeb.getUrlLoader().loadUrl(getUrl());
        long n = System.currentTimeMillis();
        Log.i("Info", "init used time:" + (n - p));

    }

    protected void initUI() {
        tvTitle.setText(title + "");//设置标题
        londingWeb();//加载web
        //相关设置
        int type = SharedPreferencesUtils.getKeyValue(this, Consts.AgenWebPlayerType, Consts.Phone);
        isPlayerType(type);
    }


    private void setListener() {

    }

    public void initData() {
        CommentBean model = getIntent().getParcelableExtra("bean");
        if (model != null) {
            url = model.getUrl();
            title = model.getTitle();
        }
        useTypeDialog = new UseTypeDialog(this);
    }



    public String getTitleData() {
        return "标题";
    }

    private void londingWeb() {
        mAgentWeb = AgentWeb.with(this)
                .setAgentWebParent(mLinearLayout, new LinearLayout.LayoutParams(-1, -1))
                .useDefaultIndicator()
                .setWebChromeClient(mWebChromeClient)
                .setWebViewClient(mWebViewClient)
                .setMainFrameErrorView(R.layout.agentweb_error_page, -1)
                .setSecurityType(AgentWeb.SecurityType.STRICT_CHECK)
                .setWebLayout(new WebLayout(this))
                .setOpenOtherPageWays(DefaultWebClient.OpenOtherPageWays.DISALLOW)//打开其他应用时，弹窗咨询用户是否前往其他应用
                .interceptUnkownUrl() //拦截找不到相关页面的Scheme
                .createAgentWeb()
                .ready()
                .go(url);
    }

    private void setWebSetting() {
        ////支持获取手势焦点
        mAgentWeb.getWebCreator().getWebView().requestFocusFromTouch();

        mAgentWeb.getAgentWebSettings().getWebSettings().setDomStorageEnabled(true);//设定支持缩放

        mAgentWeb.getAgentWebSettings().getWebSettings().setJavaScriptEnabled(true);
        mAgentWeb.getAgentWebSettings().getWebSettings().setUseWideViewPort(true);//设定支持viewport
        mAgentWeb.getAgentWebSettings().getWebSettings().setLoadWithOverviewMode(true);   //自适应屏幕
        mAgentWeb.getAgentWebSettings().getWebSettings().setBuiltInZoomControls(true);
        mAgentWeb.getAgentWebSettings().getWebSettings().setDisplayZoomControls(false);
        mAgentWeb.getAgentWebSettings().getWebSettings().setSupportZoom(true);//设定支持缩放

        if (title.equals(DataModel.titles[2])){
            mAgentWeb.getAgentWebSettings().getWebSettings().setGeolocationEnabled(true);//定位
            mAgentWeb.getAgentWebSettings().getWebSettings().setJavaScriptCanOpenWindowsAutomatically(true);//设置js可以直接打开窗口，如window.open()，默认为false

        }
        int version = Build.VERSION.SDK_INT;
        if(version >= 21) {
            mAgentWeb.getAgentWebSettings().getWebSettings().setMixedContentMode(android.webkit.WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);//允许混合加载http与https
        }

        //webview的缓存模式
        //settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        //自动加载图片
        mAgentWeb.getAgentWebSettings().getWebSettings().setLoadsImagesAutomatically(true);
        mAgentWeb.getAgentWebSettings().getWebSettings().setDefaultTextEncodingName("utf-8");//设置默认的字符编码
        mAgentWeb.getAgentWebSettings().getWebSettings().setAllowFileAccess(true); // 允许访问文件
        mAgentWeb.getAgentWebSettings().getWebSettings().setPluginState(WebSettings.PluginState.ON);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {//5.0 以上的手机要加这个
            mAgentWeb.getAgentWebSettings().getWebSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
    }

    private void isPlayerType(int type) {
        SharedPreferencesUtils.setKeyValue(this, Consts.AgenWebPlayerType, type);
        setWebSetting();
        if (type == Consts.Computer) {
            mAgentWeb.getAgentWebSettings().getWebSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            mAgentWeb.getAgentWebSettings().getWebSettings().setUserAgentString("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36");
            // mAgentWeb.getWebCreator().getWebView().reload();
        } else {
            //mAgentWeb.getWebCreator().getWebView().reload();
        }

    }

    private WebViewClient mWebViewClient = new WebViewClient() {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public void onPageStarted(final WebView view, String url, Bitmap favicon) {
            //do you  work
            Log.i("Info", "BaseWebActivity onPageStarted");
        }

        @Override
        public void onPageFinished(final WebView view, String url) {
            super.onPageFinished(view, url);
            String js = ADFilterTool.getClearAdDivJs(view.getContext());
            Log.v("adJs", js);
            view.loadUrl(js);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ADFilterTool.hideSlector(view);
                }
            },500);
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
            if (!ADFilterTool.hasAd(view.getContext(), url)) {
                return super.shouldInterceptRequest(view, url);
            } else {
                return new WebResourceResponse(null, null, null);
            }
//            url= url.toLowerCase();
//            Log.i("kfog", "shouldInterceptRequest: "+getUrl());
//            if(!url.contains(getUrl())){
//                if(!ADFilterTool.hasAd(view.getContext(),url)){
//                    return super.shouldInterceptRequest(view,url);
//                }else{
//                    return new WebResourceResponse(null,null,null);
//                }
//            }else{
//                return super.shouldInterceptRequest(view,url);
//            }
        }
    };
    private WebChromeClient mWebChromeClient = new WebChromeClient() {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            //do you work
//            Log.i("Info","onProgress:"+newProgress);
        }

        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
//            if (mTitleTextView != null) {
//                mTitleTextView.setText(title);
//            }
        }
    };


    private void showDialog() {

        if (mAlertDialog == null) {
            mAlertDialog = new AlertDialog.Builder(this)
                    .setMessage("您确定要关闭该页面吗?")
                    .setNegativeButton("再逛逛", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (mAlertDialog != null) {
                                mAlertDialog.dismiss();
                            }
                        }
                    })//
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if (mAlertDialog != null) {
                                mAlertDialog.dismiss();
                            }
                            BaseWebActivity.this.finish();
                        }
                    }).create();
        }
        mAlertDialog.show();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (mAgentWeb.handleKeyEvent(keyCode, event)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onPause() {
        mAgentWeb.getWebLifeCycle().onPause();
        super.onPause();

    }

    @Override
    protected void onResume() {
        mAgentWeb.getWebLifeCycle().onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //mAgentWeb.destroy();
        mAgentWeb.getWebLifeCycle().onDestroy();
        mAgentWeb.destroy();
        mAgentWeb = null;
    }

    @OnClick({R.id.back, R.id.close, R.id.reflesh, R.id.mTv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back: //返回
                if (!mAgentWeb.back()) {
                    finish();
                } else {
                    mAgentWeb.back();
                }
                break;
            case R.id.close:
                finish();//关闭
                break;
            case R.id.reflesh:
                mAgentWeb.getWebCreator().getWebView().reload();//刷新
                break;
            case R.id.mTv:
                useType();
                break;
        }
    }

    //todo 复制地址
    private void copyAddress() {
        //获取剪贴板管理器：
        ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        // 创建普通字符型ClipData
        ClipData mClipData = ClipData.newPlainText("Label", mAgentWeb.getWebCreator().getWebView().getOriginalUrl() + "");
        // 将ClipData内容放到系统剪贴板里。
        cm.setPrimaryClip(mClipData);
        Tools.showShortToast("亲！已为你复制成功！");
        useType();//加载平台 手机，电脑

    }

    private boolean isShow = false;
    private void useType() {
        if (!isShow){
            useTypeDialog.show();
            isShow = true;
        }else {
            useTypeDialog.dismiss();
            isShow = false;
        }

        useTypeDialog.setUseTypeDialogListener(new UseTypeDialog.OnUseTypeDialogListener() {
            @Override
            public void onComputer() {
                copyAddress();
                useTypeDialog.dismiss();
                EventBusUtils.postStickyObject(new BaseVipWebActivity.UseType(Consts.Computer));
                startActivity(new Intent(BaseWebActivity.this, VipWebActivity.class));
            }

            @Override
            public void onPhone() {
                copyAddress();
                useTypeDialog.dismiss();
                EventBusUtils.postStickyObject(new BaseVipWebActivity.UseType(Consts.Phone));
                startActivity(new Intent(BaseWebActivity.this, VipWebActivity.class));

            }
        });
        useTypeDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isShow = false;
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(newConfig.orientation==Configuration.ORIENTATION_PORTRAIT){//现在是竖屏
            mTopMenuLinearLayout.setVisibility(View.VISIBLE);
        }
        if(newConfig.orientation==Configuration.ORIENTATION_LANDSCAPE){//现在是横屏
            mTopMenuLinearLayout.setVisibility(View.GONE);
        }
    }

    /**
     * 设置状态栏的属性
     */
    protected boolean useThemestatusBarColor = false;//是否使用特殊的标题栏背景颜色，android5.0以上可以设置状态栏背景色，如果不使用则使用透明色值
    protected boolean withoutUseStatusBarColor = false;//是否使用状态栏文字和图标为暗色，如果状态栏采用了白色系，则需要使状态栏和图标为暗色，android6.0以上可以设置

    protected void setStatusBar() {
        //来自 http://blog.csdn.net/smileiam/article/details/73603840
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {//5.0及以上
            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            decorView.setSystemUiVisibility(option);
            //根据上面设置是否对状态栏单独设置颜色
            if (useThemestatusBarColor) {
                getWindow().setStatusBarColor(Color.WHITE);
            } else {
                getWindow().setStatusBarColor(Color.TRANSPARENT);
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {//4.4到5.0
            WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
            localLayoutParams.flags = (WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS | localLayoutParams.flags);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !withoutUseStatusBarColor) {//android6.0以后可以对状态栏文字颜色和图标进行修改
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }
}
