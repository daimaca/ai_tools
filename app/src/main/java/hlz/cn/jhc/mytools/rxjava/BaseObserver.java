package hlz.cn.jhc.mytools.rxjava;

import android.content.Context;
import android.text.TextUtils;

import com.just.agentweb.LogUtils;

import hlz.cn.jhc.mytools.application.MyApplication;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

public abstract class BaseObserver<T> extends DisposableObserver<BaseEntity<T>> {
    private Disposable mDisposable;
    private Context mContext;
    private static final String TAG = "BaseObserver";

    protected BaseObserver(Context context) {
        this.mContext = context.getApplicationContext();
        MyApplication.mCompositeDisposable.add(this);
    }


    @Override
    public void onNext(BaseEntity<T> tBaseEntity) {
        LogUtils.i("AXIBA", tBaseEntity.getStatus() + "");
        T t = tBaseEntity.getContent();
        String msg = tBaseEntity.getErrMsg();
        onHandleSuccess(t, msg);
    }

    @Override
    public void onError(Throwable e) {
        // 属于接口通了情况下的异常
        if (e instanceof ResultException) {
            LogUtils.i(TAG, "onError: " + ((ResultException) e).getErrMsg());
            if (TextUtils.isEmpty(((ResultException) e).getErrMsg())) {
                onHandleError("暂无更多数据！");
                MyApplication.mCompositeDisposable.remove(this);
                return;
            } else {
                onHandleError(((ResultException) e).getErrMsg());
                MyApplication.mCompositeDisposable.remove(this);
                return;
            }

        }
        // 属于服务器报错或404等类似的异常
        LogUtils.i(TAG, "onError: " + e.toString());
        onHandleError("暂无更多数据！");
        MyApplication.mCompositeDisposable.remove(this);
    }

    @Override
    public void onComplete() {
        LogUtils.i(TAG, "onComplete: ====================>>>>>>>>>>>>>");
    }

    protected void cancelDisposable() {
        if (!mDisposable.isDisposed())
            mDisposable.dispose();
    }

    protected abstract void onHandleSuccess(T t, String msg);

    protected abstract void onHandleError(String msg);
}