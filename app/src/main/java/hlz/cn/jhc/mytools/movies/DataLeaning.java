package hlz.cn.jhc.mytools.movies;

import java.util.ArrayList;
import java.util.List;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;

/**
 * todo 学习
 * Created by Administrator on 2018/6/17.
 */

public class DataLeaning {
    public static String[] names = new String[]{
            "慕课网", "网易云课堂", "腾讯课堂", "中国大学-MOOC", "实验楼",
            "百度传课", "学堂在线", "19课堂", "博学谷", "51学院"

    };
    public static int[] img = new int[]{
            R.mipmap.img_muke, R.mipmap.img_wangyiyun, R.mipmap.img_tengxunketang, R.mipmap.img_chinadaxue, R.mipmap.img_shiyanlou,
            R.mipmap.img_baiduchuanke, R.mipmap.img_xuetangzaixian, R.mipmap.img_19ketang, R.mipmap.img_boxuegu, R.mipmap.img_51xueyuan,
    };

    public static String[] urls = new String[]{"https://www.imooc.com/",//1 慕课网
            "http://study.163.com/",//2 网易云课堂
            "https://ke.splash_qq.com/",//3 腾讯课堂
            "https://www.icourse163.org/",//4 中国大学-MOOC
            "https://www.shiyanlou.com/",//5 实验楼

            "https://chuanke.baidu.com/",// 百度传课
            "http://www.xuetangx.com/",// img_xuetangzaixian
            "http://19.offcn.com/",// img_19ketang
            "https://www.boxuegu.com/",// img_boxuegu
            "http://edu.51cto.com/",// img_boxuegu
    };

    /**
     *  "http://www.1966w.com/",//20 7D影城
     */

    /**
     * todo 视频平台列表
     *
     * @return
     */

    public static List<WatchCollectionTable> getList() {
        List<WatchCollectionTable> movicesModels = new ArrayList<>();

        movicesModels = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            WatchCollectionTable model = new WatchCollectionTable();
            model.setTitle(names[i]);
            model.setUrl(urls[i]);
            model.setImg(img[i]);
            model.setIsOwnDefined("1");
            movicesModels.add(model);
        }

        return movicesModels;
    }
}
