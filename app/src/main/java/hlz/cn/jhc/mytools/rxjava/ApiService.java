package hlz.cn.jhc.mytools.rxjava;

import hlz.cn.jhc.mytools.home.homefragment.UpdateAppBean;
import hlz.cn.jhc.mytools.rxjava.test.TestBean;
import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiService {

    // test
    @POST("/shop/detail")
    Observable<BaseEntity<TestBean>> test(
            @Body RequestBody requestBody
    );
    // App 更新
    @GET("/apiv2/app/check?_api_key=1a1fbd4d56a890a6f76ae26160586116&appKey=fae16db8047866704f983b6d289d3fcd")
    Observable<UpdateAppBean> updateApp(

    );

}

