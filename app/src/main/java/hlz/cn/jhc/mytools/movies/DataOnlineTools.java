package hlz.cn.jhc.mytools.movies;

import java.util.ArrayList;
import java.util.List;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;

/**
 * todo 购物
 * Created by Administrator on 2018/6/17.
 */

public class DataOnlineTools {
    String url1 = "http://tool.aibangxiang.com/tool/music/";//音乐解析
    String url2 = "http://www.doutuxiu.com/";//斗图秀
    String url3 = "http://www.5ifxw.com/fuli/";//我爱分析福利
    String url4 = "http://www.5ifxw.com/zhuangbi/";//在线制作表情

    public static String[] names = new String[]{"音乐解析", "斗图秀", "爱斗图","斗图啦","在线制作表情", "我爱分析福利",
            "程序员的工具箱","EasyIcon","聚神铺导航","动漫星空","动漫图片"


    };

    public static String[] urls = new String[]{
            "http://tool.aibangxiang.com/tool/music/",
            "http://www.doutuxiu.com/",
            "http://www.adoutu.com/index",
            "http://www.doutula.com/",
            "http://www.5ifxw.com/zhuangbi/",
            "http://www.5ifxw.com/fuli/",

            "https://tool.lu/",
            "https://www.easyicon.net/",
            "http://www.jspoo.com/vip.html",
            "http://acg.gamersky.com/",
            "https://www.veer.com/favorites/87086_75948_07396167543211e8af297cd30ac4c61e?utm_source=baidu&utm_medium=cpc&utm_campaign=%E4%B8%93%E9%A2%98-%E5%85%B6%E4%BB%96&utm_content=%E5%8A%A8%E6%BC%AB%E5%9B%BE%E7%89%87&utm_term=%E5%8A%A8%E6%BC%AB%E5%9B%BE%E7%89%87%E7%BD%91%E7%AB%99&chid=901&b_scene_zt=1",
    };

    /**
     *
     *
     *  动漫图片： https://www.veer.com/favorites/87086_75948_07396167543211e8af297cd30ac4c61e?utm_source=baidu&utm_medium=cpc&utm_campaign=%E4%B8%93%E9%A2%98-%E5%85%B6%E4%BB%96&utm_content=%E5%8A%A8%E6%BC%AB%E5%9B%BE%E7%89%87&utm_term=%E5%8A%A8%E6%BC%AB%E5%9B%BE%E7%89%87%E7%BD%91%E7%AB%99&chid=901&b_scene_zt=1
     */

    /**
     * todo 视频平台列表
     *
     * @return
     */
    public static List<WatchCollectionTable> getList() {
        List<WatchCollectionTable> movicesModels = new ArrayList<>();

        movicesModels = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            WatchCollectionTable model = new WatchCollectionTable();
            model.setTitle(names[i]);
            model.setUrl(urls[i]);
            model.setImg(0);
            model.setIsOwnDefined("1");
            movicesModels.add(model);
        }

        return movicesModels;
    }
}
