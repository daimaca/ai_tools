package hlz.cn.jhc.mytools.home;

import java.util.ArrayList;
import java.util.List;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.greendao.TagTable;

/**
 * Created by Administrator on 2018/8/15.
 */

public class DataModel {

//    public static String newsUrl = "https://news.sina.cn/gn?vt=4&pos=3";
    public static String newsUrl = "https://toutiao.eastday.com/";
//    public static String newsUrl = "https://portal.3g.qq.com/";
//    public static String newsUrl = "http://m.ilxdh.com/";

    public static String[] titles = new String[]{
            "视频集合","直播","地图导航","购物","学习",
            "漫画","音乐","小说","在线工具","我的管理"
    };

    private static final int[] imgs = new int[]{
            R.mipmap.shipingjihe_img,R.mipmap.zhibo_img,R.mipmap.daohang_img,R.mipmap.gouwu_img,R.mipmap.xuexi_img,
            R.mipmap.manhua_img,R.mipmap.yinyue_img,R.mipmap.xiaoshuo_img,R.mipmap.zaixiangongju_img,R.mipmap.user_more
    };

    public static List<TagTable> getListHome(){
        List<TagTable> homeList = new ArrayList<>();
        for (int i = 0; i < titles.length; i++) {
            TagTable bean = new TagTable();
            bean.setName(titles[i]);
            bean.setIsOwnDefined("1");
            bean.setImg(imgs[i]);
            homeList.add(bean);
        }
        return homeList;
    }

}
