package hlz.cn.jhc.mytools.jpush;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;

public class NotificationTextDetail extends BaseActivity {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_data)
    TextView tvData;
    private JPushMessageBean bean;

    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_notification_text_detail);
        ButterKnife.bind(this);
        bean = getIntent().getParcelableExtra(JPushType.JPUSH_MESSAGE_BEAN_KEY);
    }

    @Override
    protected void initData() {

        if (bean != null) {
            if (TextUtils.isEmpty(bean.getTitle())) {
                tvTitle.setText("详情");
            } else {
                tvTitle.setText(bean.getTitle());
            }
            tvData.setText(bean.getContent());
        } else {
            tvTitle.setText("详情");
            tvData.setText("未获取到内容！");
        }
    }

    @OnClick(R.id.back)
    public void onClick() {
        finish();
    }
}
