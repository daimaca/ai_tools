package hlz.cn.jhc.mytools.movies.agenweb;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;

import com.just.agentweb.AgentWebView;

import butterknife.BindView;
import butterknife.ButterKnife;
import hlz.cn.jhc.mytools.R;

/**
 * Created by Administrator on 2018/6/18.
 */

public class AgentWebActivity extends AppCompatActivity {

    @BindView(R.id.mAgentWebView)
    AgentWebView mAgentWebView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agentweb);
        ButterKnife.bind(this);

        init();
    }

    private void init() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

//        if (mAgentWeb.handleKeyEvent(keyCode, event)) {
//            return true;
//        }
        return super.onKeyDown(keyCode, event);
    }
}
