package hlz.cn.jhc.mytools.movies;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mcxtzhang.swipemenulib.SwipeMenuLayout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.comment.LabelType;
import hlz.cn.jhc.mytools.db.DBDaoUtils;
import hlz.cn.jhc.mytools.greendao.TagTable;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;
import hlz.cn.jhc.mytools.movies.web.ProfessionalParsingX5WebActivity;
import hlz.cn.jhc.mytools.movies.web.X5WebActivity;
import hlz.cn.jhc.mytools.settings.HelpActivity;
import hlz.cn.jhc.mytools.umeng.UMengUtils;
import hlz.cn.jhc.mytools.utils.Tools;

/**
 * Created by Administrator on 2018/6/17.
 */

public class MoviceAdapter extends RecyclerView.Adapter<MoviceAdapter.MyViewHolder> {

    private Context context;
    private List<WatchCollectionTable> list;
    private DBDaoUtils dbDaoUtils;
    private TagTable tag;

    public MoviceAdapter(Context context, List<WatchCollectionTable> list, DBDaoUtils dbDaoUtils,TagTable tag) {
        this.context = context;
        this.list = list;
        this.dbDaoUtils = dbDaoUtils;
        this.tag = tag;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.movies_item, null);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final WatchCollectionTable bean = list.get(position);
        if (bean != null) {
            if (bean.getIsOwnDefined().equals("2")){
//                holder.mSwipeMenuLayout.setSwipeEnable(true);
                holder.tvError.setVisibility(View.VISIBLE);
                holder.tvEditor.setVisibility(View.VISIBLE);
                holder.tvDelete.setVisibility(View.VISIBLE);
            }else {
//                holder.mSwipeMenuLayout.setSwipeEnable(false);
                holder.tvError.setVisibility(View.VISIBLE);
                holder.tvEditor.setVisibility(View.GONE);
                holder.tvDelete.setVisibility(View.GONE);
            }
            holder.tvTitle.setText(String.valueOf(position+1)+". "+bean.getTitle());
            if (bean.getImg()==0){
                holder.ivImg.setVisibility(View.INVISIBLE);
                holder.tvImgText.setText(bean.getTitle());
                holder.tvImgText.setVisibility(View.VISIBLE);
            }else {
                holder.ivImg.setVisibility(View.VISIBLE);
                Glide.with(context).load(bean.getImg()).into(holder.ivImg);
                holder.tvImgText.setVisibility(View.GONE);
            }
        }else {
            holder.mSwipeMenuLayout.setSwipeEnable(false);
        }
        if (position == 0) {
            holder.mView.setVisibility(View.GONE);
        } else {
            holder.mView.setVisibility(View.VISIBLE);
        }

        if (TextUtils.equals(bean.getLabel(), LabelType.LABEL_OFFICIAL)){
            holder.tvLabel.setBackgroundResource(R.drawable.label_01);
            holder.tvLabel.setText(LabelType.LABEL_OFFICIAL);
        }else if (TextUtils.equals(bean.getLabel(), LabelType.LABEL_FLUENCY)){
            holder.tvLabel.setBackgroundResource(R.drawable.label_02);
            holder.tvLabel.setText(LabelType.LABEL_FLUENCY);
        }else if (TextUtils.equals(bean.getLabel(), LabelType.LABEL_RECOMMEND)){
            holder.tvLabel.setBackgroundResource(R.drawable.label_03);
            holder.tvLabel.setText(LabelType.LABEL_RECOMMEND);
        }else {
            holder.tvLabel.setBackgroundResource(R.drawable.label_04);
            holder.tvLabel.setText("其他");
        }

        holder.tvError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                上报错误
                Map<String,String> map = new HashMap<>();
                map.put("title",bean.getTitle());
                map.put("url",bean.getUrl());
                map.put("isOwnDefined",bean.getIsOwnDefined());
                UMengUtils.onEvent(context.getApplicationContext(),UMengUtils.eventIdWatchCollectionTableError,map);
                Tools.showShortToast("上报成功，管理员将会尽快处理！");
            }
        });

        holder.tvEditor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final WatchConllectionDialog dialog = new WatchConllectionDialog(context,bean);
                dialog.show();
                // 解决无法弹出软键盘
                dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                dialog.setOnSureClickListener(new WatchConllectionDialog.OnSureClickListener() {
                    @Override
                    public void sureClick(String title, String url) {
                        bean.setTitle(title);
                        bean.setUrl(url);
                        boolean s = dbDaoUtils.update(bean);
                        if (s){
                            notifyDataSetChanged();
                            Tools.showShortToast("编辑成功！");
                            dialog.dismiss();
                        }
                    }
                });
            }
        });

        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean is = dbDaoUtils.delete(bean);
                if (is){
                    list.remove(position);
                    notifyDataSetChanged();
                    ((MoviesMainActivity)context).isShowNoDataView();
                }
            }
        });

        holder.mConstraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(bean.getUrl())){
                    context.startActivity(new Intent(context,ProfessionalParsingX5WebActivity.class));
                    return;
                }
//                统计
                Map<String,String> map = new HashMap<>();
                map.put("title",bean.getTitle());
                map.put("url",bean.getUrl());
                map.put("isOwnDefined",bean.getIsOwnDefined());
                UMengUtils.onEvent(context.getApplicationContext(),UMengUtils.eventIdWatchCollectionTable,map);

                Intent intent = new Intent(context, X5WebActivity.class);
                intent.putExtra("bean", list.get(position));
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        if (list == null || list.size() == 0) return 0;
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_img)
        CircleImageView ivImg;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_img_text)
        TextView tvImgText;
        @BindView(R.id.mView)
        View mView;
        @BindView(R.id.mSwipeMenuLayout)
        SwipeMenuLayout mSwipeMenuLayout;
        @BindView(R.id.tv_editor)
        TextView tvEditor;
        @BindView(R.id.tv_delete)
        TextView tvDelete;
        @BindView(R.id.tv_error)
        TextView tvError;
        @BindView(R.id.mConstraintLayout)
        ConstraintLayout mConstraintLayout;
        @BindView(R.id.tv_label)
        TextView tvLabel;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
