package hlz.cn.jhc.mytools.comment;

import android.content.Context;

import java.util.concurrent.ExecutionException;

/**
 * 下载单张图片
 */
public class DownLoadImageThread extends Thread{

    private Context context;
    private String url;
    public DownLoadImageThread(Context context, String url){
        this.context = context;
        this.url = url;
    }

    @Override
    public void run() {
        DownLoadImageUtils.downloadImage(context,url);
    }
}
