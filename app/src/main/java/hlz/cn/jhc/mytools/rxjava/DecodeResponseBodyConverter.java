package hlz.cn.jhc.mytools.rxjava;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.just.agentweb.LogUtils;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Converter;

public class DecodeResponseBodyConverter<T> implements Converter<ResponseBody, T> {
    private static final String TAG = "RRetrofitUtils";
    private final TypeAdapter<T> adapter;
    private final Gson gson;

    DecodeResponseBodyConverter(TypeAdapter<T> adapter, Gson gson) {
        this.adapter = adapter;
        this.gson = gson;
    }

    @Override
    public T convert(ResponseBody value) throws IOException {
        String body = value.string();
        LogUtils.i(TAG, "convert -------body------" + body);
        //先解密 在解压
        try {
            BaseEntity<String> allResponseData = gson.fromJson(body, BaseEntity.class);
            String code = allResponseData.getErrCode();
            if (code.equals("200")) {
                //获取加密数据，解密，之后再让adapter去处理json串，解析具体的数据就可以了
                String content = allResponseData.getContent();
                return adapter.fromJson(content);
            } else {
                LogUtils.i(TAG, "返回err==：" + code);
                LogUtils.i(TAG, "返回err==：" + allResponseData.getErrMsg());
                throw new ResultException(allResponseData.getErrMsg(), code);
            }
        } finally {
            value.close();
        }
    }
}
