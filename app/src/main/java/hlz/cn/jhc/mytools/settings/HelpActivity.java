package hlz.cn.jhc.mytools.settings;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.movies.web.X5WebView;
import hlz.cn.jhc.mytools.share.ShareUtils;
import hlz.cn.jhc.mytools.utils.Tools;

/**
 * 帮助
 */
public class HelpActivity extends BaseActivity {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.mViewParent)
    RelativeLayout mViewParent;
    @BindView(R.id.mTv)
    TextView mTv;
    private X5WebView mWebView;
    private String mHelpUrl = "https://s.wcd.im/v/37iriZ35/";

    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_help);
        ButterKnife.bind(this);
    }

    @Override
    protected void initData() {
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText("帮助中心");
        mTv.setVisibility(View.VISIBLE);
        mTv.setText("分享好友");

        initWeb();
    }

    private void initWeb() {
        mWebView = new X5WebView(this, null);

        mViewParent.addView(mWebView, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView webView, int i) {
                super.onProgressChanged(webView, i);
                if (i == 0 || i >= 100) {
                    mProgressBar.setVisibility(View.GONE);
                } else {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mProgressBar.setProgress(i);
                }
            }
        });
        mWebView.loadUrl(mHelpUrl);


    }

    @Override
    protected void onDestroy() {
        if (mViewParent != null) {
            mViewParent.removeView(mWebView);
        }
        if (mWebView != null) {
            mWebView.destroy();
        }
        super.onDestroy();
    }

    @OnClick({R.id.back, R.id.mTv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.mTv:
                if (!TextUtils.isEmpty(mHelpUrl)) {
                    ShareUtils.shareText(HelpActivity.this, mHelpUrl);
                }else {
                    Tools.showShortToast("未获取到分享数据！");
                }

                break;
        }
    }
}
