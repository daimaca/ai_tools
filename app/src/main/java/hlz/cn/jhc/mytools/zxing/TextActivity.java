package hlz.cn.jhc.mytools.zxing;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;

public class TextActivity extends BaseActivity {

    @BindView(R.id.tv_data)
    TextView tvData;
    public static final String RESULT_TEXT_KEY = "RESULT_TEXT_KEY";
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.mTv)
    TextView mTv;
    private String result;

    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_text);
        ButterKnife.bind(this);
        result = getIntent().getStringExtra(RESULT_TEXT_KEY);
        init();
    }

    @Override
    protected void initData() {

    }

    private void init() {
        tvTitle.setText("详情");
        tvTitle.setVisibility(View.VISIBLE);
        mTv.setVisibility(View.GONE);

        tvData.setText(TextUtils.isEmpty(result) ? "未扫描到任何数据" : result);
    }

    @OnClick(R.id.back)
    public void onClick() {
        finish();
    }
}
