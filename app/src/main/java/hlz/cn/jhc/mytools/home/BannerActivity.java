package hlz.cn.jhc.mytools.home;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;

public class BannerActivity extends BaseActivity {

    public static final String BANNER_ITEM_DATA_KEY = "BANNER_ITEM_DATA_KEY";
    @BindView(R.id.iv_banner)
    ImageView ivBanner;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    private BannerItemData bannerItemData;

    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_banner);
        ButterKnife.bind(this);
        bannerItemData = getIntent().getParcelableExtra(BANNER_ITEM_DATA_KEY);
    }

    @Override
    protected void initData() {
        if (bannerItemData != null) {
            ivBanner.setBackgroundResource(bannerItemData.getImg());
            tvTitle.setText(bannerItemData.getTitle());
        }
    }

    @OnClick(R.id.back)
    public void onClick() {
        finish();
    }

    public static class BannerItemData implements Parcelable {
        int img;
        String title;

        public BannerItemData() {
        }

        protected BannerItemData(Parcel in) {
            img = in.readInt();
            title = in.readString();
        }

        public static final Creator<BannerItemData> CREATOR = new Creator<BannerItemData>() {
            @Override
            public BannerItemData createFromParcel(Parcel in) {
                return new BannerItemData(in);
            }

            @Override
            public BannerItemData[] newArray(int size) {
                return new BannerItemData[size];
            }
        };

        public int getImg() {
            return img;
        }

        public void setImg(int img) {
            this.img = img;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(img);
            parcel.writeString(title);
        }
    }
}
