package hlz.cn.jhc.mytools.base;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import com.just.agentweb.LogUtils;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by Administrator on 2018/6/12.
 */

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "BaseActivity";
    private final int CREATE_STATUS = 1;
    private final int START_STATUS = 2;
    private final int RESUME_STATUS = 3;
    private final int PAUSE_STATUS = 4;
    private final int STOP_STATUS = 5;
    private final int DESTROY_STATUS = 6;
    private static int ACTIVITY_STATUS = 0;
    protected Bundle savedInstanceState;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        setLayout();
        setStatusBar();
        initData();
        ACTIVITY_STATUS = CREATE_STATUS;
        LogUtils.i(TAG, "onCreate: " + ACTIVITY_STATUS);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @Override
    protected void onStart() {
        super.onStart();
        ACTIVITY_STATUS = START_STATUS;
        LogUtils.i(TAG, "onCreate: "+ACTIVITY_STATUS);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ACTIVITY_STATUS = RESUME_STATUS;
        MobclickAgent.onResume(this);
        MobclickAgent.onPageStart(getPackageName().getClass().getSimpleName());
        LogUtils.i(TAG, "onCreate: "+ACTIVITY_STATUS);
    }


    @Override
    protected void onPause() {
        super.onPause();
        ACTIVITY_STATUS = PAUSE_STATUS;
        MobclickAgent.onPause(this);
        MobclickAgent.onPageEnd(getPackageName().getClass().getSimpleName());
        LogUtils.i(TAG, "onCreate: "+ACTIVITY_STATUS);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ACTIVITY_STATUS = STOP_STATUS;
        LogUtils.i(TAG, "onCreate: "+ACTIVITY_STATUS);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ACTIVITY_STATUS = DESTROY_STATUS;
        LogUtils.i(TAG, "onCreate: "+ACTIVITY_STATUS);
    }

    // TODO: 2018/6/12 获取activity的状态
    public static int getActivityStatus() {
        return ACTIVITY_STATUS;
    }

    protected abstract void setLayout();
    protected abstract void initData();

    /**
     * 设置状态栏的属性
     */
    protected boolean useThemestatusBarColor = false;//是否使用特殊的标题栏背景颜色，android5.0以上可以设置状态栏背景色，如果不使用则使用透明色值
    protected boolean withoutUseStatusBarColor = false;//是否使用状态栏文字和图标为暗色，如果状态栏采用了白色系，则需要使状态栏和图标为暗色，android6.0以上可以设置
    protected void setStatusBar() {
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);//设置绘画模式
        //来自 http://blog.csdn.net/smileiam/article/details/73603840
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {//5.0及以上
            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            decorView.setSystemUiVisibility(option);
            //根据上面设置是否对状态栏单独设置颜色
            if (useThemestatusBarColor) {
                getWindow().setStatusBarColor(Color.WHITE);
            } else {
                getWindow().setStatusBarColor(Color.TRANSPARENT);
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {//4.4到5.0
            WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
            localLayoutParams.flags = (WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS | localLayoutParams.flags);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !withoutUseStatusBarColor) {//android6.0以后可以对状态栏文字颜色和图标进行修改
            getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN|View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR); //黑色
        }
    }


    public <view extends View> view getViewId(int resId){
        return (view) findViewById(resId);
    }
    @Override
    public void onClick(View view) {

    }
}
