package hlz.cn.jhc.mytools.jpush;

public interface JPushType {

    String TEXT_DETAIL = "TEXT_DETAIL";//正常文本通知
    String GOTO_UPDATE_ACTIVITY = "GOTO_UPDATE_ACTIVITY";//activity更新跳转


    String JPUSH_MESSAGE_BEAN_KEY = "JPUSH_MESSAGE_BEAN_KEY";//获取JPushMessageBean实例的Key


    String json = "{\n" +
            "\ttitle:\"测试\",\n" +
            "\ttype:\"TEXT_DETAIL\",\n" +
            "\tcontent:\"测试内容\"\n" +
            "\t\n" +
            "}";
    String json2 = "{\n" +
            "\ttitle:\"测试\",\n" +
            "\ttype:\"GOTO_UPDATE_ACTIVITY\",\n" +
            "\tcontent:\"测试内容\"\n" +
            "\t\n" +
            "}";
}
