package hlz.cn.jhc.mytools.home;

/**
 * Created by Administrator on 2018/8/15.
 */

public class HomeBean {
    private String title;
    private int img;
    private String isOwnDefined;// 1 系统 2 自定义

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getIsOwnDefined() {
        return isOwnDefined;
    }

    public void setIsOwnDefined(String isOwnDefined) {
        this.isOwnDefined = isOwnDefined;
    }
}
