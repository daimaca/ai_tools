package hlz.cn.jhc.mytools.home.minefragment;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hlz.cn.jhc.mytools.R;

public class MineAdapter extends BaseQuickAdapter<MineBean, MineAdapter.ViewHolder> {

    public MineAdapter(int layoutResId, @Nullable List<MineBean> data) {
        super(R.layout.fragment_mine_item, getMineData());
    }

    @Override
    protected void convert(MineAdapter.ViewHolder helper, MineBean item) {
        if (item != null) {
            Glide.with(mContext).load(item.getImg()).into(helper.ivImg);
            helper.tvTitle.setText(item.getTitle());
        }
    }

    private static List<MineBean> getMineData() {
        List<MineBean> list = new ArrayList<>();

        return list;
    }

    private static int[] imgs = new int[]{

    };
    private static String[] title = new String[]{
            "反馈","关于小志",""
    };


    static
    class ViewHolder extends BaseViewHolder{
        @BindView(R.id.iv_img)
        CircleImageView ivImg;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.ll_freeback)
        LinearLayout llFreeback;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
