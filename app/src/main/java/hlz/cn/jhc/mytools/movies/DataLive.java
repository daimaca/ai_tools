package hlz.cn.jhc.mytools.movies;

import java.util.ArrayList;
import java.util.List;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;

/**
 * todo 直播
 * Created by Administrator on 2018/6/17.
 */

public class DataLive {
    public static String[] names = new String[]{"斗鱼直播", "虎牙直播", "快手直播", "bili直播", "YY直播",
            "战旗直播", "龙珠直播", "企鹅电竞", "触手直播", "火猫直播",
            "花椒直播", "映客直播", "熊猫TV", "章鱼直播", "华数直播",
            "24直播", "CC直播", "冰豆直播", "网易直播"
    };
    public static int[] img = new int[]{
            R.mipmap.img_douyu_live, R.mipmap.img_huya_live, R.mipmap.img_kuaishou_live, R.mipmap.img_bilibili_live, R.mipmap.img_yy_live,
            R.mipmap.img_zhanqi_live, R.mipmap.img_longzhu_live, R.mipmap.img_qiedianjing_live, R.mipmap.img_chushou_live, R.mipmap.img_huomao_live,
            R.mipmap.img_huajiao_live, R.mipmap.img_yinke_live, R.mipmap.img_xiongmaotv_live, R.mipmap.img_zhangyu_live, R.mipmap.img_huashuo_live,
            R.mipmap.img_24_live, R.mipmap.img_cc_live, R.mipmap.img_bingdou_live, R.mipmap.img_wangyi_live,
    };

    public static String[] urls = new String[]{
            "https://www.douyu.com/",// 斗鱼直播
            "http://www.huya.com/",// 虎牙直播
            "https://live.kuaishou.com/",// 快手直播
            "https://live.bilibili.com/",// bili直播
            "http://www.yy.com/",// YY直播

            "https://www.zhanqi.tv/",// 战旗直播
            "http://www.longzhu.com/",//6 龙珠直播
            "https://egame.splash_qq.com/?adtag=adtag.live",//7 企鹅电竞
            "https://www.chushou.tv/",//8 触手直播
            "https://www.huomao.com/",//9 火猫直播

            "http://www.huajiao.com/",//10 花椒直播
            "http://www.inke.cn/hotlive_list.html",//11 映客直播
            "https://m.panda.tv/",//12 熊猫TV
            "http://www.zhangyu.tv/",//13 章鱼直播
            "https://live.wasu.cn/",//14 华数直播

            "https://www.24zbw.com/live/",//15 24直播
            "http://cc.163.com/",//16 CC直播
            "http://tv.bingdou.net/",//17 冰豆直播
            "http://v.163.com/",//18 网易直播
    };

    /**
     *  "http://www.1966w.com/",//20 7D影城
     */

    /**
     * todo 视频平台列表
     *
     * @return
     */

    public static List<WatchCollectionTable> getList() {
        List<WatchCollectionTable> movicesModels = new ArrayList<>();

        movicesModels = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            WatchCollectionTable model = new WatchCollectionTable();
            model.setTitle(names[i]);
            model.setUrl(urls[i]);
            model.setImg(img[i]);
            model.setIsOwnDefined("1");
            movicesModels.add(model);
        }

        return movicesModels;
    }
}
