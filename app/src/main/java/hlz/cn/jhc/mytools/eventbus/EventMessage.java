package hlz.cn.jhc.mytools.eventbus;

/**
 * Created by Administrator on 2018/8/12.
 */

public class EventMessage<T> {
    private int code;//表示一类消息
    private String messageId;//表示唯一消息处理
    private T data;//不定数据

    public EventMessage(int code) {
        this.code = code;
    }
    public EventMessage(String messageId) {
        this.messageId = messageId;
    }
    public EventMessage(T data) {
        this.data = data;
    }

    public EventMessage(int code, T data) {
        this.code = code;
        this.data = data;
    }
    public EventMessage(String messageId, T data) {
        this.messageId= messageId;
        this.data = data;
    }

    public EventMessage(int code, String messageId, T data) {
        this.code = code;
        this.messageId = messageId;
        this.data = data;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "EventMessage{" +
                "code=" + code +
                ", messageId='" + messageId + '\'' +
                ", data=" + data +
                '}';
    }
}
