package hlz.cn.jhc.mytools.db;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

import hlz.cn.jhc.mytools.application.MyApplication;

/**
 * Created by Administrator on 2018/6/13.
 */

public class SharedPreferencesUtils {

    private static String Name = "Preferences";
    public static void setKeyValue(Context context,String key,String value){
        SharedPreferences preferences  = context.getSharedPreferences(Name,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,value);
        editor.commit();
    }
    public static void setKeyValue(Context context,String key,int value){
        SharedPreferences preferences  = context.getSharedPreferences(Name,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key,value);
        editor.commit();
    }
    public static void setKeyValue(Context context,String key,long value){
        SharedPreferences preferences  = context.getSharedPreferences(Name,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key,value);
        editor.commit();
    }
    public static void setKeyValue(String key,boolean value){
        SharedPreferences preferences  = MyApplication.mContext.getSharedPreferences(Name,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key,value);
        editor.commit();
    }
    public static void setKeyValue(Context context,String key,float value){
        SharedPreferences preferences  = context.getSharedPreferences(Name,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(key,value);
        editor.commit();
    }
    public static void setKeyValue(Context context,String key,Set<String> value){
        SharedPreferences preferences  = context.getSharedPreferences(Name,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putStringSet(key,value);
        editor.commit();
    }

    public static String getKeyValue(Context context,String key,String def_value){
        SharedPreferences preferences  = context.getSharedPreferences(Name,Context.MODE_PRIVATE);
        return preferences.getString(key,def_value);
    }
    public static int getKeyValue(Context context,String key,int def_value){
        SharedPreferences preferences  = context.getSharedPreferences(Name,Context.MODE_PRIVATE);
        return preferences.getInt(key,def_value);
    }
    public static long getKeyValue(Context context,String key,long def_value){
        SharedPreferences preferences  = context.getSharedPreferences(Name,Context.MODE_PRIVATE);
        return preferences.getLong(key,def_value);
    }
    public static boolean getKeyValue(String key,boolean def_value){
        SharedPreferences preferences  = MyApplication.mContext.getSharedPreferences(Name,Context.MODE_PRIVATE);
        return preferences.getBoolean(key,def_value);
    }
    public static float getKeyValue(Context context,String key,float def_value){
        SharedPreferences preferences  = context.getSharedPreferences(Name,Context.MODE_PRIVATE);
        return preferences.getFloat(key,def_value);
    }
    public static Set<String> getKeyValue(Context context,String key,Set<String> def_value){
        SharedPreferences preferences  = context.getSharedPreferences(Name,Context.MODE_PRIVATE);
        return preferences.getStringSet(key,def_value);
    }

}
