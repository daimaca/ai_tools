package hlz.cn.jhc.mytools.home;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;
import hlz.cn.jhc.mytools.utils.Tools;

public class SimpleDialog extends AlertDialog implements View.OnClickListener {


    TextView tvTitle;
    TextView tvCancel;
    TextView tvSure;
    private Context context;
    private String title;

    public SimpleDialog(Context context,String title) {
        super(context);
        this.context = context.getApplicationContext();
        this.title = title;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_simple);
        initView();
        listener();
    }

    private void listener() {

    }

    private void initView() {
        tvTitle = findViewById(R.id.tv_title);
        tvCancel = findViewById(R.id.tv_cancel);
        tvSure = findViewById(R.id.tv_sure);
        tvSure.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvTitle.setText(title);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_cancel:
                dismiss();
                break;
            case R.id.tv_sure:
                if (mOnSureClickListener!=null){
                    mOnSureClickListener.sureClick();
                }
                break;
        }
    }

    private OnSureClickListener mOnSureClickListener;
    public void setOnSureClickListener(OnSureClickListener mOnSureClickListener){
        this.mOnSureClickListener = mOnSureClickListener;
    }
    public interface OnSureClickListener{
        void sureClick();
    }
}
