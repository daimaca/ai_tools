package hlz.cn.jhc.mytools.home;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.eventbus.EventBusUtils;
import hlz.cn.jhc.mytools.greendao.TagTable;
import hlz.cn.jhc.mytools.movies.MoviesMainActivity;


public class HomeAdapter extends BaseQuickAdapter<TagTable, HomeAdapter.ViewHolder> {

    public HomeAdapter(@Nullable List<TagTable> data) {
        super(R.layout.home_item, data);
    }

    @Override
    protected void convert(ViewHolder helper, final TagTable item) {
        if (item!=null){
            if (item.getImg()==0){
                helper.ivImg.setImageResource(R.mipmap.no_img);
            }else {
                helper.ivImg.setImageResource(item.getImg());
            }
            helper.tvTitle.setText(item.getName());

            helper.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (item.getName().equals(DataModel.titles[DataModel.titles.length-1])){
                        mContext.startActivity(new Intent(mContext, HomeTagActivity.class));
                        return;
                    }
                    EventBusUtils.postStickyObject(item);
                    mContext.startActivity(new Intent(mContext, MoviesMainActivity.class));
                }
            });

        }
    }

    static class ViewHolder extends BaseViewHolder{
        @BindView(R.id.iv_img)
        CircleImageView ivImg;
        @BindView(R.id.tv_title)
        TextView tvTitle;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
