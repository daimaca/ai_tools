package hlz.cn.jhc.mytools.settings;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.sdk.CookieManager;
import com.tencent.smtt.sdk.CookieSyncManager;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.utils.TbsLog;

import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.jpush.android.api.JPushInterface;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.jpush.JPushMessageBean;
import hlz.cn.jhc.mytools.jpush.JPushType;
import hlz.cn.jhc.mytools.movies.web.X5WebView;
import hlz.cn.jhc.mytools.share.ShareUtils;
import hlz.cn.jhc.mytools.utils.Tools;

/**
 * app更新
 */
public class UpdateActivity extends BaseActivity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_head_tip_message)
    TextView tvHeadTipMessage;
    @BindView(R.id.rl_web)
    RelativeLayout rl_web;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.mTv)
    TextView mTv;

    private static final String webUrl = "https://www.pgyer.com/pRVu";
    private X5WebView mWebView;

    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_update);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
    }

    @Override
    protected void initData() {
        tvTitle.setVisibility(View.VISIBLE);
        mTv.setVisibility(View.VISIBLE);
        tvTitle.setText(getString(R.string.app_name)+"下载");
        mTv.setText("浏览器下载");
        initWeb();
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvHeadTipMessage.setText("小提示：最新版为版本号为基准；如果点击 “点击安装” 无反应,请点击右上角的 ”浏览器打开 --> 然后点击下载即可“ ");
    }

    @OnClick({R.id.back,R.id.mTv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.mTv:
                Intent intent= new Intent();
                intent.setAction("android.intent.action.VIEW");
                Uri content_url = Uri.parse(mWebView.getUrl());
                intent.setData(content_url);startActivity(intent);
                break;
        }
    }

    private void initWeb(){
        mWebView = new X5WebView(this, null);

        rl_web.addView(mWebView, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.FILL_PARENT,
                FrameLayout.LayoutParams.FILL_PARENT));

        mWebView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onProgressChanged(WebView webView, int i) {
                super.onProgressChanged(webView, i);
                if (i == 0 || i >= 100) {
                    mProgressBar.setVisibility(View.GONE);
                } else {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mProgressBar.setProgress(i);
                }
            }
        });

        settingsWeb();
        mWebView.loadUrl(webUrl);
        CookieSyncManager.createInstance(this);
        CookieSyncManager.getInstance().sync();
        CookieManager.getInstance().setAcceptCookie(true);
        CookieManager.getInstance().flush();
    }

    private void settingsWeb() {
        WebSettings webSetting = mWebView.getSettings();
        webSetting.setAllowFileAccess(true);
        webSetting.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webSetting.setSupportZoom(true);
        webSetting.setBuiltInZoomControls(true);
        webSetting.setUseWideViewPort(true);
        webSetting.setSupportMultipleWindows(false);
        // webSetting.setLoadWithOverviewMode(true);
        webSetting.setAppCacheEnabled(true);
        // webSetting.setDatabaseEnabled(true);
        webSetting.setDomStorageEnabled(true);
        webSetting.setJavaScriptEnabled(true);
        webSetting.setGeolocationEnabled(true);
        webSetting.setAppCacheMaxSize(Long.MAX_VALUE);
        webSetting.setAppCachePath(this.getDir("appcache", 0).getPath());
        webSetting.setDatabasePath(this.getDir("databases", 0).getPath());
        webSetting.setGeolocationDatabasePath(this.getDir("geolocation", 0).getPath());
        // webSetting.setPageCacheCapacity(IX5WebSettings.DEFAULT_CACHE_CAPACITY);
        webSetting.setPluginState(WebSettings.PluginState.ON_DEMAND);
        // webSetting.setRenderPriority(WebSettings.RenderPriority.HIGH);
        // webSetting.setPreFectch(true);

        /**
         * todo 设置是否是电脑版版加载web
         */
        boolean isComputer = Tools.isWebComputer();
        if (isComputer) {
            webSetting.setUserAgentString("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36");
        }
    }
}
