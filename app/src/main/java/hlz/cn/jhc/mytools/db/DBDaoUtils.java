package hlz.cn.jhc.mytools.db;

import android.content.Context;
import android.util.Log;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

import hlz.cn.jhc.mytools.greendao.TagTable;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTableDao;

/**
 *  https://blog.csdn.net/bskfnvjtlyzmv867/article/details/71250101
 */
public class DBDaoUtils {
    private static final String TAG = "DBDaoUtils";

    private DaoManager daoManager;

    public DBDaoUtils(Context context){
        daoManager = DaoManager.getInstance();
        daoManager.init(context);
    }

    /**
     * todo  插入单个记录  如果是多个记录需要在线程插入
     *
     * @param t
     * @param <T>
     */
    public <T> boolean insert(T t) {
        boolean flag = false;
        if (t instanceof TagTable) {
            flag = daoManager.getDaoSession().getTagTableDao().insert((TagTable) t) == -1 ? false : true;
        }
        if (t instanceof WatchCollectionTable) {
            flag = daoManager.getDaoSession().getWatchCollectionTableDao().insert((WatchCollectionTable) t) == -1 ? false : true;
        }
        daoManager.closeConnection();
        return flag;
    }

    /**
     * 修改一条数据
     * @param t
     * @return
     */
    public <T> boolean update(T t){
        boolean flag = false;
        try {
            daoManager.getDaoSession().update(t);
            daoManager.closeConnection();
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return flag;
    }

    /**
     *  删除一个记录
     * @param t
     * @param <T>
     */
    public <T> boolean delete(T t){
        boolean is = false;
        if (t instanceof TagTable) {
            daoManager.getDaoSession().getTagTableDao().delete((TagTable) t);
            is = true;
        }
        if (t instanceof WatchCollectionTable) {
            daoManager.getDaoSession().getWatchCollectionTableDao().delete((WatchCollectionTable) t);
            is = true;
        }
        daoManager.closeConnection();
        return is;
    }

    /**
     *  查询所有记录
     * @param t
     * @param <T>
     * @return
     */
    public <T> List<T> getList(T t){
        Log.i(TAG, "getList: ");
        if (t instanceof TagTable) {
            List<TagTable> list = daoManager.getDaoSession().loadAll(TagTable.class);
            daoManager.closeConnection();
            Log.i(TAG, "getList: "+list.size());
            return (List<T>) list;
        }
        if (t instanceof WatchCollectionTable) {
            List<WatchCollectionTable> list = daoManager.getDaoSession().loadAll(WatchCollectionTable.class);
            Log.i(TAG, "getList: "+list.size());
            daoManager.closeConnection();
            return (List<T>) list;
        }
        return null;
    }

    // 获取标签list数据
    public List<TagTable> getTagList() {
        Log.i(TAG, "getList: ");
        List<TagTable> list = daoManager.getDaoSession().loadAll(TagTable.class);
        daoManager.closeConnection();
        return list;
    }

    /**
     * 使用queryBuilder进行查询
     * @return
     */
    public List<WatchCollectionTable> getWatchCollectionList(long tagId) {
        QueryBuilder<WatchCollectionTable> queryBuilder = daoManager.getDaoSession().queryBuilder(WatchCollectionTable.class);
        List<WatchCollectionTable> list = queryBuilder.where(WatchCollectionTableDao.Properties.TagId.eq(tagId)).list();
        Log.i(TAG, "getList: ");
        daoManager.closeConnection();
        return list;
    }
}
