package hlz.cn.jhc.mytools.home.homefragment;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.ColorSpace;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.meis.widget.MeiTextPathView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.vector.update_app.UpdateAppManager;
import com.vector.update_app.UpdateCallback;
import com.youth.banner.Banner;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseFragment;
import hlz.cn.jhc.mytools.db.SharedPreferencesConst;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.greendao.TagTable;
import hlz.cn.jhc.mytools.home.DataModel;
import hlz.cn.jhc.mytools.home.HomeAdapter;
import hlz.cn.jhc.mytools.home.HomeScrollView;
import hlz.cn.jhc.mytools.movies.web.X5WebView;
import hlz.cn.jhc.mytools.utils.Tools;
import hlz.cn.jhc.mytools.utils.UpdateAppHttpUtil;
import hlz.cn.jhc.mytools.zxing.ScanActivity;

public class HomeFragment extends BaseFragment implements HomeScrollView.TranslucentListener{


    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.rlv_list)
    RecyclerView rlvList;
    @BindView(R.id.mMeiTextPathView)
    MeiTextPathView mMeiTextPathView;
    @BindView(R.id.banner)
    Banner banner;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.mHomeScrollView)
    HomeScrollView mHomeScrollView;
    @BindView(R.id.rl_alpha)
    RelativeLayout rl_alpha;
    @BindView(R.id.mFloatingActionButton)
    FloatingActionButton mFloatingActionButton;
    @BindView(R.id.iv_right_img)
    ImageView ivRightImg;
    @BindView(R.id.et_movies)
    EditText etMovies;
    @BindView(R.id.tv_movies)
    TextView tvMovies;

    @BindView(R.id.mViewParent)
    RelativeLayout mViewParent;
    @BindView(R.id.mSmartRefreshLayout)
    SmartRefreshLayout mSmartRefreshLayout;
    Unbinder unbinder;

    private HomeAdapter homeAdapter;

    private List<TagTable> homeBeans = new ArrayList<>();
    private X5WebView mWebView;


    private static HomeFragment homeFragment;
    @SuppressLint("ValidFragment")
    public HomeFragment(){}
    public static HomeFragment netInstance() {
//        if (homeFragment == null) {
//            synchronized (HomeFragment.class) {
//                if (homeFragment == null) {
//                    homeFragment = new HomeFragment();
//                }
//            }
//        }
        Bundle bundle = new Bundle();
        bundle.putString("ARG_PARAM1", "ARG_PARAM1");
        bundle.putString("ARG_PARAM2", "ARG_PARAM2");

        HomeFragment homeFragment = new HomeFragment();
        homeFragment.setArguments(bundle);
        return homeFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    //如果你需要考虑更好的体验，可以这么操作
    @Override
    public void onStart() {
        super.onStart();
        //开始轮播
        banner.startAutoPlay();
    }

    @Override
    public void onStop() {
        super.onStop();
        //结束轮播
        banner.stopAutoPlay();
    }

    @Override
    public void onResume() {
        super.onResume();
        addWeb();
    }

    @Override
    public void onPause() {
        super.onPause();
        removeWeb();
    }

    @Override
    protected void initData() {

        rl_alpha.setAlpha(0);
//        mMeiTextPathView.setVisibility(View.VISIBLE);
        back.setVisibility(View.GONE);
//        back.setImageResource(R.drawable.user);
        tvTitle.setText("AI小志");
        ivRightImg.setVisibility(View.VISIBLE);
        mHomeScrollView.setOnScrollChangedListener(this);
        HomeFragmentUtils.initBanner(banner,getContext());
        initHomeData();

        updateApp();
    }

    private void initHomeData() {

        homeBeans.addAll(DataModel.getListHome());
        GridLayoutManager manager = new GridLayoutManager(getContext(), 4);
        homeAdapter = new HomeAdapter(homeBeans);
        rlvList.setLayoutManager(manager);
        rlvList.setAdapter(homeAdapter);
        Tools.setListAnimation(getContext(), rlvList);

    }

    @OnClick({R.id.mFloatingActionButton,R.id.iv_right_img,R.id.et_movies,R.id.tv_movies})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mFloatingActionButton:
                getActivity().finish();
                break;
            case R.id.iv_right_img:
                startActivity(new Intent(getContext(), ScanActivity.class));
                break;
            case R.id.et_movies:
//                showSoftInputFromWindow();
                break;
            case R.id.tv_movies:
                queryMovies();
                break;
        }
    }

    private void queryMovies() {
        String name = etMovies.getText().toString().trim();
        if (TextUtils.isEmpty(name)){
            Tools.showShortToast("请输入视频名称");
            return;
        }
        Intent intent = new Intent();
        intent.setClass(getContext(),SearchMoviesActivity.class);
        intent.putExtra(SearchMoviesActivity.MOVIES_URL_TAG,name);
        startActivity(intent);
    }

    private void showSoftInputFromWindow(){
        etMovies.setFocusable(true);
        etMovies.setFocusableInTouchMode(true);
        etMovies.requestFocus();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
//        etMovies.findFocus();
//        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_NOT_ALWAYS);
    }
    private void hideSoftInputFromWindow(){
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etMovies.getWindowToken(), 0); //强制隐藏键盘
    }


    private void addWeb() {
        int theme = SharedPreferencesUtils.getKeyValue(getContext(), SharedPreferencesConst.ThemePositionKey, 0);
        if (theme == 0) {
//            mHomeScrollView.setBackgroundResource(R.drawable.home_bg);
            mHomeScrollView.setBackgroundResource(R.drawable.xiaozhi_bg);
            return;
        }
        mHomeScrollView.setBackgroundResource(0);
        mWebView = new X5WebView(getActivity(), null);
        mViewParent.addView(mWebView, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));

        Tools.setTheme(mWebView, theme);
    }

    private void removeWeb() {
        if (mViewParent != null) {
            mViewParent.removeView(mWebView);
        }
        if (mWebView != null) {
            mWebView.destroy();
            mWebView = null;
        }
    }

    @Override
    public void onTranslucent(float alpha) {
        rl_alpha.setAlpha(alpha);
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setAlpha(alpha);
    }


    private void updateApp(){
        new UpdateAppManager
                .Builder()
                //当前Activity
                .setActivity(getActivity())
                //更新地址
                .setUpdateUrl("https://www.pgyer.com/apiv2/app/check?_api_key=1a1fbd4d56a890a6f76ae26160586116&appKey=fae16db8047866704f983b6d289d3fcd")
                //实现httpManager接口的对象
                .setHttpManager(new UpdateAppHttpUtil())
                .setPost(false)
                .build()
                .checkNewApp(new UpdateCallback(){
                    @Override
                    protected com.vector.update_app.UpdateAppBean parseJson(String json) {
                        com.vector.update_app.UpdateAppBean updateAppBean = new com.vector.update_app.UpdateAppBean();
                        try {
                            UpdateAppBean bean = new Gson().fromJson(json,UpdateAppBean.class);
                            if (bean != null){
                                if (!TextUtils.equals(Tools.getAppVersion(),bean.getData().getBuildVersion())){
                                    updateAppBean.setUpdate("Yes")
                                            //存放json，方便自定义解析
                                            .setOriginRes(json)
                                            .setNewVersion(bean.getData().getBuildVersion())
                                            .setApkFileUrl(bean.getData().getDownloadURL())
                                            .setTargetSize(null)
                                            .setUpdateLog(bean.getData().getBuildUpdateDescription())
                                            .setConstraint(false)
                                            .setNewMd5(null);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return updateAppBean;
                    }
                });
    }
}
