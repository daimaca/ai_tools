package hlz.cn.jhc.mytools.home.homefragment;

public class SearchMoviesData {

    public static final String[] titles = new String[]{
            "电影院","疯狂电影","4k屋","田鸡影院","二十影院",
            "七七影院","vip影院","疯狂电影迷","悠久影院","九八零影院",
            "青苹果影院","沐享影院","野兔影院","夏日影院","迅播影院",
            "八一影院","3397影视","电影吧","欧呐呐","部落电影",
            "云影院","看精品","叮当动漫","雅阁视频","谍咖网",
            "看看屋","爱看番",
    };

    public static final String[] urls = new String[]{
            "https://www.chok8.com/vodsearch/-------------/?wd=《》&submit=",
            "http://ifkdy.com/?q=《》&p=1",
            "http://m.kkkkmao.com/vod-search-wd-《》-p-1.html",
            "http://www.mtotoo.com/index.php?m=vod-search-pg-1-wd-《》.html",
            "http://esyy007.com/index.php?m=vod-search-pg-1-wd-《》.html",

            "http://m.77kyy.com/vod-search-wd-《》.html",
            "http://www.sjzvip.cn/seacher.php?sousuo=《》&qiehuan=1",
            "http://fkdy.me/index.php?m=vod-search-pg-1-wd-《》.html",
            "http://m.9bbg.com/search.php?page=1&searchword=《》&searchtype=",
            "http://www.tv980.com/search.php?page=1&searchword=《》&searchtype=",

            "http://m.lalalo.com/search.php?page=1&searchword=《》&searchtype=",
            "http://vip.muyl.vip/index.php/vod/search/page/1/wd/《》.html",
            "https://m.ywt.cc/search/《》.html",
            "http://www.summerggw.com/search.php?page=1&searchword=《》",
            "http://www.2tu.cc/index.php?m=vod-search-wd-《》.html",

            "http://m.bayiyy.com/soupian/?key=《》",
            "http://m.w3397.com/search-0-1-《》.html",
            "https://m.dianyingbar.com/search/pg-1-wd-《》.html",
            "https://www.ounana.com/vodsearch/《》----------1---/",
            "https://m.vbuluo.com/search/《》-1.html",

            "http://www.kkyyy.cc/vodsearch/《》----------1---.html",
            "http://www.kanjp.com/vodsearch/《》----------1---.html",
            "http://m.kt51.com/search.php?page=1&searchword=《》&searchtype=",
            "https://www.yageys.com/vod/search/wd/《》/page/1.html",
            "https://www.dieka123.com/search.php?page=1&searchword=《》&searchtype=",

            "https://m.kankanwu.com/vod-search-wd-《》-p-1.html",
            "http://www.ikanfan.com/search/《》-1.html",

    };
}
