package hlz.cn.jhc.mytools.movies.web;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.comment.DownLoadImagesThread;
import hlz.cn.jhc.mytools.db.SharedPreferencesConst;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.home.SimpleDialog;
import hlz.cn.jhc.mytools.utils.Tools;

public class LoadResourceActivity extends BaseActivity {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.mTv)
    TextView mTv;
    @BindView(R.id.tv_head_tip_message)
    TextView tvHeadTipMessage;
    @BindView(R.id.rlv_list)
    RecyclerView rlvList;
    @BindView(R.id.mSwitchGetResource)
    Switch mSwitchGetResource;


    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_load_resource);
        ButterKnife.bind(this);
    }

    @Override
    protected void initData() {
        tvTitle.setVisibility(View.VISIBLE);
        mTv.setVisibility(View.VISIBLE);
        tvTitle.setText("提取资源");
        mTv.setText("一键下载");
        tvHeadTipMessage.setText("已为你获取" + LoadResourceAdapter.getLoadResourceList().size() + "个资源");

        LoadResourceAdapter loadResourceAdapter = new LoadResourceAdapter(LoadResourceAdapter.getLoadResourceList());
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        rlvList.setLayoutManager(layoutManager);
        rlvList.setAdapter(loadResourceAdapter);

        mSwitchGetResource.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferencesUtils.setKeyValue(SharedPreferencesConst.isGetResourceKey, b);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        mSwitchGetResource.setChecked(Tools.isGetResource());
    }

    @OnClick({R.id.back,R.id.mTv})
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;
                case R.id.mTv:
                downloadImages();
                break;
        }

    }

    private void downloadImages() {

        final SimpleDialog simpleDialog = new SimpleDialog(this,"是否一键下载到相册？");
        simpleDialog.show();
        simpleDialog.setOnSureClickListener(new SimpleDialog.OnSureClickListener() {
            @Override
            public void sureClick() {
                if (LoadResourceAdapter.getLoadResourceList().size()==0){
                    Tools.showShortToast("未获取到资源！");
                    simpleDialog.dismiss();
                    return;
                }
                Tools.showShortToast("已在后台下载中，请到相册擦看！");
                new DownLoadImagesThread(LoadResourceActivity.this,LoadResourceAdapter.getLoadResourceList()).start();
                simpleDialog.dismiss();
            }
        });
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("是否一键下载到相册？")
//                .setMessage("可能消耗流量较多,土豪请随意！！！")
//                .setNegativeButton("确定", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        Tools.showShortToast("已在后台下载中，请到相册擦看！");
//                        new DownLoadImagesThread(LoadResourceActivity.this,LoadResourceAdapter.getLoadResourceList()).start();
//                    }
//                }).setPositiveButton("取消",null).show();

    }

//    static class DownLoadImagesThread extends Thread{
//        private Context context;
//        private List<String> urls;
//        public DownLoadImagesThread(Context context,List<String> urls){
//            this.context = context.getApplicationContext();
//            this.urls = urls;
//        }
//        @Override
//        public void run() {
//            File file = null;
//            try {
//                for (int i = 0; i < urls.size(); i++) {
//                    file = Glide.with(context).asFile().load(urls.get(i)).submit().get();
//                    displayToGallery(context,file);
//                    Thread.sleep(200);
//                }
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }
}
