package hlz.cn.jhc.mytools.settings;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.glomadrian.grav.GravView;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.umeng.UMengUtils;
import hlz.cn.jhc.mytools.utils.Tools;
import me.james.biuedittext.BiuEditText;

/**
 * 反馈
 */
public class FeedBackActivity extends BaseActivity {


    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.mBiuEditText)
    BiuEditText mBiuEditText;
    @BindView(R.id.btn_bottom)
    Button btnBottom;
    @BindView(R.id.mConstraintLayoutPaoPao)
    ConstraintLayout mConstraintLayoutPaoPao;
    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);
    }

    @Override
    protected void initData() {
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText("反馈");
        btnBottom.setText("一键反馈");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Tools.isOpenPaoPao(mConstraintLayoutPaoPao);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mConstraintLayoutPaoPao.removeAllViews();
    }

    @OnClick({R.id.back, R.id.btn_bottom})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.btn_bottom:
                submit();
                break;
        }
    }

    private void submit() {
        String content = mBiuEditText.getText().toString().trim();
        if (TextUtils.isEmpty(content)){
            Tools.showShortToast("请输入反馈内容！");
            return;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");// HH:mm:ss
        //获取当前时间
        Date date = new Date(System.currentTimeMillis());
        String time = simpleDateFormat.format(date);
        HashMap<String,String> map = new HashMap<>();
        map.put("time",time+"");//时间
        map.put("content",content);//反馈内容
        map.put("title","一键反馈");//反馈内容
        Log.i("eventIdUserFeedBack", "submit: "+new Gson().toJson(map));
        UMengUtils.onEvent(this.getApplicationContext(),UMengUtils.eventIdUserFeedBack,map);
        Tools.showShortToast("反馈成功，管理员将会尽快处理，感谢你的反馈！");
        finish();
    }
}
