package hlz.cn.jhc.mytools.utils;

import android.util.Log;

public class LogUtils {

    private static final boolean isOpen = true;

    public static void Logi(String TAG, String text) {
        if (isOpen) {
            Log.i(TAG, "Log-i: -------" + text);
        }
    }

    public static void Loge(String TAG, String text) {
        if (isOpen) {
            Log.e(TAG, "Log-e: -------" + text);
        }
    }

    public static void Logd(String TAG, String text) {
        if (isOpen) {
            Log.d(TAG, "Log-d: -------" + text);
        }
    }
}
