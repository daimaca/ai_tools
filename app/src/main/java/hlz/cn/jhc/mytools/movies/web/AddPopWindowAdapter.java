package hlz.cn.jhc.mytools.movies.web;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.utils.Tools;

public class AddPopWindowAdapter extends BaseQuickAdapter<AddPopWindowAdapter.PopWindowBean, AddPopWindowAdapter.ViewHolder> {

    public static String[] titles = new String[]{"专业解析","提取资源"};
    public static int[] imgs = {0,0};



    public AddPopWindowAdapter(@Nullable List<PopWindowBean> data) {
        super(R.layout.add_popwindow_menu_item, data);
    }

    @Override
    protected void convert(ViewHolder helper, PopWindowBean item) {
        if (item != null){
            if (item.getImg() == 0){
                Glide.with(mContext).load(R.mipmap.no_img).apply(Tools.getNoImageRequestOptions()).into(helper.ivImg);
            }else {
                Glide.with(mContext).load(item.getImg()).apply(Tools.getNoImageRequestOptions()).into(helper.ivImg);
            }
            helper.tvTitle.setText(item.getTitle());
        }
    }

    public static List<PopWindowBean> getPopWindowData(){
        List<PopWindowBean> beans = new ArrayList<>();
        for (int i = 0; i < titles.length; i++) {
            PopWindowBean bean = new PopWindowBean();
            bean.setImg(0);
            bean.setTitle(titles[i]);
            beans.add(bean);
        }
        return beans;
    }

    static class ViewHolder extends BaseViewHolder{
        @BindView(R.id.iv_img)
        CircleImageView ivImg;
        @BindView(R.id.tv_title)
        TextView tvTitle;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    static class PopWindowBean {
        private int img;
        private String title;

        public int getImg() {
            return img;
        }

        public void setImg(int img) {
            this.img = img;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }


}
