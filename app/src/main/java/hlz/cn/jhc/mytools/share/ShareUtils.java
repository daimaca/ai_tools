package hlz.cn.jhc.mytools.share;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.comment.FightImageThread;
import hlz.cn.jhc.mytools.comment.ShareImageThread;
import hlz.cn.jhc.mytools.utils.Tools;

public class ShareUtils {
    public static final String title = "将AI小志分享到";

    /**
     * 分享单个文本
     * @param context
     * @param text
     */
    public static void shareText(Context context,String text) {
        if (TextUtils.isEmpty(text)) {
            Tools.showShortToast("未获取到分享内容！");
            return;
        }
        Intent textIntent = new Intent(Intent.ACTION_SEND);
        textIntent.setType("text/plain");
        textIntent.putExtra(Intent.EXTRA_TEXT, text);
        context.startActivity(Intent.createChooser(textIntent, title));
    }

    /**
     * 分享多个文本
     * @param context
     * @param texts
     */
    public static void shareTexts(Context context, ArrayList<String> texts) {
        if (texts == null) {
            Tools.showShortToast("未获取到分享内容！");
            return;
        }
        Intent textIntent = new Intent(Intent.ACTION_SEND);
        textIntent.setType("text/plain");
        textIntent.putStringArrayListExtra(Intent.EXTRA_TEXT, texts);
        context.startActivity(Intent.createChooser(textIntent, title));
    }

    /**
     * 分享单张图片
     * @param context
     * @param url
     */
    public static void shareImage(final Context context, final String url){
        new ShareImageThread(context,url).start();
    }

    /**
     * 分享单张图片
     * @param context
     * @param res
     */
    public static void shareImage(final Context context, final int res){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap = null;
                try {
                    bitmap = Glide.with(context).asBitmap().load(res).submit().get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (bitmap == null) {
                    return;
                }
                Uri imageUri = getImageUri(context,bitmap);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
                shareIntent.setType("image/*");
                context.startActivity(Intent.createChooser(shareIntent, title));
            }
        }).start();

    }

    /**
     * 分享多张图片
     * @param context
     * @param urls
     */
    public static void shareImages(Context context,ArrayList<String> urls){

        ArrayList<Uri> uriList = new ArrayList<>();
        for (int i = 0; i < urls.size(); i++) {
            Uri uri = getImageUri(context, urls.get(i));
            uriList.add(uri);
        }
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uriList);
        shareIntent.setType("image/*");
        context.startActivity(Intent.createChooser(shareIntent, title));
    }

    public static Uri getImageUri(Context context,Bitmap bitmap){
        if (bitmap == null) {
            return null;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {//https://blog.csdn.net/smileiam/article/details/79753745
            // 首先保存图片
            File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsoluteFile();//注意小米手机必须这样获得public绝对路径
            String fileName = "AI小志相册";
            File appDir = new File(file ,fileName);
            if (!appDir.exists()) {
                appDir.mkdirs();
            }
            fileName = System.currentTimeMillis() + ".jpg";
            File currentFile = new File(appDir, fileName);

            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(currentFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (fos != null) {
                        fos.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // 最后通知图库更新
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                    Uri.fromFile(new File(currentFile.getPath()))));
            return FileProvider.getUriForFile(context, "hlz.cn.jhc.mytools.fileProvider", currentFile);
        } else {
            Uri uri = Uri.parse(MediaStore.Images.Media.insertImage(context.getContentResolver(),bitmap , null,null));
//            String path = getAbsoluteImagePath(context,uri);
            // 最后通知图库更新
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                    uri));
            return uri;
        }

    }

    public static Uri getImageUri(Context context, String urlImage) {
        try {
            Bitmap bitmap = Glide.with(context).asBitmap().load(urlImage).submit().get();
            if (bitmap == null) {
                return null;
            }
            return getImageUri(context, bitmap);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 斗图轰炸
     * @param context
     * @param url
     * @param count
     */
    public static void shareFightImages(Context context,String url,int count){
        new FightImageThread(context,url,count).start();
    }


    public static  String getAbsoluteImagePath(Context context,Uri contentUri) {

        //如果是对媒体文件，在android开机的时候回去扫描，然后把路径添加到数据库中。
        //由打印的contentUri可以看到：2种结构。正常的是：content://那么这种就要去数据库读取path。
        //另外一种是Uri是 file:///那么这种是 Uri.fromFile(File file);得到的
        System.out.println(contentUri);

        String[] projection = { MediaStore.Images.Media.DATA };
        String urlpath;
        CursorLoader loader = new CursorLoader(context,contentUri, projection, null,null ,null);
        Cursor cursor = loader.loadInBackground();
        try {
            int column_index = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            urlpath =cursor.getString(column_index);
            //如果是正常的查询到数据库。然后返回结构
            return urlpath;
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }finally{
            if(cursor != null){
                cursor.close();
            }
        }

        //如果是文件。Uri.fromFile(File file)生成的uri。那么下面这个方法可以得到结果
        urlpath = contentUri.getPath();
        return urlpath;
    }
}
