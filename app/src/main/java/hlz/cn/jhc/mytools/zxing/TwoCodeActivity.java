package hlz.cn.jhc.mytools.zxing;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.google.zxing.client.result.ParsedResultType;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.comment.DownLoadImageThread;
import hlz.cn.jhc.mytools.utils.Tools;
import me.james.biuedittext.BiuEditText;

public class TwoCodeActivity extends BaseActivity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.mRadioButton1)
    RadioButton mRadioButton1;
    @BindView(R.id.mRadioButton2)
    RadioButton mRadioButton2;
    @BindView(R.id.mRadioGroup)
    RadioGroup mRadioGroup;
    @BindView(R.id.editUrl1)
    BiuEditText editUrl1;
    @BindView(R.id.iv_code)
    ImageView ivCode;
    @BindView(R.id.btn_save)
    Button btnSave;
    @BindView(R.id.btn_create_bitmap)
    Button btnCreateBitmap;

    private ParsedResultType type = ParsedResultType.URI;

    private String mUrl = "";

    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_two_code);
        mUrl = getIntent().getStringExtra("url");
        ButterKnife.bind(this);

    }

    @Override
    protected void initData() {
        tvTitle.setText("二维码生成");
        tvTitle.setVisibility(View.VISIBLE);
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.mRadioButton1:
                        editUrl1.setHint("请输入链接，如以 https或http或www 开头");
                        type = ParsedResultType.URI;
                        break;
                    case R.id.mRadioButton2:
                        editUrl1.setHint("请输入内容");
                        type = ParsedResultType.TEXT;
                        break;
                }

            }
        });

        new MyThread().start();
    }

    class MyThread extends Thread{
        @Override
        public void run() {
            try {
                Thread.sleep(500);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!TextUtils.isEmpty(mUrl)){
                            editUrl1.setText(mUrl);
                        }else {
                            editUrl1.setText("");
                        }
                    }
                });
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick({R.id.back, R.id.btn_save, R.id.btn_create_bitmap})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.btn_save:
                if (bitmap == null) {
                    Tools.showShortToast("请生成图片");
                    return;
                }
                Tools.showShortToast("已保存到相册，请在相册中查看！");
                new ZXingUtils.DownLoadImageThread(this,bitmap).start();
                break;
            case R.id.btn_create_bitmap:
                commit();
                break;
        }
    }

    private Bitmap bitmap;

    private void commit() {
        String text = editUrl1.getText().toString().trim();
        if (TextUtils.isEmpty(text)) {
            Tools.showShortToast("请输入内容");
            return;
        }
        bitmap = ZXingUtils.createBitmap(this, text, type);
        ivCode.setImageBitmap(bitmap);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }
    }
}
