package hlz.cn.jhc.mytools.jpush;

import android.os.Parcel;
import android.os.Parcelable;

public class JPushMessageBean implements Parcelable {
    private String title;
    private String type;
    private String content;

    public JPushMessageBean(){}

    protected JPushMessageBean(Parcel in) {
        title = in.readString();
        type = in.readString();
        content = in.readString();
    }

    public static final Creator<JPushMessageBean> CREATOR = new Creator<JPushMessageBean>() {
        @Override
        public JPushMessageBean createFromParcel(Parcel in) {
            return new JPushMessageBean(in);
        }

        @Override
        public JPushMessageBean[] newArray(int size) {
            return new JPushMessageBean[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(type);
        parcel.writeString(content);
    }
}
