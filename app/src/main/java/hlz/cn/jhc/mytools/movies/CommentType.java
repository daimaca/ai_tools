package hlz.cn.jhc.mytools.movies;

/**
 * Created by Administrator on 2018/8/19.
 */

public interface CommentType {
    String VideoCollection = "VideoCollection";//视频集合
    String live = "live";//直播
    String MapNavigation = "MapNavigation";//地图导航
    String shopping = "shopping";//购物
    String learning = "learning";//学习
    String comic = "comic";//漫画
    String news = "news";//新闻
    String music = "music";//音乐
    String novel = "novel";//小说
}
