package hlz.cn.jhc.mytools.eventbus;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Administrator on 2018/8/12.
 */

public class EventBusUtils {

    /**
     * 注册 EventBus
     *
     * @param subscriber
     */
    public static void register(Object subscriber) {
        EventBus eventBus = EventBus.getDefault();
        if (!eventBus.isRegistered(subscriber)) {
            eventBus.register(subscriber);
        }
    }

    /**
     * 解除注册 EventBus
     *
     * @param subscriber
     */
    public static void unregister(Object subscriber) {
        EventBus eventBus = EventBus.getDefault();
        if (eventBus.isRegistered(subscriber)) {
            eventBus.unregister(subscriber);
        }
    }

    /**
     * 发送事件消息
     *
     * @param event
     */
    public static void post(EventMessage event) {
        EventBus.getDefault().post(event);
    }

    /**
     * 发送粘性事件消息
     *
     * @param event
     */
    public static void postSticky(EventMessage event) {
        EventBus.getDefault().postSticky(event);
    }

    public static void postStickyObject(Object event) {
        EventBus.getDefault().postSticky(event);
    }

    public static void removePostSticky(Object subscriber) {
        EventBus.getDefault().removeStickyEvent(subscriber);
    }
}
