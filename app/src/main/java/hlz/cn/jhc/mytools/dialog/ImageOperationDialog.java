package hlz.cn.jhc.mytools.dialog;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.comment.DownLoadImageThread;
import hlz.cn.jhc.mytools.comment.MessageBombActivity;
import hlz.cn.jhc.mytools.share.ShareUtils;
import hlz.cn.jhc.mytools.utils.Tools;

public class ImageOperationDialog extends BaseDialog {

    private RecyclerView rlvList;
    private String url;
    private Context context;

    public ImageOperationDialog(Context context,String url) {
        super(context, R.style.BottomDialogAnimationStyle);
        this.url = url;
        this.context = context;
    }

    @Override
    void setLayout() {
        setContentView(R.layout.dialog_image_operation);
        setBottomAnimation();
    }

    @Override
    void initView() {
        rlvList = findViewById(R.id.rlv_list);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        ImageOperationDialogAdapter adapter = new ImageOperationDialogAdapter(ImageOperationDialogAdapter.getImageData());
        rlvList.setLayoutManager(manager);
        rlvList.setAdapter(adapter);

        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                switch (position) {
                    case 0://下载
                        new DownLoadImageThread(context,url).start();
                        Tools.showShortToast("已下载，请到相册查看！");
                        dismiss();
                        break;
                    case 1://分享
                        ShareUtils.shareImage(getContext(),url);
                        dismiss();
                        break;
                    case 2://斗图轰炸
//                        ShareUtils.shareFightImages(getContext(),url,50);
                        dismiss();
                        Intent intent = new Intent();
                        intent.putExtra(MessageBombActivity.URL_FLAG,url);
                        intent.setClass(getContext(),MessageBombActivity.class);
                        getContext().startActivity(intent);
                        break;
                    case 3://取消
                        dismiss();
                        break;
                }

            }
        });
    }
}
