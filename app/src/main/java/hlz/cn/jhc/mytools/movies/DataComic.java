package hlz.cn.jhc.mytools.movies;

import java.util.ArrayList;
import java.util.List;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;

/**
 * todo 漫画
 * Created by Administrator on 2018/6/17.
 */

public class DataComic {
    public static String[] names = new String[]{"动漫之家", "动漫屋", "搜漫画", "极速漫画", "漫本",
            "SF漫画", "知音漫客", "有妖气", "hao123漫画", "布卡漫画",
            "快看漫画", "漫漫漫画", "腾讯漫画", "网易漫画", "暴走漫画",
            "微博漫画","漫画岛","土豪漫画","奇妙漫画"
    };
    public static int[] img = new int[]{
            R.mipmap.img_dongmanzhijia_comic, R.mipmap.img_dongmanwu_comic, R.mipmap.img_shoumanhua_comic, R.mipmap.img_jisu_comic, R.mipmap.img_manben_comic,
            R.mipmap.img_sf_comic, R.mipmap.img_zhiyinmanke_comic, R.mipmap.img_youyaoqi_comic, R.mipmap.img_hao123_comic, R.mipmap.img_buka_comic,
            R.mipmap.img_kuaikan_comic, R.mipmap.img_manman_comic, R.mipmap.img_tengxunmanhua_comic, R.mipmap.img_wangyi_comic, R.mipmap.img_baozhou_comic,
            R.mipmap.img_weibomanhua_comic,0,0,0,
    };

    public static String[] urls = new String[]{"https://manhua.dmzj.com/update_1.shtml",//1
            "http://www.dm5.com/",//2
            "http://www.somanhua.com/",//3
            "http://www.1kkk.com/",//4
            "http://www.manben.com/",//5

            "https://manhua.sfacg.com/",//6
            "https://www.zymk.cn/",//7
            "http://www.u17.com/",//8
            "https://www.hao123.com/manhua",//9
            "http://www.buka.cn/",//10

            "http://www.kuaikanmanhua.com/",//11
            "https://www.manmanapp.com/",//12
            "https://ac.qq.com/",//13
            "https://manhua.163.com/",//14
            "http://baozoumanhua.com/",//15

            "http://manhua.weibo.com/",//16
            "http://www.manhuadao.cn/",//16
            "https://www.tohomh123.com/",//土豪漫画
            "https://www.qimiaomh.com/",//奇妙漫画
    };

    /**
     *  "http://www.1966w.com/",//20 7D影城
     */

    /**
     * todo 视频平台列表
     *
     * @return
     */


    public static List<WatchCollectionTable> getList() {
        List<WatchCollectionTable> movicesModels = new ArrayList<>();

        movicesModels = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            WatchCollectionTable model = new WatchCollectionTable();
            model.setTitle(names[i]);
            model.setUrl(urls[i]);
            model.setImg(img[i]);
            model.setIsOwnDefined("1");
            movicesModels.add(model);
        }

        return movicesModels;
    }
}
