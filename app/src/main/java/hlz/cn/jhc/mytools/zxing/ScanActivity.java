package hlz.cn.jhc.mytools.zxing;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.zxing.Result;
import com.google.zxing.client.result.ParsedResult;
import com.google.zxing.client.result.ParsedResultType;
import com.mylhyl.zxing.scanner.OnScannerCompletionListener;
import com.mylhyl.zxing.scanner.ScannerView;
import com.mylhyl.zxing.scanner.decode.QRDecode;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.eventbus.EventBusContst;
import hlz.cn.jhc.mytools.eventbus.EventBusUtils;
import hlz.cn.jhc.mytools.eventbus.EventMessage;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;
import hlz.cn.jhc.mytools.movies.web.X5WebActivity;
import hlz.cn.jhc.mytools.utils.Tools;
import hlz.cn.jhc.mytools.zxing.pic.PickPictureTotalActivity;

public class ScanActivity extends BaseActivity implements OnScannerCompletionListener {
    private static final String TAG = "ScanActivity";

    @BindView(R.id.scanner_view)
    ScannerView mScannerView;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.mTv)
    TextView mTv;
    @BindView(R.id.iv_isLight)
    ImageView ivIsLight;
    @BindView(R.id.iv_two_code)
    ImageView ivTwoCode;

    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_zxing_scan);
        ButterKnife.bind(this);
        EventBusUtils.register(this);
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBusUtils.unregister(this);
    }

    @Override
    protected void initData() {

    }

    private void init() {
        tvTitle.setText("扫描");
        tvTitle.setVisibility(View.VISIBLE);
        mTv.setText("相册");
        mTv.setVisibility(View.VISIBLE);
        camra();

        ZXingUtils.setScannerViewStyle(mScannerView, this);
        mScannerView.setOnScannerCompletionListener(this);
    }

    private void camra() {
        if (ContextCompat.checkSelfPermission(ScanActivity.this, Manifest.permission
                .CAMERA) != PackageManager.PERMISSION_GRANTED) {
            //权限还没有授予，需要在这里写申请权限的代码
            ActivityCompat.requestPermissions(ScanActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    APPLY_READ_EXTERNAL_STORAGE);
        }
    }


    @Override
    protected void onResume() {
        mScannerView.onResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        mScannerView.onPause();
        super.onPause();
    }


    public static final int APPLY_READ_EXTERNAL_STORAGE = 0x111;

    private void openPhoto() {
        if (ContextCompat.checkSelfPermission(ScanActivity.this, Manifest.permission
                .WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //权限还没有授予，需要在这里写申请权限的代码
            ActivityCompat.requestPermissions(ScanActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    APPLY_READ_EXTERNAL_STORAGE);
        } else {
            PickPictureTotalActivity.gotoActivity(ScanActivity.this);
        }
    }

    private boolean isLight = false;

    private void isOpenLight() {
        if (ZXingUtils.hasFlash()) {
            if (isLight) {
                ZXingUtils.isLight(mScannerView, false);
                isLight = false;
            } else {
                ZXingUtils.isLight(mScannerView, true);
                isLight = true;
            }
        } else {
            Tools.showShortToast("该设备不支持！");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_CANCELED && resultCode == Activity.RESULT_OK) {
            if (requestCode == PickPictureTotalActivity.REQUEST_CODE_SELECT_PICTURE) {
                String picturePath = data.getStringExtra(PickPictureTotalActivity
                        .EXTRA_PICTURE_PATH);


            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN,sticky = true)
    public void getPicPath(PickPictureBean bean){
        if (bean!=null){
            new MyThread(bean.getPicPath()).start();
        }
        EventBusUtils.removePostSticky(bean);
    }

    class MyThread extends Thread{
        private String path;
        public MyThread(String path){
            this.path = path;
        }
        @Override
        public void run() {
            QRDecode.decodeQR(path, ScanActivity.this);
        }
    }

    @Override
    public void onScannerCompletion(final Result rawResult, final ParsedResult parsedResult, final Bitmap barcode) {
        Log.i(TAG, "onScannerCompletion: ");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                EventBusUtils.postSticky(new EventMessage(EventBusContst.CMD_FINISH_ACTIVITY));
                scanOpration(rawResult, parsedResult, barcode);

            }
        });

    }

    /**
     * todo 扫描结果操作
     *
     * @param rawResult
     * @param parsedResult
     * @param barcode
     */
    private void scanOpration(Result rawResult, ParsedResult parsedResult, Bitmap barcode) {
        if (rawResult != null) {
//            Intent data = new Intent();
//            data.putExtra("scan_data", rawResult.getText());
//            data.putExtra("uri", parsedResult.getDisplayResult());
//            data.putExtra("type", parsedResult.getType());
//            setResult(RESULT_OK, data);
//            Log.i(TAG, "scanOpration: rawResult 1   " + new Gson().toJson(rawResult));
//            Log.i(TAG, "scanOpration: rawResult 2  " + new Gson().toJson(parsedResult) + "/" + parsedResult.getDisplayResult());
//            Log.i(TAG, "scanOpration: rawResult 3  " + new Gson().toJson(barcode));
//            Log.i(TAG, "scanOpration: " + parsedResult.getDisplayResult() + "///////" + parsedResult.getType().name());
            if (parsedResult.getType() == ParsedResultType.URI) { // {"uri":"http://m.atobo.com.cn/u/0ds11893265","type":"URI"}
                String url = parsedResult.getDisplayResult();
                WatchCollectionTable bean = new WatchCollectionTable();
                bean.setUrl(url);
                bean.setTitle("扫描结果");
                Intent intent = new Intent(this, X5WebActivity.class);
                intent.putExtra("bean", bean);
                startActivity(intent);
            } else {//{"normalizedProductID":"061720000077","productID":"06172747","type":"PRODUCT"}
                Intent intent = new Intent();
                intent.putExtra(TextActivity.RESULT_TEXT_KEY, parsedResult.getDisplayResult());
                intent.setClass(this, TextActivity.class);
                startActivity(intent);
            }
//            finish();
        } else {
            Tools.showShortToast("未扫描到 二维码");
//            finish();
        }
    }

    @OnClick({R.id.back, R.id.mTv,R.id.iv_isLight, R.id.iv_two_code})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.mTv:
                openPhoto();
                break;
            case R.id.iv_isLight:
                isOpenLight();
                break;
            case R.id.iv_two_code:
                startActivity(new Intent(this,TwoCodeActivity.class));
                break;
        }
    }
}
