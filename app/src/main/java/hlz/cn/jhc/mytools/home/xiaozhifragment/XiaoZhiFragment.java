package hlz.cn.jhc.mytools.home.xiaozhifragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mcxtzhang.swipemenulib.SwipeMenuLayout;
import com.tencent.smtt.sdk.TbsVideo;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.sharerec.recorder.Recorder;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseFragment;
import hlz.cn.jhc.mytools.home.Game2048Activity;
import hlz.cn.jhc.mytools.mob.ShareRECUtils;
import hlz.cn.jhc.mytools.movies.web.BrowserActivity;
import hlz.cn.jhc.mytools.shortvideo.ShootVideoActivity;
import hlz.cn.jhc.mytools.utils.Tools;

public class XiaoZhiFragment extends BaseFragment {


    @BindView(R.id.editUrl1)
    EditText editUrl1;
    @BindView(R.id.btnGo1)
    TextView btnGo1;
    @BindView(R.id.ll_2048)
    LinearLayout ll2048;
    @BindView(R.id.ll_rec)
    LinearLayout llRec;
    @BindView(R.id.ll_more)
    LinearLayout llMore;
    @BindView(R.id.ll_browser)
    LinearLayout ll_browser;
    Unbinder unbinder;

    private static final String STOP = "STOP";
    private static final String START = "START";
    private static final String PAUSE = "PAUSE";
    private static final String RESUME = "RESUME";
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_start)
    TextView tvStart;
    @BindView(R.id.tv_pause)
    TextView tvPause;
    @BindView(R.id.tv_resume)
    TextView tvResume;
    @BindView(R.id.tv_stop)
    TextView tvStop;
    @BindView(R.id.tv_load_video)
    TextView tvLoadVideo;
    @BindView(R.id.mSwipeMenuLayout)
    SwipeMenuLayout mSwipeMenuLayout;
    private int time = 0;
    private String recStatus = STOP;

    private Timer timer;

    @SuppressLint("ValidFragment")
    public XiaoZhiFragment() {
    }

    public static XiaoZhiFragment netInstance() {
        Bundle bundle = new Bundle();
        XiaoZhiFragment xiaoZhiFragment = new XiaoZhiFragment();
        xiaoZhiFragment.setArguments(bundle);
        return xiaoZhiFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_xiaozhi, container, false);
        unbinder = ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    protected void initData() {

    }

    private void startTimer() {
        if (timer == null) {
            timer = new Timer();
        }
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (recStatus.equals(START) || recStatus.equals(RESUME)) {
                            time++;
                            tvTime.setText(String.valueOf(time) + " s");
                        } else if (recStatus.equals(STOP)) {
                            time = 0;
                            tvTime.setText(String.valueOf(time) + " s");
                            stopTimer();
                        }

                    }
                });
            }
        }, 0, 1000);
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        stopTimer();
    }

    @OnClick({R.id.btnGo1, R.id.ll_2048, R.id.ll_rec, R.id.ll_more, R.id.ll_browser})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnGo1:
                pay();
                break;
            case R.id.ll_2048:
                startActivity(new Intent(getContext(), Game2048Activity.class));
                break;
            case R.id.ll_rec:
                break;
            case R.id.ll_more:
                Tools.showShortToast("管理员正在开发中");
//                startActivity(new Intent(getContext(), ShootVideoActivity.class));
                break;
            case R.id.ll_browser:
                startActivity(new Intent(getContext(), BrowserActivity.class));
                break;
        }
    }

    private void pay() {
        String videoUrl = editUrl1.getText().toString().trim();
        if (TextUtils.isEmpty(videoUrl)) {
            Tools.showShortToast("请输入播放地址。。。");
            return;
        }

        if (TbsVideo.canUseTbsPlayer(getContext())) {
            Tools.showShortToast("加载中。。。");
            TbsVideo.openVideo(getContext(), videoUrl);
        } else {
            Tools.showShortToast("暂不支持，请重试！");
        }
//        public static boolean canUseTbsPlayer(Context context)
//判断当前Tbs播放器是否已经可以使用。
//        public static void openVideo(Context context, String videoUrl)
//直接调用播放接口，传入视频流的url
//        public static void openVideo(Context context, String videoUrl, Bundle extraData)
//extraData对象是根据定制需要传入约定的信息，没有需要可以传如null
//extraData可以传入key: "screenMode", 值: 102, 来控制默认的播放UI
//类似: extraData.putInt("screenMode", 102); 来实现默认全屏+控制栏等UI
    }

    /**
     * rec 控制
     * @param view
     */
    @OnClick({R.id.tv_start, R.id.tv_pause, R.id.tv_resume,R.id.tv_load_video, R.id.tv_stop})
    public void onRECClick(View view) {
        switch (view.getId()) {
            case R.id.tv_start:
               if (recStatus.equals(STOP)){
                   ShareRECUtils.startREC(Recorder.LevelVideoQuality.LEVEL_SUPER_HIGH, Recorder.LevelMaxFrameSize.LEVEL_1280_768);
                   recStatus = START;
                   Tools.showShortToast("已开始！");
                   startTimer();
               }else {
                   recOpration();
               }
                break;
            case R.id.tv_pause:
                if (recStatus.equals(START)||recStatus.equals(RESUME)){
                    ShareRECUtils.pauseREC();
                    recStatus = PAUSE;
                    Tools.showShortToast("已暂停！");
                }else{
                    recOpration();
                }
                break;
            case R.id.tv_resume:
                if (recStatus.equals(PAUSE)){
                    ShareRECUtils.resumeREC();
                    recStatus = RESUME;
                    Tools.showShortToast("已继续！");
                }else{
                    recOpration();
                }
                break;
            case R.id.tv_load_video:
                ShareRECUtils.showProfileREC();
                break;
                case R.id.tv_stop:
                if (recStatus.equals(START)||recStatus.equals(PAUSE)||recStatus.equals(RESUME)){
                    ShareRECUtils.stopREC();
                    recStatus = STOP;
                    Tools.showShortToast("已停止！");
                }else{
                    recOpration();
                }
                break;
        }
    }

    private void recOpration(){
        if (recStatus.equals(STOP)){
            Tools.showShortToast("已停止！");
        }else if (recStatus.equals(START)){
            Tools.showShortToast("已开始！");
        }else if (recStatus.equals(PAUSE)){
            Tools.showShortToast("已暂停！");
        }else if (recStatus.equals(RESUME)){
            Tools.showShortToast("已继续！");
        }
    }

}
