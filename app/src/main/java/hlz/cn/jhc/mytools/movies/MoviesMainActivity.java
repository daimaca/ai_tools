package hlz.cn.jhc.mytools.movies;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.meis.widget.ball.BounceBallView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.comment.LabelType;
import hlz.cn.jhc.mytools.db.DBDaoUtils;
import hlz.cn.jhc.mytools.db.SharedPreferencesConst;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.eventbus.EventBusUtils;
import hlz.cn.jhc.mytools.greendao.TagTable;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;
import hlz.cn.jhc.mytools.movies.web.BrowserActivity;
import hlz.cn.jhc.mytools.movies.web.X5WebActivity;
import hlz.cn.jhc.mytools.utils.Consts;
import hlz.cn.jhc.mytools.utils.Tools;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class MoviesMainActivity extends BaseActivity {

    private static final int RC_CAMERA_AND_LOCATION = 1;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.rlv)
    RecyclerView rlv;
    @BindView(R.id.mSwitch)
    Switch mSwitch;
    @BindView(R.id.mBounceBallView)
    BounceBallView mBounceBallView;
    @BindView(R.id.floatingActionButton)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.noDataView)
    LinearLayout noDataView;
    @BindView(R.id.tv_noData)
    TextView tv_noData;
    @BindView(R.id.ll_SearchView)
    LinearLayout ll_SearchView;
    @BindView(R.id.et_movies)
    EditText et_movies;

    private List<WatchCollectionTable> list = new ArrayList<>();
    private MoviceAdapter moviceAdapter;
    private DBDaoUtils dbDaoUtils;
    private TagTable tagTable;

    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_movies_main);
        ButterKnife.bind(this);
        setStatusBar();
        dbDaoUtils = new DBDaoUtils(this);
        EventBusUtils.register(this);

    }

    public void isShowNoDataView(){
        if (list==null||list.size()==0){
            noDataView.setVisibility(View.VISIBLE);
            tv_noData.setText("暂时你还未添加，请点击右下角添加自己所爱吧！");
        }else {
            noDataView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBusUtils.unregister(this);
        if (mBounceBallView!=null){
            mBounceBallView.cancel();
            mBounceBallView = null;
        }
    }

    @SuppressLint("RestrictedApi")
    @Subscribe(threadMode = ThreadMode.MAIN,sticky = true)
    public void getData(TagTable bean) {
        tagTable = bean;
        initViewData(tagTable);
        EventBusUtils.removePostSticky(bean);
    }

    @SuppressLint("RestrictedApi")
    private void initViewData(TagTable tagTable){
        if (tagTable != null) {

            if (tagTable.get_id() == null || tagTable.get_id() == 0) {
                floatingActionButton.setVisibility(View.GONE);
            }else {
                floatingActionButton.setVisibility(View.VISIBLE);
            }
            //设置标题
            tvTitle.setText(tagTable.getName());
            setList(list, tagTable);
            /**
             * todo 初始化数据
             */
            LinearLayoutManager manager = new LinearLayoutManager(this);
            moviceAdapter = new MoviceAdapter(this, list,dbDaoUtils,tagTable);
            rlv.setLayoutManager(manager);
            rlv.setAdapter(moviceAdapter);
            Tools.setListAnimation(this, rlv);

            if (tagTable.getName().equals("地图导航")){
                methodRequiresTwoPermission();
            }
        } else {
            floatingActionButton.setVisibility(View.GONE);
        }
        isShowNoDataView();
    }


    @AfterPermissionGranted(1)
    private void methodRequiresTwoPermission() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "请允许此权限，否则可能影响应用的使用",
                    RC_CAMERA_AND_LOCATION, perms);
        }
    }

    @Override
    protected void initData() {

        back.setVisibility(View.VISIBLE);
        mSwitch.setVisibility(View.VISIBLE);
        et_movies.clearFocus();

        /**
         * todo 设置是否是电脑版版加载web
         */
        boolean isComputer = Tools.isWebComputer();
        if (isComputer) {
            mSwitch.setChecked(true);
        } else {
            mSwitch.setChecked(false);
        }
        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferencesUtils.setKeyValue(SharedPreferencesConst.isComputerKey, b);
            }
        });

        initBallView();
        search();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initViewData(tagTable);
    }

    private void initBallView() {
//        mBounceBallView.config()
//                .ballCount(5)
//                .bounceCount(3)
//                .ballDelay(220)
//                .duration(3300)
//                .radius(20)
//                .isPhysicMode(true)
//                .isRamdomPath(true)
//                .isRandomColor(true)
//                .isRandomRadius(true)
//                .apply();
        if (Tools.isOpenBall()){
            mBounceBallView.start();
        }
    }

    @OnClick({R.id.back,R.id.floatingActionButton})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.floatingActionButton:
                addWatchs();
                break;
        }

    }

    private void search() {
        et_movies.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                list.clear();
                setList(list,tagTable);
                if (!TextUtils.isEmpty(s)){
                    Iterator<WatchCollectionTable> it = list.iterator();
                    while (it.hasNext()) {
                        WatchCollectionTable dic = it.next();
                        if (!dic.getTitle().contains(s)) {
                            it.remove();
                        }
                    }
                    moviceAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        String movice = Tools.getText(et_movies.getText().toString().trim());

    }

    private void addWatchs() {
        final WatchConllectionDialog watchConllectionDialog = new WatchConllectionDialog(this);
        watchConllectionDialog.show();
        // 解决无法弹出软键盘
        watchConllectionDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        watchConllectionDialog.setOnSureClickListener(new WatchConllectionDialog.OnSureClickListener() {
            @Override
            public void sureClick(String title, String url) {
                Tools.showShortToast(tagTable.get_id()+"");
                WatchCollectionTable table = new WatchCollectionTable();
                table.setImg(0);
                table.setIsOwnDefined("2");
                table.setTagId(tagTable.get_id());
                table.setTitle(title);
                table.setUrl(url);
                boolean s = dbDaoUtils.insert(table);
                if (s){
                    list.add(table);
                    moviceAdapter.notifyDataSetChanged();
                    Tools.showShortToast("添加成功！");
                    watchConllectionDialog.dismiss();
                    isShowNoDataView();
                }
            }
        });
    }

    private void setList(List<WatchCollectionTable> list,TagTable bean){
        if (bean.getName().equals("视频集合")){
            list.addAll(DataMovices.getList());
        }else if (bean.getName().equals("直播")){
            list.addAll(DataLive.getList());
        }else if (bean.getName().equals("地图导航")){
            list.addAll(DataMapNavigation.getList());
        }else if (bean.getName().equals("购物")){
            list.addAll(DataShopping.getList());
        }else if (bean.getName().equals("学习")){
            list.addAll(DataLeaning.getList());
        }else if (bean.getName().equals("漫画")){
            list.addAll(DataComic.getList());
        }else if (bean.getName().equals("音乐")){
            list.addAll(DataMusic.getList());
        }else if (bean.getName().equals("小说")){
            list.addAll(DataNovel.getList());
        }else if (bean.getName().equals("在线工具")){
            list.addAll(DataOnlineTools.getList());
        }else {
            list.addAll(dbDaoUtils.getWatchCollectionList(bean.get_id()));
        }

    }
}
