package hlz.cn.jhc.mytools.mob;

import cn.sharerec.recorder.Recorder;
import cn.sharerec.recorder.impl.LibGDXRecorder;
import cn.sharerec.recorder.impl.SystemRecorder;
import hlz.cn.jhc.mytools.db.DaoManager;
import hlz.cn.jhc.mytools.home.xiaozhifragment.MediaOutputREC;
import hlz.cn.jhc.mytools.utils.Tools;

public class ShareRECUtils {


    private static volatile SystemRecorder recorder;

    private ShareRECUtils(){}
    public static SystemRecorder getSystemRecorder() {
        if (recorder == null) {
            synchronized (ShareRECUtils.class) {
                if (recorder == null) {
                    recorder = new SystemRecorder();
//                    recorder.setMediaOutput(MediaOutputREC.getInstance());

                    // 设置视频的最大尺寸，如1280x720
                    recorder.setMaxFrameSize(Recorder.LevelMaxFrameSize.LEVEL_1280_720);
                    // 设置视频的质量（如高、中、低）
                    recorder.setVideoQuality(Recorder.LevelVideoQuality.LEVEL_HIGH);
                    // 设置视频的最短时长（如5秒）
                    recorder.setMinDuration(1 * 1000);
                    // 设置视频的输出路径
                    String path = DaoManager.getBasePath() + "/aixiaozhi/录屏文件";
                    recorder.setCacheFolder(path);
                }
            }
        }
        return recorder;
    }

    /**
     *  开始录制
     * @param quality
     */
    public static void startREC(Recorder.LevelVideoQuality quality,Recorder.LevelMaxFrameSize size){
        // 启动录制前，先判断当前设备是否具备录屏功能，只有具备录屏功能的设备，才能启动录屏程序
        if (getSystemRecorder().isAvailable()) {
            getSystemRecorder().start();
        }else {
            Tools.showShortToast("你的设备不支持！");
        }
    }

    // 暂停录制
    public static void pauseREC(){
        getSystemRecorder().pause();
    }
    // 从暂停中恢复继续录制
    public static void resumeREC(){
        getSystemRecorder().resume();
    }
    // 停止录制
    public static void stopREC(){
        getSystemRecorder().stop();
    }
    // 进入视屏分享页面
    public static void showShareREC(){
        getSystemRecorder().showShare();
    }
    // 进入个人资料页面
    public static void showProfileREC(){
        getSystemRecorder().showProfile();
    }
    // 进入应用视频列表页面
    public static void showVideoCenterREC(){
        getSystemRecorder().showVideoCenter();
    }
}
