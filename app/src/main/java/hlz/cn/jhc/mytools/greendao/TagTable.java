package hlz.cn.jhc.mytools.greendao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity(indexes = {
        @Index(value = "name", unique = true)
})
public class TagTable {
    @Id(autoincrement = true)
    private Long _id;
    @NotNull
    private String name;
    private String isOwnDefined;// 1 系统 2 自定义
    private String imgPath;
    private int img;
@Generated(hash = 1503936010)
public TagTable(Long _id, @NotNull String name, String isOwnDefined,
        String imgPath, int img) {
    this._id = _id;
    this.name = name;
    this.isOwnDefined = isOwnDefined;
    this.imgPath = imgPath;
    this.img = img;
}
@Generated(hash = 1306394045)
public TagTable() {
}
public Long get_id() {
    return this._id;
}
public void set_id(Long _id) {
    this._id = _id;
}
public String getName() {
    return this.name;
}
public void setName(String name) {
    this.name = name;
}
public String getIsOwnDefined() {
    return this.isOwnDefined;
}
public void setIsOwnDefined(String isOwnDefined) {
    this.isOwnDefined = isOwnDefined;
}
public String getImgPath() {
    return this.imgPath;
}
public void setImgPath(String imgPath) {
    this.imgPath = imgPath;
}
public int getImg() {
    return this.img;
}
public void setImg(int img) {
    this.img = img;
}
}
