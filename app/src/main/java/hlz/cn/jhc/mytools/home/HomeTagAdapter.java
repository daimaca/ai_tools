package hlz.cn.jhc.mytools.home;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.zhy.view.flowlayout.FlowLayout;

import java.util.List;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.db.DBDaoUtils;
import hlz.cn.jhc.mytools.eventbus.EventBusUtils;
import hlz.cn.jhc.mytools.greendao.TagTable;
import hlz.cn.jhc.mytools.greendao.WatchCollectionTable;
import hlz.cn.jhc.mytools.movies.MoviesMainActivity;
import hlz.cn.jhc.mytools.utils.Tools;

/**
 * Created by Administrator on 2018/10/28.
 */

public class HomeTagAdapter extends com.zhy.view.flowlayout.TagAdapter<TagTable> {

    private Context mContext;
    private List<TagTable> data;
    private DBDaoUtils dbDaoUtils;

    public HomeTagAdapter(List<TagTable> data, Context context, DBDaoUtils dbDaoUtils) {
        super(data);
        this.mContext = context;
        this.data = data;
        this.dbDaoUtils = dbDaoUtils;
    }

    @Override
    public View getView(FlowLayout parent, final int position, final TagTable bean) {
        View view =  LayoutInflater.from(mContext).inflate(R.layout.home_tag_item, parent, false);
        TextView tv_tag = view.findViewById(R.id.tv_tag);
        tv_tag.setText(bean.getName());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBusUtils.postStickyObject(data.get(position));
                mContext.startActivity(new Intent(mContext, MoviesMainActivity.class));
            }
        });
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (bean.getIsOwnDefined().equals("2")){
                    final SimpleDialog simpleDialog = new SimpleDialog(mContext,"是否删除该标签？");
                    simpleDialog.show();
                    simpleDialog.setOnSureClickListener(new SimpleDialog.OnSureClickListener() {
                        @Override
                        public void sureClick() {
                            boolean s = dbDaoUtils.delete(bean);
                            if (s){
                                data.remove(position);
                                notifyDataChanged();
                                Tools.showShortToast("删除成功！");
                                List<WatchCollectionTable> tables = dbDaoUtils.getWatchCollectionList(bean.get_id());
                                for (int i = 0; i <tables.size() ; i++) {
                                    dbDaoUtils.delete(tables.get(i));
                                }
                                simpleDialog.dismiss();
                            }

                        }
                    });
                }else {
                    Tools.showShortToast("无法删除系统提供的标签！");
                }
                return true;
            }
        });

        return view;
    }
}
