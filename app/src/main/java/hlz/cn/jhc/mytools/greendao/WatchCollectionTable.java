package hlz.cn.jhc.mytools.greendao;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class WatchCollectionTable implements Parcelable {

    @Id(autoincrement = true)
    private Long _id;
    private long tagId;
    @NotNull
    private String title;
    @NotNull
    private String url;
    private String imgPath;
    private int img;
    private String type;
    private String isOwnDefined;// 1 系统 2 自定义
    private String label;
    @Generated(hash = 1842907026)
    public WatchCollectionTable(Long _id, long tagId, @NotNull String title,
            @NotNull String url, String imgPath, int img, String type,
            String isOwnDefined, String label) {
        this._id = _id;
        this.tagId = tagId;
        this.title = title;
        this.url = url;
        this.imgPath = imgPath;
        this.img = img;
        this.type = type;
        this.isOwnDefined = isOwnDefined;
        this.label = label;
    }
    @Generated(hash = 548135761)
    public WatchCollectionTable() {
    }

    protected WatchCollectionTable(Parcel in) {
        if (in.readByte() == 0) {
            _id = null;
        } else {
            _id = in.readLong();
        }
        tagId = in.readLong();
        title = in.readString();
        url = in.readString();
        imgPath = in.readString();
        img = in.readInt();
        type = in.readString();
        isOwnDefined = in.readString();
        label = in.readString();
    }

    public static final Creator<WatchCollectionTable> CREATOR = new Creator<WatchCollectionTable>() {
        @Override
        public WatchCollectionTable createFromParcel(Parcel in) {
            return new WatchCollectionTable(in);
        }

        @Override
        public WatchCollectionTable[] newArray(int size) {
            return new WatchCollectionTable[size];
        }
    };

    public Long get_id() {
        return this._id;
    }
    public void set_id(Long _id) {
        this._id = _id;
    }
    public long getTagId() {
        return this.tagId;
    }
    public void setTagId(long tagId) {
        this.tagId = tagId;
    }
    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getUrl() {
        return this.url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getImgPath() {
        return this.imgPath;
    }
    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }
    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getIsOwnDefined() {
        return this.isOwnDefined;
    }
    public void setIsOwnDefined(String isOwnDefined) {
        this.isOwnDefined = isOwnDefined;
    }
    public String getLabel() {
        return this.label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public int getImg() {
        return this.img;
    }
    public void setImg(int img) {
        this.img = img;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (_id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(_id);
        }
        parcel.writeLong(tagId);
        parcel.writeString(title);
        parcel.writeString(url);
        parcel.writeString(imgPath);
        parcel.writeInt(img);
        parcel.writeString(type);
        parcel.writeString(isOwnDefined);
        parcel.writeString(label);
    }
}
