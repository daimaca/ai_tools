package hlz.cn.jhc.mytools.home;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.View;


public class HomeScrollView extends NestedScrollView {
    private TranslucentListener mTranslucentListener;

    public HomeScrollView(Context context) {
        super(context);
    }

    public HomeScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HomeScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public void onNestedScroll(View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        super.onNestedScroll(target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (mTranslucentListener != null) {
            // alpha = 滑出去的高度/(screenHeight/3);
            float heightPixels = getContext().getResources().getDisplayMetrics().heightPixels;
            float scrollY = getScrollY();//该值 大于0
            float alpha = scrollY / (heightPixels / 5);// 0~1  透明度是1~0
            // 这里选择的screenHeight的1/3 是alpha改变的速率 （根据你的需要你可以自己定义）
            mTranslucentListener.onTranslucent(alpha);


        }
    }

    public void setOnScrollChangedListener(TranslucentListener listener) {
        mTranslucentListener = listener;
    }

    public interface TranslucentListener {
        /**
         * 透明度的回调
         * @param alpha
         */
        void  onTranslucent(float alpha);
    }

}