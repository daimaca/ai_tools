package hlz.cn.jhc.mytools.home;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.db.SharedPreferencesConst;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.movies.web.X5WebView;
import hlz.cn.jhc.mytools.utils.Tools;

public class ThemeWebViewActivity extends BaseActivity {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.mTv)
    TextView mTv;
    @BindView(R.id.mRelativeLayout)
    RelativeLayout mRelativeLayout;
    @BindView(R.id.iv_last)
    ImageView ivLast;
    @BindView(R.id.iv_next)
    ImageView ivNext;
    @BindView(R.id.btn_bottom)
    Button btnBottom;
    private X5WebView x5WebView;
    private int theme = 1;

    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_theme_webview);
        ButterKnife.bind(this);
    }

    @Override
    protected void initData() {
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText("设置主题");
        btnBottom.setText("保持该主题");
        mTv.setVisibility(View.VISIBLE);
        mTv.setText("恢复默认主题");
        addWeb();
    }

    @OnClick({R.id.back, R.id.iv_last, R.id.iv_next, R.id.btn_bottom, R.id.mTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.iv_last:
                if (theme <= 1){
                    theme = 1;
                    Tools.setTheme(x5WebView, theme);
                }else {
                    theme--;
                    Tools.setTheme(x5WebView, theme);
                }
                break;
            case R.id.iv_next:
                if (theme >= 14){
                    theme = 14;
                    Tools.setTheme(x5WebView, theme);
                }else {
                    if (theme==0){
                        theme = 1;
                    }
                    theme++;
                    Tools.setTheme(x5WebView,theme);
                }
                break;
            case R.id.btn_bottom:
                SharedPreferencesUtils.setKeyValue(this,SharedPreferencesConst.ThemePositionKey,theme);
                Tools.showShortToast("设置成功！");
                finish();
                break;
            case R.id.mTv:
                SharedPreferencesUtils.setKeyValue(this,SharedPreferencesConst.ThemePositionKey,0);
                Tools.showShortToast("设置成功！");
                finish();
                break;
        }
    }

    private void addWeb(){
        x5WebView = new X5WebView(this, null);
        mRelativeLayout.addView(x5WebView, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));
        theme = SharedPreferencesUtils.getKeyValue(this,SharedPreferencesConst.ThemePositionKey,0);
        if (theme == 0){
            theme = 1;
        }
        Tools.setTheme(x5WebView,theme);
//        String url = "file:///android_asset/theme01/index.html";
//        x5WebView.loadUrl(url);
    }

    private void removeWeb(){
        if (mRelativeLayout!=null){
            mRelativeLayout.removeAllViews();
            mRelativeLayout = null;
        }
        if (x5WebView!=null){
            x5WebView.destroy();
            x5WebView = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeWeb();
    }
}
