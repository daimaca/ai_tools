package hlz.cn.jhc.mytools.login;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.db.SharedPreferencesConst;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.utils.Tools;

public class LoginActivity extends BaseActivity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.btn_sure)
    Button btnSure;

    public static final String defaultName = "hou";
    public static final String defaultPassword = "hou19950621lizhi";


    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @Override
    protected void initData() {
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText("管理员登录");
    }

    @OnClick({R.id.back, R.id.btn_sure})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.btn_sure:
                submit();
                break;
        }
    }

    private void submit() {
        String name = etName.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (TextUtils.isEmpty(name)){
            Tools.showShortToast("请输入你的账号");
            return;
        }else if (TextUtils.isEmpty(password)){
            Tools.showShortToast("请输入你的密码");
            return;
        }else if (name.equals(defaultName)&&password.equals(defaultPassword)){
            SharedPreferencesUtils.setKeyValue(this, SharedPreferencesConst.nameKey,name);
            SharedPreferencesUtils.setKeyValue(this, SharedPreferencesConst.passwordKey,password);
            Tools.showShortToast("已成功开启管理员模式！");
        }else {
            SharedPreferencesUtils.setKeyValue(this, SharedPreferencesConst.nameKey,name);
            SharedPreferencesUtils.setKeyValue(this, SharedPreferencesConst.passwordKey,password);
            Tools.showShortToast("你并非管理员，已为你开启普通模式！");
        }
        finish();
    }
}
