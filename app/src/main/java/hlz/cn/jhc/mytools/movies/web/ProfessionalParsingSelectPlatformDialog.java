package hlz.cn.jhc.mytools.movies.web;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.db.SharedPreferencesConst;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.movies.CommentSelectPlatformBean;
import hlz.cn.jhc.mytools.utils.Tools;

/**
 * Created by Administrator on 2018/10/21.
 */

public class ProfessionalParsingSelectPlatformDialog extends AlertDialog {
    private Context context;
    private TextView tv_select_title;
    private TextView tv_cancel;
    private TextView tv_sure;
    private RecyclerView rlv_list;
    private int platformPosition;
    private SelectPlatformAdapter selectPlatformAdapter;

    public ProfessionalParsingSelectPlatformDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_platform_dialog);
        // 去掉四个黑角
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        initView();
        initData();
        setListener();
    }

    private void setListener() {
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectOnclick != null) {
                }
                mSelectOnclick.onCancel();
            }
        });
        tv_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectOnclick != null) {
                    mSelectOnclick.onSure(DataVip.getListData(platformPosition).get(platformPosition));
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        DisplayMetrics metrics = new DisplayMetrics();
        getWindow().getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        getWindow().getAttributes().width = (int) (width * 0.8);
        getWindow().getAttributes().height = (int) (height * 0.8);
    }

    private void initData() {
        selectPlatformAdapter = new SelectPlatformAdapter(context, DataVip.getListData(platformPosition));
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        rlv_list.setLayoutManager(layoutManager);
        rlv_list.setAdapter(selectPlatformAdapter);
        Tools.setListAnimation(context,rlv_list);
    }



    private void initView() {
        tv_select_title = findViewById(R.id.tv_select_title);
        tv_cancel = findViewById(R.id.tv_cancel);
        tv_sure = findViewById(R.id.tv_sure);
        rlv_list = findViewById(R.id.rlv_list);
        platformPosition = SharedPreferencesUtils.getKeyValue(context, SharedPreferencesConst.SelectPlatformPositionKey, 0);
        tv_select_title.setText("当前平台：" + Tools.getText(DataVip.names[platformPosition]));
    }

    private SelectOnclick mSelectOnclick;

    public void setOnSelectOnclick(SelectOnclick mSelectOnclick) {
        this.mSelectOnclick = mSelectOnclick;
    }

    public interface SelectOnclick {
        void onSure(CommentSelectPlatformBean bean);

        void onCancel();
    }

    class SelectPlatformAdapter extends RecyclerView.Adapter<SelectPlatformAdapter.MyViewHolder> {

        private Context context;
        private List<CommentSelectPlatformBean> list_select;

        public SelectPlatformAdapter(Context context, List<CommentSelectPlatformBean> list) {
            this.context = context;
            this.list_select = list;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.select_platform_dialog_item, null);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            CommentSelectPlatformBean model = list_select.get(position);
            if (model != null) {
                holder.tvTitle.setText(model.getTitle());
                if (model.getImg()==0){
                    Glide.with(context).load("").apply(Tools.getNoImageRequestOptions()).into(holder.ivImg);
                }else {
                    Glide.with(context).load(model.getImg()).apply(Tools.getNoImageRequestOptions()).into(holder.ivImg);
                }
//                Glide.with(context).load(model.getImg()).error(R.mipmap.no_img).into(holder.ivImg);
                if (DataVip.getListData(platformPosition).get(position).getIsSelect().equals("1")) {
                    holder.tvTitle.setTextColor(context.getResources().getColor(R.color.green_light));
                } else {
                    holder.tvTitle.setTextColor(context.getResources().getColor(R.color._333333));
                }
            }
        }

        @Override
        public int getItemCount() {
            if (DataVip.getListData(platformPosition) == null || DataVip.getListData(platformPosition).size() == 0) return 0;
            return DataVip.getListData(platformPosition).size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.iv_img)
            ImageView ivImg;
            @BindView(R.id.tv_title)
            TextView tvTitle;

            public MyViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        platformPosition = getAdapterPosition();
                        DataVip.getListData(platformPosition);
                        SharedPreferencesUtils.setKeyValue(context, SharedPreferencesConst.SelectPlatformPositionKey, platformPosition);
                        notifyDataSetChanged();
                        tv_select_title.setText("当前平台：" + Tools.getText(DataVip.names[platformPosition]));

                    }
                });
            }
        }
    }
}
