package hlz.cn.jhc.mytools.settings;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.share.ShareUtils;
import hlz.cn.jhc.mytools.utils.Tools;

/**
 * 打赏
 */
public class PlayRewardActivity extends BaseActivity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_wechat_download)
    ImageView ivWechatDownload;
    @BindView(R.id.iv_app_download)
    ImageView ivAppDownload;
    @BindView(R.id.btn_bottom)
    Button btnBottom;
    @BindView(R.id.tv_head_tip_message)
    TextView tvHeadTipMessage;

    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_play_reward);
        ButterKnife.bind(this);
    }

    @Override
    protected void initData() {
        tvHeadTipMessage.setText("小提示：点击图片可以分享哈！");
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText("打赏");
    }

    @OnClick({R.id.back, R.id.iv_wechat_download, R.id.iv_app_download, R.id.btn_bottom})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.iv_wechat_download:
                ShareUtils.shareImage(this, R.mipmap.wechat_download);
                break;
            case R.id.iv_app_download:
                ShareUtils.shareImage(this, R.mipmap.app_download);
                break;
            case R.id.btn_bottom:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        submit();
                    }
                }).start();
                Tools.showShortToast("已保存，请到相册查看！");
                break;
        }
    }

    private void submit() {
        try {
            Bitmap bitmap = Glide.with(this).asBitmap().load(R.mipmap.wechat_play_reward).submit().get();
            Bitmap bitmap2 = Glide.with(this).asBitmap().load(R.mipmap.alipay_play_reward).submit().get();
            ShareUtils.getImageUri(this, bitmap);
            ShareUtils.getImageUri(this, bitmap2);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
