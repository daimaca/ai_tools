package hlz.cn.jhc.mytools.base;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.StyleRes;
import android.util.DisplayMetrics;
import android.view.View;

/**
 * Created by Administrator on 2018/6/12.
 */

public abstract class BaseDialog extends AlertDialog implements View.OnClickListener {

    public float rate5 = 0.5f;
    public float rate6 = 0.6f;
    public float rate7 = 0.7f;
    public float rate8 = 0.8f;
    public float rate9 = 0.9f;

    protected BaseDialog(Context context) {
        super(context);
    }

    protected BaseDialog(Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setLayout());
        initView();
        initData();
    }


    @Override
    protected void onStart() {
        super.onStart();
        /**
         * getRealMetrics - 屏幕的原始尺寸，即包含状态栏。
         * version >= 4.2.2
         */
        DisplayMetrics metrics = new DisplayMetrics();
        getWindow().getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        getWindow().getAttributes().width = width * setWidth();
        getWindow().getAttributes().height = (int) (height * rate5);
    }

    protected abstract int setWidth();

    protected abstract int setLayout();

    protected abstract void initData();

    protected abstract void initView();

    public <view extends View> view getViewId(int resId){
        return (view) findViewById(resId);
    }

    @Override
    public void onClick(View view) {

    }
}
