package hlz.cn.jhc.mytools.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.github.glomadrian.grav.GravView;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.application.MyApplication;
import hlz.cn.jhc.mytools.db.SharedPreferencesConst;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.login.LoginActivity;
import hlz.cn.jhc.mytools.movies.web.LoadResourceAdapter;
import hlz.cn.jhc.mytools.movies.web.X5WebView;

/**
 * Created by Administrator on 2018/6/12.
 */

public class Tools {

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private static String errCode;
    private static Toast mToast;

    /**
     * todo  Toast 方法
     * @param text
     */
    public static void showShortToast(String text){
        TextView view = new TextView(MyApplication.mContext);
        view.setText(text);
        view.setTextColor(MyApplication.mContext.getResources().getColor(R.color.white));
        view.setBackgroundDrawable(MyApplication.mContext.getResources().getDrawable(R.drawable.toast_bg));
        if (mToast == null){
            mToast = new Toast(MyApplication.mContext);
        }
        mToast.setView(view);
        mToast.show();
    }
    public static void showOwnShortToast(String text,View view){
//        TextView view1 = new TextView(MyApplication.mContext);
//        view1.setText(text);
//        view1.setTextColor(MyApplication.mContext.getResources().getColor(R.color.theme_01));
//        view1.setBackgroundDrawable(MyApplication.mContext.getResources().getDrawable(R.drawable.toast_bg));
        if (mToast == null){
            mToast = new Toast(MyApplication.mContext);
        }
        mToast.setView(view);
        mToast.show();
//        Toast.makeText(MyApplication.mContext,text,Toast.LENGTH_SHORT).show();
    }
    public static void showLongToast(String text){
        Toast.makeText(MyApplication.mContext,text,Toast.LENGTH_LONG).show();
    }

    /**
     *  todo HashMap 排序
     * @param map
     * @return
     */
    public static List<MyMap> sortMapKey(HashMap<String,String> map){

        List<MyMap> list = new ArrayList<>();
//        Map<String,String> map1 = new HashMap<>();
        for (String key : map.keySet()){
            Log.i("sort_test", "sortMapKey  22: "+key);
            list.add(new MyMap(key,map.get(key)));
        }

        Collections.sort(list, new Comparator<MyMap>() {
            @Override
            public int compare(MyMap o1, MyMap o2) {
                return o1.key.compareTo(o2.key);
            }
        });

        for (int i = 0; i < list.size(); i++) {
            Log.i("sort_test", "sortMapKey: "+list.get(i).key);
        }
        return list;
    }

    /**
     *  todo 拼接 Sign
     * @param list
     * @return
     */
    public static String getSign(List<MyMap> list){
        StringBuffer sign =  new StringBuffer();
        for (int i = 0; i <list.size() ; i++) {
            sign.append(list.get(i).key);
            sign.append("=");
            sign.append(list.get(i).value);
            if (i<list.size()-1){
                sign.append("&");
            }

        }
        return sign.toString();
    }

    public static class MyMap{
        String key;
        String value;
        public MyMap(){}
        public MyMap(String key,String value){
            this.key = key;
            this.value = value;
        }
    }

    //todo 微信，支付宝，UMS 支付方式变量
    public static final String WXName = "com.tencent.mm";
    public static final String ZFBName = "com.eg.android.AlipayGphone";
    public static final String UMSName = "com.sinonet.chinaums";

    /**
     * TODO  检查 微信,支付宝是否安装
     * @param packageName
     * @return
     */
    public static boolean CheckInstall(final String packageName, Activity context){
        boolean checkResult = false;
        try {
            PackageInfo packageInfo = MyApplication.mContext.getPackageManager().getPackageInfo(packageName, 0);
            if (packageInfo == null) {
//                checkResult="未安装";
                checkResult = false;
            } else {
//                checkResult="已经安装";
                checkResult = true;
            }
        } catch (Exception e) {
//            checkResult="未安装";
            checkResult = false;
        }
        if (checkResult){

        }else {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (packageName.equals(WXName)){
                        showShortToast("你还未安装微信,请你先安装再试!");
                    }else if (packageName.equals(ZFBName)){
                        showShortToast("你还未安装支付宝,请你先安装再试!");
                    }else {
                        showShortToast("你还未安装全民付,请你先安装再试!");
                    }

                }
            });
        }
        return checkResult;
    }

    //todo 检测支付宝
    public static boolean checkAliPayInstalled(Context context) {

        Uri uri = Uri.parse("alipays://platformapi/startApp");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        ComponentName componentName = intent.resolveActivity(context.getPackageManager());
        return componentName != null;
    }

    /**
     * TODO 检查网络是否可用
     *
     * @param
     * @return
     */
    public static boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) MyApplication.mContext.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager == null) {
            return false;
        }
        NetworkInfo networkinfo = manager.getActiveNetworkInfo();
        if (networkinfo == null || !networkinfo.isAvailable()) {
            return false;
        }
        return true;
    }

    /**
     * todo 移除SearchView的下划线
     * @param sv_View
     */
    public static void removeSearchViewUnderLine(SearchView sv_View){
        Class<?> s = sv_View.getClass();
        try {
            Field field = s.getDeclaredField("mSearchPlate");
            field.setAccessible(true);
            View mView = (View) field.get(sv_View);
            //--设置背景
            mView.setBackgroundColor(Color.TRANSPARENT);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * todo 用来获取View的视图
     * @param context
     * @param layout
     * @return
     */
    public static View getView(Context context, int layout){
        View view = LayoutInflater.from(context).inflate(layout,null);
        return view;
    }
    //todo 用于Handler的Messge信息传递
    public static Message getHandlerWhatMessage(int what){
        Message message = new Message();
        message.what = what;
        return message;
    }
    //todo 用于Handler的Messge信息传递
    public static Message getHandlerObjectMessage(int what,Object obj){
        Message message = new Message();
        message.what = what;
        message.obj = obj;
        return message;
    }
    /*todo************************************用户信息*********************************************/



    /**
     * todo 注册广播
     * @param context
     * @param register
     * @param action
     */
    public static void registerReceiver(Activity context, BroadcastReceiver register, String action){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(action);
        context.registerReceiver(register,intentFilter);
    }
    /**
     * todo 注销广播
     * @param context
     * @param register
     */
    public static void unregisterReceiver(Activity context, BroadcastReceiver register){
        context.unregisterReceiver(register);
    }

    public static String getText(String text){
        if (TextUtils.isEmpty(text)){
            return "";
        }
        return text;
    }

    //todo 设置列表动画
    public static void setListAnimation(Context context,RecyclerView rlv) {
        if (rlv == null) return;
        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(context, resId);
        rlv.setLayoutAnimation(animation);
    }

    /**
     * 获得app的版本号
     *
     * @return
     */
    public static String getAppVersion() {
        String version = "";
        PackageManager manager = MyApplication.getInstance().getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(MyApplication.getInstance().getPackageName(), 0);
            version = info.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "未获取到版本号";
    }

    /**
     * 否是需要电脑版版加载web
     * @return
     */
    public static boolean isWebComputer(){
        boolean isComputer = SharedPreferencesUtils.getKeyValue(SharedPreferencesConst.isComputerKey, false);
        return isComputer;
    }
    /**
     * 否是需要电脑版版加载web
     * @return
     */
    public static boolean isOpenBall(){
        boolean isOpenBall = SharedPreferencesUtils.getKeyValue(SharedPreferencesConst.isOpenBallKey, false);
        return isOpenBall;
    }

    private static RequestOptions mNoImageRequestOptions;
    public static RequestOptions getNoImageRequestOptions(){
        if (mNoImageRequestOptions == null){
            mNoImageRequestOptions = new RequestOptions();
            mNoImageRequestOptions.error(R.mipmap.no_img);
        }
        return mNoImageRequestOptions;
    }

    public static int getHeightPixels() {
        WindowManager wm = (WindowManager) MyApplication.mContext.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

    public static int getWidthPixels() {
        WindowManager wm = (WindowManager) MyApplication.mContext.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);

        Log.i("getWidthPixels", "getWidthPixels: "+ dm.widthPixels);
        Log.i("getWidthPixels", "getWidthPixels: "+ dm.densityDpi);
        return dm.widthPixels;
    }

    //安卓中像素px和dp的转换：
    public static int Dp2Px(float dp) {
        final float scale = MyApplication.mContext.getResources().getDisplayMetrics().density; //当前屏幕密度因子
        return (int) (dp * scale + 0.5f);
    }

    public static int Px2Dp(float px) {
        final float scale = MyApplication.mContext.getResources().getDisplayMetrics().density;
        return (int) (px / scale + 0.5f);
    }

    /**
     *  判断是否是图片，如果是添加列表
     * @param text
     * @return
     */
    public static void addImage(String text){
        if (!Tools.isGetResource()){
            return;
        }
        if (TextUtils.isEmpty(text)){
            return;
        }

        LogUtils.Logi("addImage", "addImage:未处理 "+text);
        if (text.contains(".js") || text.contains(".html") || text.contains(".htm") || text.contains(".css") || text.contains(".php")|| text.endsWith("/")||text.length() > 250
                || text.contains("api")) {
            return;
        }

        if (text.contains("=")&&!(text.contains("jpg")|| text.contains("png")|| text.contains("gif")|| text.contains("webp")
                || text.contains("jpeg")|| text.contains("svg")|| text.contains("psd")|| text.contains("ico")|| text.contains("image"))){
            return;
        }
        LogUtils.Logi("getImage", "addImage: "+text);
        LoadResourceAdapter.getLoadResourceList().add(text);
//        if (text.endsWith(".gif")||text.endsWith(".jpg")||text.endsWith(".png")||text.endsWith(".webp")
//                ||text.endsWith(".jpeg")||text.endsWith(".svg")||text.endsWith(".psd")
//                ||text.endsWith(".webp")||text.endsWith(".ico")
//                ||text.endsWith("/0")//腾讯特殊
//
//                ) {
//            Log.i("addImage", "addImage: "+text);
//            LoadResourceAdapter.getLoadResourceList().add(text);
//        }
    }

    /**
     * 判断此url是否是广告
     * @param url
     * @return
     */
    public static boolean isAd(String url){
        if (!Tools.isShieldingAdvertising())return false;
        if (TextUtils.isEmpty(url))return false;
        //特殊情况 含gif但是不是广告
        if (url.contains("https://pic.china-gif.com"))return false;

        if (url.contains("https://jc.bubbleandyeson.com")
                ||url.contains("https://jc.4006825178.com")
                ||url.contains("gif")
                ||url.contains("https://img.nigu.net.cn")
                ||url.contains("https://pic.517wr.cn")
                ||url.contains("https://pic.kele12.cn")
        ){
            return true;
        }
        return false;
    }

    /**
     * 判断是否是管理员
     *
     * @return
     */
    public static boolean isAdmin() {
        String name = SharedPreferencesUtils.getKeyValue(MyApplication.mContext, SharedPreferencesConst.nameKey, "0");
        String password = SharedPreferencesUtils.getKeyValue(MyApplication.mContext, SharedPreferencesConst.passwordKey, "0");
        if (name.equals(LoginActivity.defaultName) && password.equals(LoginActivity.defaultPassword)) {
            return true;
        }
        return false;
    }

    /**
     * todo 程序是否在前台运行
     *
     * @return
     */
    public static boolean isAppOnForeground() {
        ActivityManager activityManager = (ActivityManager) MyApplication.mContext.getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        String packageName = MyApplication.mContext.getApplicationContext().getPackageName();
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager
                .getRunningAppProcesses();
        if (appProcesses == null)
            return false;
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.processName.equals(packageName)
                    && appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                return true;
            }
        }
        return false;
    }


    public static boolean isNotificationEnabled(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return isEnableV26(context);
        } else {
            return isEnableV19(context);
        }
    }

    /**
     * Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
     * 19及以上
     *
     * @param context
     * @return
     */
    private static final String CHECK_OP_NO_THROW = "checkOpNoThrow";
    private static final String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean isEnableV19(Context context) {
        AppOpsManager mAppOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        ApplicationInfo appInfo = context.getApplicationInfo();
        String pkg = context.getApplicationContext().getPackageName();
        int uid = appInfo.uid;

        Class appOpsClass;
        try {
            appOpsClass = Class.forName(AppOpsManager.class.getName());
            Method checkOpNoThrowMethod = appOpsClass.getMethod(CHECK_OP_NO_THROW, Integer.TYPE, Integer.TYPE,
                    String.class);
            Field opPostNotificationValue = appOpsClass.getDeclaredField(OP_POST_NOTIFICATION);

            int value = (Integer) opPostNotificationValue.get(Integer.class);
            return ((Integer) checkOpNoThrowMethod.invoke(mAppOps, value, uid, pkg) == AppOpsManager.MODE_ALLOWED);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
     * 针对8.0及以上设备
     *
     * @param context
     * @return
     */
    public static boolean isEnableV26(Context context) {
        try {
            NotificationManager notificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);
            Method sServiceField = notificationManager.getClass().getDeclaredMethod("getService");
            sServiceField.setAccessible(true);
            Object sService = sServiceField.invoke(notificationManager);

            ApplicationInfo appInfo = context.getApplicationInfo();
            String pkg = context.getApplicationContext().getPackageName();
            int uid = appInfo.uid;

            Method method = sService.getClass().getDeclaredMethod("areNotificationsEnabledForPackage"
                    , String.class, Integer.TYPE);
            method.setAccessible(true);
            return (boolean) method.invoke(sService, pkg, uid);
        } catch (Exception e) {
        }
        return false;
    }

    private static final int REQUEST_SETTING_NOTIFICATION = 1;
    public static void gotoNotificationSetting(Activity activity) {
        ApplicationInfo appInfo = activity.getApplicationInfo();
        String pkg = activity.getApplicationContext().getPackageName();
        int uid = appInfo.uid;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
                //这种方案适用于 API 26, 即8.0（含8.0）以上可以用
                intent.putExtra(Settings.EXTRA_APP_PACKAGE, pkg);
                intent.putExtra(Settings.EXTRA_CHANNEL_ID, uid);
                //这种方案适用于 API21——25，即 5.0——7.1 之间的版本可以使用
                intent.putExtra("app_package", pkg);
                intent.putExtra("app_uid", uid);
                activity.startActivityForResult(intent, REQUEST_SETTING_NOTIFICATION);
            } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.setData(Uri.parse("package:" + activity.getPackageName()));
                activity.startActivityForResult(intent, REQUEST_SETTING_NOTIFICATION);
            } else {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                activity.startActivityForResult(intent, REQUEST_SETTING_NOTIFICATION);
            }
        } catch (Exception e) {
            Intent intent = new Intent(Settings.ACTION_SETTINGS);
            activity.startActivityForResult(intent, REQUEST_SETTING_NOTIFICATION);
        }
    }


    /**
     * 获取最新版本号的消息
     * @param
     * @return
     */
    public static String getLastVersionName(){
        return SharedPreferencesUtils.getKeyValue(MyApplication.mContext, SharedPreferencesConst.JPushVersionNameKey,"点击获取最新版本");
    }

    /**
     *  是否去广告
     */
    public static boolean isShieldingAdvertising(){//默认去广告
        boolean isShieldingAdvertising = SharedPreferencesUtils.getKeyValue(SharedPreferencesConst.isShieldingAdvertisingKey, true);
        return isShieldingAdvertising;
    }
    /**
     *  是否开启泡泡
     */
    private static GravView mGravView;
    public static void isOpenPaoPao(ConstraintLayout layout){
        if (layout == null){
            return;
        }
        if (mGravView == null){
            mGravView = (GravView) LayoutInflater.from(MyApplication.mContext).inflate(R.layout.grav_view_layout,null);
        }

        layout.removeAllViews();
        boolean isOpenPaoPao = SharedPreferencesUtils.getKeyValue(SharedPreferencesConst.isOpenpaopaoKey, true);
        if (isOpenPaoPao){
            layout.setVisibility(View.VISIBLE);
            layout.addView(mGravView);
        }else {
            layout.setVisibility(View.GONE);
        }
    }public static boolean isOpenPaoPao(){
        return SharedPreferencesUtils.getKeyValue(SharedPreferencesConst.isOpenpaopaoKey, true);
    }

    static int p = 1;
    public static void setTheme(X5WebView webView,int p){
        //
        String theme = "theme01";
        if (p<10){
            theme = "theme0"+String.valueOf(p);
        }else {
            theme = "theme"+String.valueOf(p);
        }
        String url = "file:///android_asset/"+theme+"/index.html";
        webView.loadUrl(url);
    }

    /**
     * 否是开启提取资源
     * @return
     */
    public static boolean isGetResource(){
        boolean isGetResourceKey = SharedPreferencesUtils.getKeyValue(SharedPreferencesConst.isGetResourceKey, false);
        return isGetResourceKey;
    }
}
