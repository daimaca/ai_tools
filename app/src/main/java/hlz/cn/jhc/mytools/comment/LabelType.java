package hlz.cn.jhc.mytools.comment;

public interface LabelType {

    String labelSystem = "系统";

    /**
     *  type 类型
     */
    String labelMovices = "labelMovices";// 视频集合
    String labelLive = "labelLive";//直播
    String labelMapNavigation = "labelMapNavigation";//地图导航
    String labelShopping = "labelShopping";//购物
    String labelLeaning = "labelLeaning";//学习
    String labelComic = "labelComic";//漫画
    String labelMusic = "labelMusic";//音乐
    String labelNovel = "labelNovel";//小说
    String labelOnlineTools = "labelOnlineTools";//在线工具
    String labelOwnDefinedTag = "labelOwnDefinedTag";//自定义Tag


    //          标签

    /**
     * todo 小说
     */
    String labelRead = "阅读";
    String labelOnlineListen = "在线听";

    /**
     * todo 电影
     */
    String LABEL_OFFICIAL = "官方";
    String LABEL_RECOMMEND = "推荐";
    String LABEL_FLUENCY = "流畅";
}
