package hlz.cn.jhc.mytools.comment;

import android.view.View;
import android.widget.PopupWindow;

public class AddPopWindow extends PopupWindow {

    private int x = 0;
    private int y = 20;

    @Override
    public void setContentView(View contentView) {
        super.setContentView(contentView);
    }


    @Override
    public void showAsDropDown(View anchor) {
        super.showAsDropDown(anchor, x, y);
    }
}
