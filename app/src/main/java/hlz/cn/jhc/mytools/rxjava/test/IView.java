package hlz.cn.jhc.mytools.rxjava.test;

import hlz.cn.jhc.mytools.rxjava.BaseView;

public interface IView extends BaseView {

    void uiTest(String msg);
}
