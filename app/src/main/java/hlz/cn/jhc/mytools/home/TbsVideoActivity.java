package hlz.cn.jhc.mytools.home;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tencent.smtt.sdk.TbsVideo;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.utils.Tools;
import me.james.biuedittext.BiuEditText;

public class TbsVideoActivity extends BaseActivity {


    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.editUrl1)
    BiuEditText editUrl1;
    @BindView(R.id.btnGo1)
    Button btnGo1;

    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_tbs_video);
        ButterKnife.bind(this);
    }

    @Override
    protected void initData() {
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText("在线播放器");
    }

    @OnClick({R.id.back, R.id.btnGo1})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.btnGo1:
                pay();
                break;
        }
    }

    private void pay() {
        String videoUrl = editUrl1.getText().toString().trim();
        if (TextUtils.isEmpty(videoUrl)){
            Tools.showShortToast("请输入播放地址。。。");
            return;
        }

        if (TbsVideo.canUseTbsPlayer(this)){
            Tools.showShortToast("加载中。。。");
            TbsVideo.openVideo(this,videoUrl);
        }else {
            Tools.showShortToast("暂不支持，请重试！");
        }
//        public static boolean canUseTbsPlayer(Context context)
//判断当前Tbs播放器是否已经可以使用。
//        public static void openVideo(Context context, String videoUrl)
//直接调用播放接口，传入视频流的url
//        public static void openVideo(Context context, String videoUrl, Bundle extraData)
//extraData对象是根据定制需要传入约定的信息，没有需要可以传如null
//extraData可以传入key: "screenMode", 值: 102, 来控制默认的播放UI
//类似: extraData.putInt("screenMode", 102); 来实现默认全屏+控制栏等UI
    }
}
