package hlz.cn.jhc.mytools.rxjava;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.just.agentweb.LogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitUtils {
    private static final String TAG = "RetrofitUtils";

    private static ApiService apiService;

    private static final int DEFAULT_TIMEOUT = 10000;
    private static final long TIME_OUT = 30;

    private static OkHttpClient httpClient = new OkHttpClient.Builder()
            .addInterceptor(new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                @Override
                public void log(String message) {
                    LogUtils.i(TAG, "log: ---message----" + message);
                }
            }).setLevel(HttpLoggingInterceptor.Level.BODY))
            .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(TIME_OUT, TimeUnit.SECONDS)
            .build();

    public static ApiService getApiService() {
        if (apiService == null) {
            synchronized (RetrofitUtils.class) {
                if (apiService == null) {
//                    OkHttpClient.Builder builder = new OkHttpClient.Builder();
//                    builder.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);

                    apiService = new Retrofit.Builder()
                            .client(httpClient)
//                            .addConverterFactory(GsonConverterFactory.create())
                            .addConverterFactory(DecodeConverterFactory.create(buildGson()))
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                    .baseUrl(TestPresenter.baseUrl)
                            .build()
                            .create(ApiService.class);
                }
            }
        }

        return apiService;
    }

    private static ApiService api;
    public static ApiService getApiService2(String baseUrl) {
        api = new Retrofit.Builder()
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(baseUrl)
                .build()
                .create(ApiService.class);
        return api;
    }

    public static RequestBody getBody(HashMap<String, String> map) {
        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"), new Gson().toJson(map));
    }

    public static String getCommonJsonData(HashMap map){
        JSONObject j=new JSONObject();
        try {
            Set<Map.Entry<String,String>> entry = map.entrySet();
            Iterator<Map.Entry<String,String>> ite = entry.iterator();
            while(ite.hasNext())
            {
                Map.Entry<String,String> en = ite.next();
                String key = en.getKey();
                String value = en.getValue();
                j.put(key,value);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String json=j.toString();
        return json;
    }

    private static Gson buildGson() {
        return new GsonBuilder().serializeNulls().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                // 此处可以添加Gson 自定义TypeAdapter
//                .registerTypeAdapter(UserInfo.class, new UserInfoTypeAdapter())
                .create();
    }

}
