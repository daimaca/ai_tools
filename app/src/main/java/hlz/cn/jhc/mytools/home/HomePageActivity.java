package hlz.cn.jhc.mytools.home;

import android.Manifest;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.github.glomadrian.grav.GravView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.base.BaseFragment;
import hlz.cn.jhc.mytools.home.homefragment.HomeFragment;
import hlz.cn.jhc.mytools.home.minefragment.MineFragment;
import hlz.cn.jhc.mytools.home.xiaozhifragment.XiaoZhiFragment;
import hlz.cn.jhc.mytools.jpush.JPushUtil;
import hlz.cn.jhc.mytools.jpush.TagAliasOperatorHelper;
import hlz.cn.jhc.mytools.utils.Tools;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class HomePageActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks {


    private static final String TAG = "HomePageActivity";
    private static final String HOME_FRAGMENT_TAG = "HOME_FRAGMENT_TAG";
    private static final String XIAO_ZHI_FRAGMENT_TAG = "XIAO_ZHI_FRAGMENT_TAG";
    private static final String MINE_FRAGMENT_TAG = "MINE_FRAGMENT_TAG";
    @BindView(R.id.fl_home)
    FrameLayout flHome;
    @BindView(R.id.view_home_line)
    View viewHomeLine;
    @BindView(R.id.rb_home)
    RadioButton rbHome;
    @BindView(R.id.rb_xz)
    RadioButton rbXz;
    @BindView(R.id.rb_mine)
    RadioButton rbMine;
    @BindView(R.id.rg_tabs)
    RadioGroup rgTabs;
    @BindView(R.id.mConstraintLayoutPaoPao)
    ConstraintLayout mConstraintLayoutPaoPao;
    private ColorStateList colorStateList;

    public List<BaseFragment> mFragments = new ArrayList<>();
    private HomeFragment mHomeFragment;
    private XiaoZhiFragment mXiaoZhiFragment;
    private MineFragment mMineFragment;
    private Fragment mCurrentFragment;
    private FragmentManager manager;


    @Override
    protected void setLayout() {
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 11) {
                getWindow()
                        .setFlags(
                                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
            }
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_home_page);
        ButterKnife.bind(this);
//        setStatusBar();
        methodRequiresTwoPermission();
    }

    @Override
    protected void initData() {
        Log.i(TAG, "initData: -----------1111111111111----------");
        int[] colors = new int[]{getResources().getColor(R.color._666666), getResources().getColor(R.color.green_tab)};
        int[][] states = new int[2][];
        states[0] = new int[]{-android.R.attr.state_checked};
        states[1] = new int[]{android.R.attr.state_checked};
        colorStateList = new ColorStateList(states, colors);
        rbHome.setTextColor(colorStateList);
        rbXz.setTextColor(colorStateList);
        rbMine.setTextColor(colorStateList);

        setAlias(JPushUtil.getDeviceId(this));//设置别名

        initFragment();
        rbHome.setChecked(true);
        mCurrentFragment = mFragments.get(0);
        setCurrentFragment(mFragments.get(0));
        Log.i(TAG, "initData: -----------22222222222----------");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Tools.isOpenPaoPao(mConstraintLayoutPaoPao);
//        if (Tools.isOpenPaoPao()) {
//            mConstraintLayoutPaoPao.setVisibility(View.VISIBLE);
//        } else {
//            mConstraintLayoutPaoPao.setVisibility(View.INVISIBLE);
//        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mConstraintLayoutPaoPao.removeAllViews();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        rbHome.setChecked(true);
    }

    private void initWidget(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            // 初始化Fragment
            mHomeFragment = HomeFragment.netInstance();
            mFragments.add(mHomeFragment);
            initFragment();
        } else {
            manager = getSupportFragmentManager();
            // 由于FragmentManager已经存在了这些Fragment直接获取就可以了
            mHomeFragment = (HomeFragment) getSupportFragmentManager()
                    .findFragmentByTag(HOME_FRAGMENT_TAG);
            mXiaoZhiFragment = (XiaoZhiFragment) getSupportFragmentManager()
                    .findFragmentByTag(XIAO_ZHI_FRAGMENT_TAG);
            mMineFragment = (MineFragment) getSupportFragmentManager()
                    .findFragmentByTag(MINE_FRAGMENT_TAG);
        }

    }

    private void setCurrentFragment(BaseFragment fg) {
        FragmentTransaction transaction = manager.beginTransaction();
        //如果之前没有添加过
        if (!fg.isAdded()) {

            if (fg instanceof HomeFragment) {
                transaction
                        .hide(mCurrentFragment)
                        .add(R.id.fl_home, fg, HOME_FRAGMENT_TAG)
                        .show(fg);
            } else if (fg instanceof XiaoZhiFragment) {
                transaction
                        .hide(mCurrentFragment)
                        .add(R.id.fl_home, fg, XIAO_ZHI_FRAGMENT_TAG)
                        .show(fg);
            } else if (fg instanceof MineFragment) {
                transaction
                        .hide(mCurrentFragment)
                        .add(R.id.fl_home, fg, MINE_FRAGMENT_TAG)
                        .show(fg);
            }

        } else {

            for (int i = 0; i < mFragments.size(); i++) {
                transaction.hide(mFragments.get(i));
            }
            transaction
                    .show(fg);
        }
        //全局变量，记录当前显示的fragment
        mCurrentFragment = fg;
        transaction.commit();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    private void initFragment() {

        if (manager == null) {
            manager = getSupportFragmentManager();
        }

        if (mFragments == null) {
            mFragments = new ArrayList<>();
        }

        if (mFragments.size() == 0) {
            mFragments.add(0, HomeFragment.netInstance());
            mFragments.add(1, XiaoZhiFragment.netInstance());
            mFragments.add(2, MineFragment.netInstance());
        }

        if (savedInstanceState != null) {
            mFragments.clear();

            if (manager.findFragmentByTag(HOME_FRAGMENT_TAG) != null) {
                mFragments.add(0, (HomeFragment) manager.findFragmentByTag(HOME_FRAGMENT_TAG));
            } else {
                mFragments.add(0, HomeFragment.netInstance());
            }

            if (manager.findFragmentByTag(XIAO_ZHI_FRAGMENT_TAG) != null) {
                mFragments.add(1, (XiaoZhiFragment) manager.findFragmentByTag(XIAO_ZHI_FRAGMENT_TAG));
            } else {
                mFragments.add(1, XiaoZhiFragment.netInstance());
            }
            if (manager.findFragmentByTag(MINE_FRAGMENT_TAG) != null) {
                mFragments.add(2, (MineFragment) manager.findFragmentByTag(MINE_FRAGMENT_TAG));
            } else {
                mFragments.add(2, MineFragment.netInstance());
            }
        }


    }

    private boolean isAdd(int f) {
        Fragment fragment = null;
        if (f == 0) {
            fragment = manager.findFragmentByTag(HOME_FRAGMENT_TAG);
        }
        if (f == 1) {
            fragment = manager.findFragmentByTag(XIAO_ZHI_FRAGMENT_TAG);
        }
        if (f == 2) {
            fragment = manager.findFragmentByTag(MINE_FRAGMENT_TAG);
        }

        if (fragment == null) {
            return false;
        } else {
            return true;
        }
    }

    private void showHomeFragment() {
        initFragment();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();

        if (mCurrentFragment instanceof HomeFragment) {
            return;
        }
        fragmentTransaction.show(mHomeFragment);
        if (mXiaoZhiFragment != null) {
            fragmentTransaction.hide(mXiaoZhiFragment);
        }
        if (mMineFragment != null) {
            fragmentTransaction.hide(mMineFragment);
        }
        fragmentTransaction.commit();
        mCurrentFragment = mHomeFragment;
        // 设置状态栏文字图标颜色
//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    private void showXiaoZhiFragment() {
        if (mCurrentFragment instanceof XiaoZhiFragment) {
            return;
        }
        if (mXiaoZhiFragment == null) {
            initWidget(null);
        }
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.show(mXiaoZhiFragment);

        if (mHomeFragment != null) {
            fragmentTransaction.hide(mHomeFragment);
        }
        if (mMineFragment != null) {
            fragmentTransaction.hide(mMineFragment);
        }
        fragmentTransaction.commit();
        mCurrentFragment = mXiaoZhiFragment;
        // 设置状态栏文字图标颜色
//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    private void showMineFragment() {

        if (mCurrentFragment instanceof MineFragment) {
            return;
        }
        if (mXiaoZhiFragment == null) {
            initWidget(null);
        }
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.show(mMineFragment);

        if (mHomeFragment != null) {
            fragmentTransaction.hide(mHomeFragment);
        }
        if (mXiaoZhiFragment != null) {
            fragmentTransaction.hide(mXiaoZhiFragment);
        }
        fragmentTransaction.commit();
        mCurrentFragment = mMineFragment;
        // 设置状态栏文字图标颜色
//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @OnClick({R.id.rb_home, R.id.rb_xz, R.id.rb_mine})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rb_home:
//                showHomeFragment();
                setCurrentFragment(mFragments.get(0));
                break;
            case R.id.rb_xz:
//                if (mXiaoZhiFragment == null) {
//                    mXiaoZhiFragment = XiaoZhiFragment.netInstance();
//                    mFragments.add(mXiaoZhiFragment);
//                    FragmentTransaction fragmentTransaction = manager.beginTransaction();
//                    fragmentTransaction.add(R.id.fl_home, mXiaoZhiFragment, XIAO_ZHI_FRAGMENT_TAG);
//                    fragmentTransaction.commit();
//                }
//                showXiaoZhiFragment();
                setCurrentFragment(mFragments.get(1));
                break;
            case R.id.rb_mine:
//                if (mMineFragment == null) {
//                    mMineFragment = MineFragment.netInstance();
//                    mFragments.add(mMineFragment);
//                    FragmentTransaction fragmentTransaction = manager.beginTransaction();
//                    fragmentTransaction.add(R.id.fl_home, mMineFragment, MINE_FRAGMENT_TAG);
//                    fragmentTransaction.commit();
//                }
//                showMineFragment();
                setCurrentFragment(mFragments.get(2));
                break;
        }
    }

    /**
     * todo 极光设置别名
     */
    int sequence = 1;

    private void setAlias(String aliasStr) {
        //CarstewardId 车管家id作为用户别名
        Log.i(TAG, "setAlias: " + aliasStr);
        if (JPushUtil.isValidTagAndAlias(aliasStr)) {
            //JPush 设置Alias
            TagAliasOperatorHelper.TagAliasBean aliasBean = new TagAliasOperatorHelper.TagAliasBean();
            aliasBean.setAlias(aliasStr);
            aliasBean.setAction(TagAliasOperatorHelper.ACTION_SET);
            aliasBean.setAliasAction(true);
            sequence++;
            TagAliasOperatorHelper.getInstance().handleAction(this, sequence, aliasBean);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    int RC_CAMERA_AND_LOCATION = 1;

    @AfterPermissionGranted(1)
    private void methodRequiresTwoPermission() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "请允许此权限，否则可能影响应用的使用",
                    RC_CAMERA_AND_LOCATION, perms);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
//        Log.i(TAG, "onPermissionsGranted: 11111111111111111111" + new Gson().toJson(perms));
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
//        Log.i(TAG, "onPermissionsDenied: 222222222222222222222222" + new Gson().toJson(perms));
    }

    /**
     * 设置状态栏的属性
     */
    protected boolean useThemestatusBarColor = false;//是否使用特殊的标题栏背景颜色，android5.0以上可以设置状态栏背景色，如果不使用则使用透明色值
    protected boolean withoutUseStatusBarColor = false;//是否使用状态栏文字和图标为暗色，如果状态栏采用了白色系，则需要使状态栏和图标为暗色，android6.0以上可以设置

    protected void setStatusBar() {
        //来自 http://blog.csdn.net/smileiam/article/details/73603840
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {//5.0及以上
            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            decorView.setSystemUiVisibility(option);
            //根据上面设置是否对状态栏单独设置颜色
            if (useThemestatusBarColor) {
                getWindow().setStatusBarColor(Color.WHITE);
            } else {
                getWindow().setStatusBarColor(Color.TRANSPARENT);
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {//4.4到5.0
            WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
            localLayoutParams.flags = (WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS | localLayoutParams.flags);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !withoutUseStatusBarColor) {//android6.0以后可以对状态栏文字颜色和图标进行修改
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }
}
