package hlz.cn.jhc.mytools.movies.agenweb;

import android.content.res.Configuration;
import android.view.View;

import hlz.cn.jhc.mytools.db.SharedPreferencesConst;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.movies.web.DataVip;

/**
 * Created by cenxiaozhong on 2017/5/22.
 *  <p>
 *
 */

public class VipWebActivity extends BaseVipWebActivity {

    @Override
    public String getUrl() {
        int p = SharedPreferencesUtils.getKeyValue(this, SharedPreferencesConst.SelectPlatformPositionKey,0);
        return DataVip.urls[p];
    }

    @Override
    public String getTitleData() {
        int p = SharedPreferencesUtils.getKeyValue(this, SharedPreferencesConst.SelectPlatformPositionKey,0);
        return DataVip.names[p];
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(newConfig.orientation==Configuration.ORIENTATION_PORTRAIT){//现在是竖屏
            mTopMenuLinearLayout.setVisibility(View.VISIBLE);
        }
        if(newConfig.orientation==Configuration.ORIENTATION_LANDSCAPE){//现在是横屏
            mTopMenuLinearLayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //测试Cookies
        /*try {

            String targetUrl="";
            Log.i("Info","cookies:"+ AgentWebConfig.getCookiesByUrl(targetUrl="http://www.jd.com"));
            AgentWebConfig.removeAllCookies(new ValueCallback<Boolean>() {
                @Override
                public void onReceiveValue(Boolean value) {
                    Log.i("Info","onResume():"+value);
                }
            });

            String tagInfo=AgentWebConfig.getCookiesByUrl(targetUrl);
            Log.i("Info","tag:"+tagInfo);
            AgentWebConfig.syncCookie("http://www.jd.com","ID=IDHl3NVU0N3ltZm9OWHhubHVQZW1BRThLdGhLaFc5TnVtQWd1S2g1REcwNVhTS3RXQVFBQEBFDA984906B62C444931EA0");
            String tag=AgentWebConfig.getCookiesByUrl(targetUrl);
            Log.i("Info","tag:"+tag);
            AgentWebConfig.removeSessionCookies();
            Log.i("Info","removeSessionCookies:"+AgentWebConfig.getCookiesByUrl(targetUrl));
        }catch (Exception e){
            e.printStackTrace();
        }*/

    }
}
