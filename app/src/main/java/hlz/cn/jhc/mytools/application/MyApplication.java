package hlz.cn.jhc.mytools.application;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheEntity;
import com.lzy.okgo.cache.CacheMode;
import com.lzy.okgo.cookie.CookieJarImpl;
import com.lzy.okgo.cookie.store.DBCookieStore;
import com.lzy.okgo.cookie.store.MemoryCookieStore;
import com.lzy.okgo.cookie.store.SPCookieStore;
import com.lzy.okgo.https.HttpsUtils;
import com.lzy.okgo.interceptor.HttpLoggingInterceptor;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;
import com.mob.MobSDK;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.DefaultRefreshFooterCreator;
import com.scwang.smartrefresh.layout.api.DefaultRefreshHeaderCreator;
import com.scwang.smartrefresh.layout.api.RefreshFooter;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.tencent.smtt.sdk.QbSdk;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import cn.jpush.android.api.JPushInterface;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.db.SharedPreferencesUtils;
import hlz.cn.jhc.mytools.umeng.UMengUtils;
import hlz.cn.jhc.mytools.utils.BadgeUtils;
import hlz.cn.jhc.mytools.utils.Tools;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.OkHttpClient;

/**
 * Created by Administrator on 2018/6/12.
 */

public class MyApplication extends Application{

    //todo SmartRefreshLayout static 代码段可以防止内存泄露
    static {
        //设置全局的Header构建器
        SmartRefreshLayout.setDefaultRefreshHeaderCreator(new DefaultRefreshHeaderCreator() {
            @Override
            public RefreshHeader createRefreshHeader(Context context, RefreshLayout layout) {
                layout.setPrimaryColorsId(R.color.white, R.color._666666);//全局设置主题颜色
                return new ClassicsHeader(context);//.setTimeFormat(new DynamicTimeFormat("更新于 %s"));//指定为经典Header，默认是 贝塞尔雷达Header
            }
        });
        //设置全局的Footer构建器
        SmartRefreshLayout.setDefaultRefreshFooterCreator(new DefaultRefreshFooterCreator() {
            @Override
            public RefreshFooter createRefreshFooter(Context context, RefreshLayout layout) {
                //指定为经典Footer，默认是 BallPulseFooter
                return new ClassicsFooter(context).setDrawableSize(20);
            }
        });
    }


    private static final String TAG = "MyApplication";
    private static MyApplication instance;
    public static Context mContext;
    public static CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mContext = getApplicationContext();

        new Thread(new Runnable() {
            @Override
            public void run() {
                /**
                 *  x5内核加载
                 */
                londingX5();

                /**
                 * 友盟
                 */
                umeng();

                initJPush();//JPush


            }
        }).start();

        settingBadge();//角标相关设置

        MobSDK.init(this,"29ec6424b2f15","f7288c27be00064db33dd86c9bae1dce");//mob - init

        /**
         *  初始化 OkGo
          */
       // initOkGo();

    }

    private void initJPush(){
        // 89c6cb20c0de0cd26354d7b9dc51c954
        JPushInterface.setDebugMode(true);
        JPushInterface.init(this);
    }

    /**
     * todo 角标相关设置
     */
    private void settingBadge(){
        BadgeUtils.BadgeCount  = 0;//进入app角标为零
//        JPushInterface.clearAllNotifications(mContext);
//        JPushInterface.clearLocalNotifications(mContext);
        JPushInterface.setLatestNotificationNumber(this,Integer.MAX_VALUE);//极光推送设置角标最大的个数

        activityLifeCallBack();// 监听前后台
    }

    private void initOkGo() {
        OkGo.getInstance().init(this);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor("OkGo");
        //log打印级别，决定了log显示的详细程度
        loggingInterceptor.setPrintLevel(HttpLoggingInterceptor.Level.BODY);
        //log颜色级别，决定了log在控制台显示的颜色
        loggingInterceptor.setColorLevel(Level.INFO);
        builder.addInterceptor(loggingInterceptor);
        //非必要情况，不建议使用，第三方的开源库，使用通知显示当前请求的log，不过在做文件下载的时候，这个库好像有问题，对文件判断不准确
//        builder.addInterceptor(new ChuckInterceptor(this));

        //全局的读取超时时间
        builder.readTimeout(OkGo.DEFAULT_MILLISECONDS, TimeUnit.MILLISECONDS);
        //全局的写入超时时间
        builder.writeTimeout(OkGo.DEFAULT_MILLISECONDS, TimeUnit.MILLISECONDS);
        //全局的连接超时时间
        builder.connectTimeout(OkGo.DEFAULT_MILLISECONDS, TimeUnit.MILLISECONDS);

        //使用sp保持cookie，如果cookie不过期，则一直有效
        builder.cookieJar(new CookieJarImpl(new SPCookieStore(this)));
        //使用数据库保持cookie，如果cookie不过期，则一直有效
        builder.cookieJar(new CookieJarImpl(new DBCookieStore(this)));
        //使用内存保持cookie，app退出后，cookie消失
        builder.cookieJar(new CookieJarImpl(new MemoryCookieStore()));

        //方法一：信任所有证书,不安全有风险
        HttpsUtils.SSLParams sslParams1 = HttpsUtils.getSslSocketFactory();
//方法二：自定义信任规则，校验服务端证书
//        HttpsUtils.SSLParams sslParams2 = HttpsUtils.getSslSocketFactory(new SafeTrustManager());
//方法三：使用预埋证书，校验服务端证书（自签名证书）
//        HttpsUtils.SSLParams sslParams3 = HttpsUtils.getSslSocketFactory(getAssets().open("srca.cer"));
//方法四：使用bks证书和密码管理客户端证书（双向认证），使用预埋证书，校验服务端证书（自签名证书）
//        HttpsUtils.SSLParams sslParams4 = HttpsUtils.getSslSocketFactory(getAssets().open("xxx.bks"), "123456", getAssets().open("yyy.cer"));
        builder.sslSocketFactory(sslParams1.sSLSocketFactory, sslParams1.trustManager);
//配置https的域名匹配规则，详细看demo的初始化介绍，不需要就不要加入，使用不当会导致https握手失败
//        builder.hostnameVerifier(new SafeHostnameVerifier());


        //---------这里给出的是示例代码,告诉你可以这么传,实际使用的时候,根据需要传,不需要就不传-------------//
        HttpHeaders headers = new HttpHeaders();
        headers.put("commonHeaderKey1", "commonHeaderValue1");    //header不支持中文，不允许有特殊字符
        headers.put("commonHeaderKey2", "commonHeaderValue2");
        HttpParams params = new HttpParams();
        params.put("commonParamsKey1", "commonParamsValue1");     //param支持中文,直接传,不要自己编码
        params.put("commonParamsKey2", "这里支持中文参数");
//-------------------------------------------------------------------------------------//
        OkGo.getInstance().init(this)                       //必须调用初始化
                .setOkHttpClient(builder.build())               //建议设置OkHttpClient，不设置将使用默认的
                .setCacheMode(CacheMode.NO_CACHE)               //全局统一缓存模式，默认不使用缓存，可以不传
                .setCacheTime(CacheEntity.CACHE_NEVER_EXPIRE)   //全局统一缓存时间，默认永不过期，可以不传
                .setRetryCount(3)                               //全局统一超时重连次数，默认为三次，那么最差的情况会请求4次(一次原始请求，三次重连请求)，不需要可以设置为0
                .addCommonHeaders(headers)                      //全局公共头
                .addCommonParams(params);                       //全局公共参数
    }

    /**
     * 友盟
     */
    private void umeng() {
        UMConfigure.init(getApplicationContext(), UMConfigure.DEVICE_TYPE_PHONE, UMengUtils.getUMengpushSecret());
        UMConfigure.init(this, "5c2985cab465f568f30000b1", "AIXiaoZhi", UMConfigure.DEVICE_TYPE_PHONE, null);
        MobclickAgent.setScenarioType(mContext.getApplicationContext(), MobclickAgent.EScenarioType.E_UM_NORMAL);
//        MobclickAgent.setSecret();
        /**
         * 设置组件化的Log开关
         * 参数: boolean 默认为false，如需查看LOG设置为true
         */
        UMConfigure.setLogEnabled(true);

        /**
         * 设置日志加密
         * 参数：boolean 默认为false（不加密）
         */
        UMConfigure.setEncryptEnabled(true);
        MobclickAgent.setSessionContinueMillis(20*1000);

    }

    public static MyApplication getInstance() {
        return instance;
    }

    //加载x5内核
    private void londingX5(){
        // TODO Auto-generated method stub
        //搜集本地tbs内核信息并上报服务器，服务器返回结果决定使用哪个内核。

        QbSdk.PreInitCallback cb = new QbSdk.PreInitCallback() {

            @Override
            public void onViewInitFinished(boolean arg0) {
                // TODO Auto-generated method stub
                //x5內核初始化完成的回调，为true表示x5内核加载成功，否则表示x5内核加载失败，会自动切换到系统内核。
                Log.d("app1", " onViewInitFinished is " + arg0);
                Log.i("app1", " onViewInitFinished is " + arg0);
            }

            @Override
            public void onCoreInitFinished() {
                // TODO Auto-generated method stub
            }
        };
        //x5内核初始化接口
        QbSdk.initX5Environment(getApplicationContext(),  cb);
        QbSdk.setDownloadWithoutWifi(true);
    }


    private void setBadge(){
        if (Tools.isAppOnForeground()){
            BadgeUtils.BadgeCount  = 0;
//            JPushInterface.clearAllNotifications(mContext);
//            JPushInterface.clearLocalNotifications(mContext);

            String carrier= BadgeUtils.getCarrier();//获取手机厂商
            if (carrier.contains("xiaomi")){//小米平台较为特殊，支持JPush角标个数
                return;
            }else {
                //todo 当进入app清除角标设置，小米可以不设置，会自动清除
                BadgeUtils.setIconCount(mContext,0);
            }
        }
    }

    //监听Activity的生命周期
    private void activityLifeCallBack(){
        registerActivityLifecycleCallbacks(lifecycleCallbacks);
    }
    ActivityLifecycleCallbacks lifecycleCallbacks = new ActivityLifecycleCallbacks() {
        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {
            Log.i(TAG, "onActivityResumed: ");
            //todo 开启前台
            Log.i(TAG, "检测是否为前台1: "+Tools.isAppOnForeground());
            setBadge();
        }

        @Override
        public void onActivityPaused(Activity activity) {
            Log.i(TAG, "onActivityPaused: ");

        }

        @Override
        public void onActivityStopped(Activity activity) {
            Log.i(TAG, "onActivityStopped: ");
            //todo 后台
            Log.i(TAG, "检测是否为前台2: "+ Tools.isAppOnForeground());
            setBadge();
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            Log.i(TAG, "onActivitySaveInstanceState: ");

        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            Log.i(TAG, "onActivityDestroyed: ");

        }
    };
}
