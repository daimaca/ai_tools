package hlz.cn.jhc.mytools.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;


import java.lang.reflect.Field;
import java.lang.reflect.Method;

import hlz.cn.jhc.mytools.R;
import me.leolin.shortcutbadger.ShortcutBadger;

public class BadgeUtils {
    private static final String TAG = "BadgeUtils";

    public static int BadgeCount = 0;

    //todo 通用 --- android角标数目
    public static void setCommentIconCount(Context context, int count) {
        ShortcutBadger.applyCount(context, count); //for 1.1.4+
    }

    //todo 华为 --- android角标数目
    public static void setWuaWeiIconCount(Context context, int count) {
//        Bundle bundle = new Bundle();
//        bundle.putString("package", context.getPackageName());
//        String launchClassName = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName()).getComponent().getClassName();
//        bundle.putString("class", launchClassName);
//        bundle.putInt("badgenumber", count);
//        context.getContentResolver().call(Uri.parse("content://com.huawei.android.launcher.settings/badge/"), "change_badge", null, bundle);

        try {
            Bundle bunlde = new Bundle();
            bunlde.putString("package", context.getPackageName());
            bunlde.putString("class", getLauncherClassName(context));
            bunlde.putInt("badgenumber", count);
            context.getContentResolver().call(Uri.parse("content://com.huawei.android.launcher.settings/badge/"), "change_badge", null, bunlde);
        } catch (Exception e) {
            e.printStackTrace();
            boolean mIsSupportedBade = false;//此处为是否显示角标
        }
    }

    //todo 小米 --- android角标数目
    public static void setMiUIIconCount(Context context, int count) {
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification.Builder builder = new Notification.Builder(context)
                .setContentTitle(context.getResources().getString(R.string.app_name)).setContentText(context.getResources().getString(R.string.app_name)).setSmallIcon(R.drawable.ic_launcher);
        Notification notification = builder.build();
        try {
            Field field = notification.getClass().getDeclaredField("extraNotification");
            Object extraNotification = field.get(notification);
            Method method = extraNotification.getClass().getDeclaredMethod("setMessageCount", int.class);
            method.invoke(extraNotification, count);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mNotificationManager.notify(0, notification);
    }

    /**
     * todo VIVO --- android角标数目
     * vivo、三星、小米手机、htc都比较友好，都通过广播设置角标，亲测都有效
     *
     * @param context
     * @param count
     */
    public static void setVivoconCount(Context context, int count) {
        Intent intent = new Intent("launcher.action.CHANGE_APPLICATION_NOTIFICATION_NUM");
        intent.putExtra("packageName", context.getPackageName());
        String launchClassName = getLauncherClassName(context);
        intent.putExtra("className", launchClassName);
        intent.putExtra("notificationNum", count);
        context.sendBroadcast(intent);
    }

    /**
     * todo 设置三星的Badge
     * vivo、三星、小米手机、htc都比较友好，都通过广播设置角标，亲测都有效
     *
     * @param context context
     * @param count   count
     */
    public static void setBadgeOfSumsung(Context context, int count) {
        // 获取你当前的应用
//        String launcherClassName = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName()).getComponent().getClassName();
//        if (launcherClassName == null) {
//            return;
//        }

        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", getLauncherClassName(context));
        context.sendBroadcast(intent);

//        Intent intent = new Intent("launcher.action.CHANGE_APPLICATION_NOTIFICATION_NUM");
//        intent.putExtra("packageName", context.getPackageName());
//        intent.putExtra("className", getLauncherClassName(context));
//        intent.putExtra("notificationNum", count);
//        context.sendBroadcast(intent);
    }

    /**
     * todo 设置索尼的Badge
     * <p/>
     * 需添加权限：<uses-permission android:name="com.sonyericsson.home.permission.BROADCAST_BADGE" />
     *
     * @param context context
     * @param count   count
     */
    public static void setBadgeOfSony(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        if (launcherClassName == null) {
            return;
        }
        boolean isShow = true;
        if (count == 0) {
            isShow = false;
        }
        Intent localIntent = new Intent();
        localIntent.setAction("com.sonyericsson.home.action.UPDATE_BADGE");
        localIntent.putExtra("com.sonyericsson.home.intent.extra.badge.SHOW_MESSAGE", isShow);//是否显示
        localIntent.putExtra("com.sonyericsson.home.intent.extra.badge.ACTIVITY_NAME", launcherClassName);//启动页
        localIntent.putExtra("com.sonyericsson.home.intent.extra.badge.MESSAGE", String.valueOf(count));//数字
        localIntent.putExtra("com.sonyericsson.home.intent.extra.badge.PACKAGE_NAME", context.getPackageName());//包名
        context.sendBroadcast(localIntent);
    }

    /**
     * todo 设置HTC的Badge
     *
     * @param context context
     * @param count   count
     */
    public static void setBadgeOfHTC(Context context, int count) {
        Intent intentNotification = new Intent("com.htc.launcher.action.SET_NOTIFICATION");
        ComponentName localComponentName = new ComponentName(context.getPackageName(), getLauncherClassName(context));
        intentNotification.putExtra("com.htc.launcher.extra.COMPONENT", localComponentName.flattenToShortString());
        intentNotification.putExtra("com.htc.launcher.extra.COUNT", count);
        context.sendBroadcast(intentNotification);

        Intent intentShortcut = new Intent("com.htc.launcher.action.UPDATE_SHORTCUT");
        intentShortcut.putExtra("packagename", context.getPackageName());
        intentShortcut.putExtra("count", count);
        context.sendBroadcast(intentShortcut);
    }

    /**
     * todo 设置Nova的Badge
     *
     * @param context context
     * @param count   count
     */
    public static void setBadgeOfNova(Context context, int count) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("tag", context.getPackageName() + "/" + getLauncherClassName(context));
        contentValues.put("count", count);
        context.getContentResolver().insert(Uri.parse("content://com.teslacoilsw.notifier/unread_count"),
                contentValues);
    }

    public static void setRemoveIconCount(Context context) {
        ShortcutBadger.removeCount(context); //for 1.1.4+
    }


    public static void setIconCount(Context context, int count) {
        String model = android.os.Build.MODEL;//获取手机型号
        String carrier = android.os.Build.MANUFACTURER.toLowerCase();//获取手机厂商
        Log.i(TAG, "setIconCount: 1." + context.getPackageName() + " 2." + getLauncherClassName(context));
        Log.i(TAG, "setIconCount: " + carrier + "个数：" + count);
        if (carrier.contains("huawei")) {
            Log.i(TAG, "华为: " + carrier);
            setWuaWeiIconCount(context, count);
        } else if (carrier.contains("xiaomi")) {
            Log.i(TAG, "小米: " + carrier);
            setMiUIIconCount(context, count);
        } else if (carrier.contains("vivo")) {
            Log.i(TAG, "vivo: " + carrier);
            setVivoconCount(context, count);
        } else if (carrier.contains("samsung") || carrier.contains("lg")) {
            Log.i(TAG, "三星: " + carrier);
            setBadgeOfSumsung(context, count);
        } else if (carrier.contains("sony")) {
            Log.i(TAG, "索尼: " + carrier);
            setBadgeOfSony(context, count);
        } else if (carrier.contains("htc")) {
            Log.i(TAG, "htc: " + carrier);
            setBadgeOfHTC(context, count);
        } else if (carrier.contains("nova")) {
            Log.i(TAG, "nova: " + carrier);
            setBadgeOfNova(context, count);
        } else {
            Log.i(TAG, "通用: " + carrier);
            setCommentIconCount(context, count);
        }
    }


    public static String getLauncherClassName(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        // To limit the components this Intent will resolve to, by setting an
        // explicit package name.
        intent.setPackage(context.getPackageName());
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        // All Application must have 1 Activity at least.
        // Launcher activity must be found!
        ResolveInfo info = packageManager
                .resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        // get a ResolveInfo containing ACTION_MAIN, CATEGORY_LAUNCHER
        // if there is no Activity which has filtered by CATEGORY_DEFAULT
        if (info == null) {
            info = packageManager.resolveActivity(intent, 0);
        }
        //////////////////////另一种实现方式//////////////////////
        // ComponentName componentName = context.getPackageManager().getLaunchIntentForPackage(mContext.getPackageName()).getComponent();
        // return componentName.getClassName();
        //////////////////////另一种实现方式//////////////////////
        return info.activityInfo.name;
    }

    //获取手机厂商
    public static String getCarrier() {
        String carrier = android.os.Build.MANUFACTURER.toLowerCase();
        return carrier;
    }
}
