package hlz.cn.jhc.mytools.home.xiaozhifragment;

import android.content.Context;

import java.nio.Buffer;

import cn.sharerec.recorder.MediaOutput;
import hlz.cn.jhc.mytools.mob.ShareRECUtils;
import hlz.cn.jhc.mytools.utils.Tools;

public class MediaOutputREC extends MediaOutput {

    private MediaOutputREC() {
    }

    private static volatile MediaOutputREC mediaOutputREC;

    public static MediaOutputREC getInstance() {
        if (mediaOutputREC == null) {
            synchronized (MediaOutputREC.class) {
                if (mediaOutputREC == null) {
                    mediaOutputREC = new MediaOutputREC();
                }
            }
        }
        return mediaOutputREC;
    }

    @Override
    public void onStart() {
        Tools.showShortToast("开始录屏");
    }

    @Override
    public void onPause() {
        Tools.showShortToast("暂停录屏");
    }

    @Override
    public void onResume() {
        Tools.showShortToast("继续录屏");
    }

    @Override
    public void onStop() {
        Tools.showShortToast("正在为你保存录屏，请稍后...");
    }

    @Override
    public void onAudio(Buffer buffer, int i, long l, int i1) {

    }

    @Override
    public void onVideo(Buffer buffer, int i, long l, int i1) {

    }
}
