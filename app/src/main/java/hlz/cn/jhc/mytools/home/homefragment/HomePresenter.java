package hlz.cn.jhc.mytools.home.homefragment;

import android.util.Log;

import com.google.gson.Gson;

import java.util.HashMap;

import hlz.cn.jhc.mytools.pugongying.PGYUtils;
import hlz.cn.jhc.mytools.rxjava.BaseObserver2;
import hlz.cn.jhc.mytools.rxjava.RetrofitUtils;
import hlz.cn.jhc.mytools.rxjava.RxSchedulers;
import hlz.cn.jhc.mytools.utils.Tools;
import io.reactivex.Observable;

public class HomePresenter implements IPresenter{

    private IView mView;
    public HomePresenter(IView view){
        mView = view;
    }

    @Override
    public void updateApp() {
        HashMap<String,String> map = new HashMap<>();
        map.put("_api_key", PGYUtils.API_KEY);
        map.put("appKey",PGYUtils.APP_KEY);
        Log.i("updateApp", "updateApp: "+new Gson().toJson(map));
        Observable<UpdateAppBean> observable = RetrofitUtils.getApiService2("https://www.pgyer.com").updateApp();
        observable.compose(RxSchedulers.<UpdateAppBean>compose()).subscribe(new BaseObserver2<UpdateAppBean>(mView.setContext()) {
            @Override
            protected void onHandleSuccess(UpdateAppBean bean) {
                mView.uiUpdateApp(bean);
            }

            @Override
            protected void onHandleError(String msg) {
                Tools.showShortToast(msg);
            }
        });
    }
}
