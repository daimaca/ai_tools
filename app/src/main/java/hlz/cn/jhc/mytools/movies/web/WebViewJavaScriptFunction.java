package hlz.cn.jhc.mytools.movies.web;

public interface WebViewJavaScriptFunction {

	void onJsFunctionCalled(String tag);
}
