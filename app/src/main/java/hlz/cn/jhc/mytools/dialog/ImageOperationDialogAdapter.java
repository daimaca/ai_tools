package hlz.cn.jhc.mytools.dialog;

import android.support.annotation.Nullable;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import hlz.cn.jhc.mytools.R;

public class ImageOperationDialogAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    public static final String[] titles = new String[]{"下载", "分享", "斗图轰炸(推荐QQ使用)", "取消"};
    public static final int[] imgs = new int[]{0, 0, 0, 0};

    public ImageOperationDialogAdapter(@Nullable List<String> data) {
        super(R.layout.dialog_image_operation_item, data);
    }

    @Override
    protected void convert(final BaseViewHolder helper, String item) {
        helper.setText(R.id.tv_title, item);
//        if (helper.getAdapterPosition() == getItemCount() - 1) {
//            helper.setVisible(R.id.mView, false);
//        } else {
//            helper.setVisible(R.id.mView, true);
//        }

    }

    public static List<String> getImageData() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < titles.length; i++) {
            list.add(titles[i]);
        }
        return list;
    }

}
