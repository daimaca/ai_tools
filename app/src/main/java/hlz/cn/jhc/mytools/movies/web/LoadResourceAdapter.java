package hlz.cn.jhc.mytools.movies.web;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.dialog.ImageOperationDialog;
import hlz.cn.jhc.mytools.share.ShareUtils;
import hlz.cn.jhc.mytools.utils.Tools;

public class LoadResourceAdapter extends BaseQuickAdapter<String, LoadResourceAdapter.ViewHolder> {

    private static List<String> mLoadResourceList;

    private List<String> data;

    public LoadResourceAdapter(@Nullable List<String> data) {
        super(R.layout.load_resource_item, data);
        this.data = data;
    }


    @Override
    protected void convert(final ViewHolder helper, final String item) {
        if (item != null) {
            Glide.with(mContext).load(item).apply(Tools.getNoImageRequestOptions()).into(helper.ivImg);
        }
        helper.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                downloadImage(item);
                ImageOperationDialog dialog = new ImageOperationDialog(mContext,item);
                dialog.show();
            }
        });

    }

    private void downloadImage(final String url) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("请选择相应的操作")
                .setNegativeButton("下载", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Tools.showShortToast("已在后台下载中，请到相册擦看！");
                        new X5WebView.DownLoadImageThread(mContext,url).start();
                    }
                })
                .setNeutralButton("分享", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ShareUtils.shareImage(mContext,url);
                    }
                }).setPositiveButton("取消",null).show();

    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R.id.iv_img)
        ImageView ivImg;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public static List<String> getLoadResourceList() {
        if (mLoadResourceList == null) {
            mLoadResourceList = new ArrayList<>();
        }
        return mLoadResourceList;
    }

    public static List<String> setLoadResourceList(List<String> list) {
        if (mLoadResourceList == null) {
            mLoadResourceList = new ArrayList<>();
        }
        mLoadResourceList.clear();
        if (list != null) {
            mLoadResourceList.addAll(list);
        }
        return mLoadResourceList;
    }
}
