package hlz.cn.jhc.mytools.comment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hlz.cn.jhc.mytools.R;
import hlz.cn.jhc.mytools.base.BaseActivity;
import hlz.cn.jhc.mytools.share.ShareUtils;
import hlz.cn.jhc.mytools.utils.Tools;
import me.james.biuedittext.BiuEditText;

/**
 * 消息轰炸
 */
public class MessageBombActivity extends BaseActivity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.editCount)
    BiuEditText editCount;
    @BindView(R.id.btn_bottom)
    Button btnBottom;

    public static final String URL_FLAG = "URL_FLAG";
    private String url;

    @Override
    protected void setLayout() {
        setContentView(R.layout.activity_message_bomb);
        ButterKnife.bind(this);
        url = getIntent().getStringExtra(URL_FLAG);
    }

    @Override
    protected void initData() {
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText("消息轰炸");
        btnBottom.setText("开始轰炸");
    }

    @OnClick({R.id.back, R.id.btn_bottom})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.btn_bottom:
                submit();
                break;
        }
    }

    private void submit() {
        String text = editCount.getText().toString().trim();
        if (TextUtils.isEmpty(text)) {
            Tools.showShortToast("请输入轰炸次数");
            return;
        }
        int count = Integer.valueOf(text);
        if (count > 0) {

            if (Tools.isAdmin()) {
                ShareUtils.shareFightImages(this, url, count);
            } else {
                if (count <= 50) {
                    ShareUtils.shareFightImages(this, url, count);
                } else {
                    Tools.showShortToast("普通用户每次最高50次轰炸！");
                }
            }
        } else {
            Tools.showShortToast("请输入合法的次数！");
        }
    }
}
